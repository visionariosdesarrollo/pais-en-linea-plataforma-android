package com.nh.quickly;

import android.app.Service;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;

import java.io.File;

/**
 * Created by Diego on 25/10/2015.
 */

final public class App {
    public static final String LOG_TAG = "DebugApp";
    public final static String URL = "http://quickly.netha.com.co";
    public final static String URL_API = URL + "/api";
    public final static String URL_API_COUNTRIES = URL_API + "/countries";
    public final static String URL_API_DEPARTMENTS = URL_API + "/departments";
    public final static String URL_API_CITIES = URL_API + "/cities";
    public final static String URL_API_ENTITIES = URL_API + "/entities";
    public final static String URL_API_EVENTS = URL_API + "/events";
    public final static String URL_API_RELEASES = URL_API + "/releases";
    public final static String URL_API_CITIES_IMAGES = URL + "/public/images/cities/";
    public final static String URL_API_ENTITIES_IMAGES = URL + "/public/images/entities/";
    public final static String URL_API_USERS_IMAGES = URL + "/public/images/users/";
    public final static String DIRECTORY_MEDIA = "/media";
    public final static String DIRECTORY_MEDIA_IMAGES = DIRECTORY_MEDIA + "/images";
    public final static String DIRECTORY_MEDIA_IMAGES_CITIES = DIRECTORY_MEDIA_IMAGES + "/cities/";

    static AudioManager audioManager;
    static SoundPool soundPool;
    static int quicklySoundPool;
    static boolean isSoundsLoaded = false;

    static SoundPool getInstanceSoundPool(Context context){
        if(soundPool == null)
            soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);

        return soundPool;
    }

    public static void preparateSounds(Context context){
        if(!isSoundsLoaded) {
            audioManager = (AudioManager) context.getSystemService(Service.AUDIO_SERVICE);
            getInstanceSoundPool(context);
            quicklySoundPool = soundPool.load(context, R.raw.quickly, 1);
            isSoundsLoaded = true;
        }
    }

    public static int quicklySoundPlay(Context context){
        int result = 0;
        if(isSoundsLoaded)
            result = soundPool.play(quicklySoundPool, (float) audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION), (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION), 1, 0, 1f);

        return result;
    }

    public static String directoryMediaImagesCities(Context context){
        return context.getResources().getString(R.string.directory) + DIRECTORY_MEDIA_IMAGES_CITIES;
    }
}