package com.nh.quickly.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.ContentProviderApp;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.fragments.menu.MenuFragment;
import com.nh.quickly.fragments.menu.options.PreferencesFragment;
import com.nh.quickly.nh.utils.NHDataBaseUtils;
import com.nh.quickly.nh.utils.NHDateUtils;

import java.util.Date;

/**
 * Created by Diego on 23/06/2016.
 */
public class MenuFragmentIntentService extends IntentService {
    public static final String TAG = MenuFragmentIntentService.class.toString();
    final long SLEEP = 20000;
    Boolean isCancel = true;
    Intent intent;
    Intent menuFragmentBroadcastReceiverIntent;

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    updateMenu();
                    break;
            }
        }
    };

    public MenuFragmentIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        this.intent = intent;
        start();
    }

    void start() {
        menuFragmentBroadcastReceiverIntent = new Intent();
        menuFragmentBroadcastReceiverIntent.setAction(MenuFragment.MenuFragmentBroadcastReceiver.ACTION_UPDATE);
        menuFragmentBroadcastReceiverIntent.addCategory(Intent.CATEGORY_DEFAULT);

        loop();
    }

    void loop() {
        while (isCancel) {
            updateMenu();
            SystemClock.sleep(SLEEP);
        }
    }

    void updateMenu() {
        String time = String.valueOf(NHDateUtils.currentTimeStampForPhp());
        City city = PreferencesFragment.getCity(getBaseContext());

        int activeEvents = 0;
        Log.d(App.LOG_TAG, "EVENTOS ACTIVOS 0::" + activeEvents);
        if (city != null) {
            Log.d(App.LOG_TAG, "EVENTOS ACTIVOS 1::" + activeEvents);
            String selection = NHDataBaseUtils.concatenateWhereWithAnd(
                    NHDataBaseUtils.concatenateWhereWithAnd(
                            Event.Columns.FULL_EVENT_TIME_STAMP_START + "<=?", Event.Columns.FULL_EVENT_TIME_STAMP_END + ">=?"
                    ),
                    Entity.Columns.FULL_CITY_ID + "=?"
            );

            activeEvents = ContentProviderApp.count(
                    getBaseContext(),
                    Event.ContentProvider.ALL_ROWS_CODE,
                    selection,
                    new String[]{time, time, String.valueOf(city.getId())}
            );
            Log.d(App.LOG_TAG, "EVENTOS ACTIVOS 2::" + activeEvents);
        }
        Log.d(App.LOG_TAG, "EVENTOS ACTIVOS 3::" + activeEvents);

        Date currentDate = new Date();
        String dayText = NHDateUtils.simpleDateFormat("EEE", currentDate);
        dayText = dayText.substring(0, 1).toUpperCase() + dayText.substring(1).toLowerCase();
        String monthText = NHDateUtils.simpleDateFormat("MMM", currentDate).toLowerCase();
        monthText = monthText.substring(0, 1).toUpperCase() + monthText.substring(1).toLowerCase();
        String dateString = String.format(getApplicationContext().getResources().getString(R.string.menu_bar_format_date), dayText, NHDateUtils.simpleDateFormat(" d ", currentDate), monthText);

        menuFragmentBroadcastReceiverIntent.putExtra(MenuFragment.ACTIVE_EVENTS, activeEvents);
        menuFragmentBroadcastReceiverIntent.putExtra(MenuFragment.DATE, dateString);
        menuFragmentBroadcastReceiverIntent.putExtra(MenuFragment.TIME, java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT).format(currentDate).toLowerCase());
        menuFragmentBroadcastReceiverIntent.putExtra(MenuFragment.TIME_MERIDIAN, NHDateUtils.simpleDateFormat("a", currentDate).toLowerCase());

        sendBroadcast(menuFragmentBroadcastReceiverIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isCancel = false;
    }
}
