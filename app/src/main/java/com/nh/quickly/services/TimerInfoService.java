package com.nh.quickly.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Diego on 11/11/2015.
 */
public class TimerInfoService extends Service {
    private Timer timer = new Timer();
    private static final long RELOAD_INTERVAL = 1000;
    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {


            }
        };
        startTimerInfo();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimerInfo();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private void startTimerInfo() {
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                handler.sendEmptyMessage(0);
            }
        }, 0, RELOAD_INTERVAL);
    }

    private void stopTimerInfo() {
        if (timer != null) {
            timer.cancel();
        }
    }

}