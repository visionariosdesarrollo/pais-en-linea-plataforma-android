package com.nh.quickly.database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.nh.quickly.database.DataBase;
import com.nh.quickly.nh.database.NHContentProvider;

/**
 * Created by Diego on 10/12/2015.
 */
public class Department extends Model {
    final String tableName = "tbl_sys_config_country_department";

    final String scheme = "CREATE TABLE " + tableName + " ("
            + Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + Columns.COUNTRY_ID + " TEXT NOT NULL, "
            + Columns.NAME + " TEXT NOT NULL "
            + ");";

    final String primaryKey = Columns._ID;

    public String getTableName() {
        return tableName;
    }

    public String getScheme() {
        return scheme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int id;
    private int countryId;
    private String name;

    public static Department model(){
        return new Department();
    }

    @Override
    public synchronized Boolean createTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(scheme);
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    @Override
    public synchronized Boolean dropTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(String.valueOf(System.out.printf(DROP_TABLE_PATTERN, tableName)));
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    public static final class ContentProvider {
        public static final String QUERY_PATH = Department.model().getTableName() + "/";
        public static final int ONE_ROW_CODE = 200;
        public static final String ONE_ROW_PATH = Department.model().getTableName() + "/#";
        public static final int ALL_ROWS_CODE = 201;
        public static final String ALL_ROWS_PATH = Department.model().getTableName();
    }

    public static final class Columns implements BaseColumns {
        private Columns(){}

        public static final String ID = "id";
        public static final String COUNTRY_ID = "countryId";
        public static final String NAME = "name";

        public static final int ID_POSITION = 0;
        public static final int COUNTRY_ID_POSITION = 1;
        public static final int NAME_POSITION = 2;

        public static final String DEFAULT_SORT_ORDER = _ID + " DESC";
        public static final String SORT_BY_NAME_ASC  = NAME + " ASC";
        public static final String SORT_BY_NAME_DESC  = NAME + " DESC";

        public static final String[] getColumns() {
            return new String[]{
                    _ID,
                    COUNTRY_ID,
                    NAME
            };
        };
    }
}