package com.nh.quickly.database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.nh.quickly.database.DataBase;
import com.nh.quickly.nh.database.models.NHNotificationManagerModel;
import com.nh.quickly.nh.database.models.NHSyncModel;

/**
 * Created by Diego on 19/06/2016.
 */
public class Release extends Model {
    public final static String TAG = Release.class.toString();
    public static final int NOTIFICATION_ID = 100;
    final String tableName = "tbl_mod_release";

    final String scheme = "CREATE TABLE " + tableName + " ("
            + Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + Columns.USER_ID + " INTEGER NOT NULL, "
            + Columns.NAME + " TEXT NOT NULL, "
            + Columns.RELEASE_DATE_START + " TEXT NOT NULL, "
            + Columns.RELEASE_DATE_END + " TEXT NOT NULL, "
            + Columns.BODY + " TEXT NOT NULL, "
            + Columns.RELEASE_TIME_STAMP_START + " LONG NOT NULL, "
            + Columns.RELEASE_TIME_STAMP_END + " LONG NOT NULL, "
            + Columns.CITY_ID + " INTEGER NOT NULL, "
            + Columns.USER_NAME + " TEXT NOT NULL, "
            + Columns.USER_IMAGE + " TEXT NOT NULL, "
            + Columns.USER_ROLE + " TEXT NOT NULL, "
            + Columns.USER_DEPARTMENT_NAME + " TEXT NOT NULL, "
            + Columns.USER_CITY_NAME + " TEXT NOT NULL, "
            + Columns.STATUS + " TEXT NOT NULL, "
            + Columns.STATUS_NOTIFICATION_MANAGER + " INTEGER NOT NULL DEFAULT " + NHNotificationManagerModel.STATUS_NEW + ", "
            + Columns.STATUS_SYNC + " INTEGER NOT NULL DEFAULT " + NHSyncModel.STATUS_NEW + " "
            + ");";

    public static final String USER_NAME = "userName";
    public static final String USER_ROLE = "userRole";
    public static final String USER_DEPARTMENT_NAME = "userDepartmentName";
    public static final String USER_CITY_NAME = "userCityName";

    final String primaryKey = Columns._ID;

    public String getTableName() {
        return tableName;
    }

    public String getScheme() {
        return scheme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int id) {
        this.cityId = cityId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int entityId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleaseDateStart() {
        return releaseDateStart;
    }

    public void setReleaseDateStart(String releaseDateStart) {
        this.releaseDateStart = releaseDateStart;
    }

    public String getReleaseDateEnd() {
        return releaseDateEnd;
    }

    public void setReleaseDateEnd(String releaseDateEnd) {
        this.releaseDateEnd = releaseDateEnd;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getReleaseTimeStampStart() {
        return releaseTimeStampStart;
    }

    public void setReleaseTimeStampStart(long releaseTimeStampStart) {
        this.releaseTimeStampStart = releaseTimeStampStart;
    }

    public long getReleaseTimeStampEnd() {
        return releaseTimeStampEnd;
    }

    public void setReleaseTimeStampEnd(long releaseTimeStampEnd) {
        this.releaseTimeStampEnd = releaseTimeStampEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private int id;
    private int userId;
    private String name;
    private String releaseDateStart;
    private String releaseDateEnd;
    private String body;
    private long releaseTimeStampStart;
    private long releaseTimeStampEnd;
    private int cityId;
    private String status;
    private String userName;

    public static Release model(){
        return new Release();
    }

    @Override
    public synchronized Boolean createTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(scheme);
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    @Override
    public synchronized Boolean dropTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(String.valueOf(System.out.printf(DROP_TABLE_PATTERN, tableName)));
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    public static final class ContentProvider {
        public static final String QUERY_PATH = Release.model().getTableName() + "/";
        public static final int ONE_ROW_CODE = 800;
        public static final String ONE_ROW_PATH = Release.model().getTableName() + "/#";
        public static final int ALL_ROWS_CODE = 801;
        public static final String ALL_ROWS_PATH = Release.model().getTableName();
    }

    public static final class Columns implements BaseColumns {
        private Columns(){}

        public static final String ID = "id";
        public static final String USER_ID = "userId";
        public static final String NAME = "name";
        public static final String RELEASE_DATE_START = "releaseDateStart";
        public static final String RELEASE_DATE_END = "releaseDateEnd";
        public static final String BODY = "body";
        public static final String RELEASE_TIME_STAMP_START = "releaseTimeStampStart";
        public static final String RELEASE_TIME_STAMP_END = "releaseTimeStampEnd";
        public static final String CITY_ID = "cityId";
        public static final String USER_NAME = "userName";
        public static final String USER_IMAGE = "userImage";
        public static final String USER_ROLE = "userRole";
        public static final String USER_DEPARTMENT_NAME = "userDepartmentName";
        public static final String USER_CITY_NAME = "userCityName";
        public static final String STATUS = "status";
        public static final String STATUS_NOTIFICATION_MANAGER = NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER;
        public static final String STATUS_SYNC = NHSyncModel.Columns.STATUS_SYNC;

        public static final int ID_POSITION = 0;
        public static final int USER_ID_POSITION = 1;
        public static final int NAME_POSITION = 2;
        public static final int RELEASE_DATE_START_POSITION = 3;
        public static final int RELEASE_DATE_END_POSITION = 4;
        public static final int BODY_POSITION = 5;
        public static final int RELEASE_TIME_STAMP_START_POSITION = 6;
        public static final int RELEASE_TIME_STAMP_END_POSITION = 7;
        public static final int CITY_ID_POSITION = 8;
        public static final int USER_NAME_POSITION = 9;
        public static final int USER_IMAGE_POSITION = 10;
        public static final int USER_ROLE_POSITION = 11;
        public static final int USER_DEPARTMENT_NAME_POSITION = 12;
        public static final int USER_CITY_NAME_POSITION = 13;
        public static final int STATUS_POSITION = 14;
        public static final int SYNC_STATUS = 15;

        public static final String DEFAULT_SORT_ORDER = _ID + " DESC";
        public static final String SORT_BY_NAME_ASC  = NAME + " ASC";
        public static final String SORT_BY_NAME_DESC  = NAME + " DESC";
        public static final String RELEASE_TIME_STAMP_START_ASC  = RELEASE_TIME_STAMP_START + " ASC";
        public static final String RELEASE_TIME_STAMP_START_DESC  = RELEASE_TIME_STAMP_START + " DESC";

        public static final String[] getColumns() {
            return new String[]{
                    _ID,
                    USER_ID,
                    NAME,
                    RELEASE_DATE_START,
                    RELEASE_DATE_END,
                    BODY,
                    RELEASE_TIME_STAMP_START,
                    RELEASE_TIME_STAMP_END,
                    CITY_ID,
                    USER_NAME,
                    USER_IMAGE,
                    USER_ROLE,
                    USER_DEPARTMENT_NAME,
                    USER_CITY_NAME,
                    STATUS
            };
        };
    }
}