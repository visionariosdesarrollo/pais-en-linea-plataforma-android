package com.nh.quickly.database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.nh.quickly.database.DataBase;
import com.nh.quickly.nh.database.models.NHSyncModel;

/**
 * Created by Diego on 10/12/2015.
 */
public class EventQuickly extends Model {
    final String tableName = "tbl_mod_event_quickly";

    final String scheme = "CREATE TABLE " + tableName + " ("
            + Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + Columns.USER_ID + " INTEGER NOT NULL, "
            + Columns.EVENT_ID + " INTEGER NOT NULL, "
            + Columns.STATUS + " TEXT NOT NULL, "
            + Columns.STATUS_SYNC + " INTEGER NOT NULL DEFAULT " + NHSyncModel.STATUS_NEW + ", "
            + "UNIQUE(" + Columns.USER_ID + ", " + Columns.EVENT_ID + ") "
            + ");";

    final String primaryKey = Columns._ID;

    public String getTableName() {
        return tableName;
    }

    public String getScheme() {
        return scheme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private int id;
    private int userId;
    private int eventId;
    private String status;

    public static EventQuickly model() {
        return new EventQuickly();
    }

    @Override
    public synchronized Boolean createTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if (db != null && db.isOpen()) {
            try {
                db.execSQL(scheme);
                error = true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
        }

        return error;
    }

    @Override
    public synchronized Boolean dropTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if (db != null && db.isOpen()) {
            try {
                db.execSQL(String.valueOf(System.out.printf(DROP_TABLE_PATTERN, tableName)));
                error = true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
        }

        return error;
    }

    public static final class ContentProvider {
        public static final String QUERY_PATH = EventQuickly.model().getTableName() + "/";
        public static final int ONE_ROW_CODE = 700;
        public static final String ONE_ROW_PATH = EventQuickly.model().getTableName() + "/#";
        public static final int ALL_ROWS_CODE = 701;
        public static final String ALL_ROWS_PATH = EventQuickly.model().getTableName();
    }

    public static final class Columns implements BaseColumns {
        private Columns() {
        }

        public static final String ID = "id";
        public static final String USER_ID = "userId";
        public static final String EVENT_ID = "eventId";
        public static final String STATUS = "status";
        public static final String STATUS_SYNC = NHSyncModel.Columns.STATUS_SYNC;

        public static final int ID_POSITION = 0;
        public static final int USER_ID_POSITION = 1;
        public static final int EVENT_ID_POSITION = 2;
        public static final int STATUS_POSITION = 3;
        public static final int STATUS_SYNC_POSITION = 4;

        public static final String FULL__ID = EventQuickly.model().getTableName() + "." + _ID;
        public static final String FULL_USER_ID = EventQuickly.model().getTableName() + "." + USER_ID;
        public static final String FULL_EVENT_ID = EventQuickly.model().getTableName() + "." + EVENT_ID;
        public static final String FULL_STATUS = EventQuickly.model().getTableName() + "." + STATUS;
        public static final String FULL_STATUS_SYNC = EventQuickly.model().getTableName() + "." + STATUS_SYNC;

        public static final String[] getColumns() {
            return new String[]{
                    _ID,
                    USER_ID,
                    EVENT_ID,
                    STATUS,
                    STATUS_SYNC
            };
        }
    }
}