package com.nh.quickly.database.models;

import com.nh.quickly.nh.database.models.NHModel;

/**
 * Created by Diego on 16/06/2016.
 */
abstract public class Model extends NHModel {
    public static final String STATUS_ALL = "todo";
    public static final String STATUS_INACTIVE = "inactivo";
    public static final String STATUS_PENDING = "pendiente";
    public static final String STATUS_ACTIVE  = "activo";
    public static final String STATUS_DELETED = "eliminado";
    public static final String STATUS_CANCELED = "cancelado";
    public static final String STATUS_COMPLIMENT = "cumplido";
    public static final String STATUS_SCHEDULED= "agendada";
    public static final String STATUS_NO_SCHEDULED = "desagendada";
}
