package com.nh.quickly.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Country;
import com.nh.quickly.database.models.Department;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.EventQuickly;
import com.nh.quickly.database.models.Release;
import com.nh.quickly.database.models.User;
import com.nh.quickly.nh.database.NHDataBase;
import com.nh.quickly.nh.database.models.NHModel;


/**
 * Created by Diego on 15/06/2016.
 */
final public class DataBase extends NHDataBase {
    public DataBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    public static DataBase getInstanceDb(Context context) {
        if (db == null)
            db = new DataBase(context, context.getResources().getString(R.string.database_name), null, context.getResources().getInteger(R.integer.database_version));

        return db;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Country.model().getScheme());
        db.execSQL(Department.model().getScheme());
        db.execSQL(City.model().getScheme());
        db.execSQL(Entity.model().getScheme());
        db.execSQL(Event.model().getScheme());
        db.execSQL(User.model().getScheme());
        db.execSQL(EventQuickly.model().getScheme());

        db.execSQL(Release.model().getScheme());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int version, int mNewVersion) {
        db.execSQL(String.valueOf(String.format(NHModel.DROP_TABLE_PATTERN, Country.model().getTableName())));
        db.execSQL(String.valueOf(String.format(NHModel.DROP_TABLE_PATTERN, Department.model().getTableName())));
        db.execSQL(String.valueOf(String.format(NHModel.DROP_TABLE_PATTERN, City.model().getTableName())));
        db.execSQL(String.valueOf(String.format(NHModel.DROP_TABLE_PATTERN, Entity.model().getTableName())));
        db.execSQL(String.valueOf(String.format(NHModel.DROP_TABLE_PATTERN, Event.model().getTableName())));
        db.execSQL(String.valueOf(String.format(NHModel.DROP_TABLE_PATTERN, User.model().getTableName())));
        db.execSQL(String.valueOf(String.format(NHModel.DROP_TABLE_PATTERN, EventQuickly.model().getTableName())));
        db.execSQL(String.valueOf(String.format(NHModel.DROP_TABLE_PATTERN, Release.model().getTableName())));

        db.execSQL(Country.model().getScheme());
        db.execSQL(Department.model().getScheme());
        db.execSQL(City.model().getScheme());
        db.execSQL(Entity.model().getScheme());
        db.execSQL(Event.model().getScheme());
        db.execSQL(User.model().getScheme());
        db.execSQL(EventQuickly.model().getScheme());
        db.execSQL(Release.model().getScheme());
    }
}
