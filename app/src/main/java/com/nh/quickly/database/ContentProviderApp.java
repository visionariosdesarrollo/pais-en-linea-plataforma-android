package com.nh.quickly.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Country;
import com.nh.quickly.database.models.Department;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.EventQuickly;
import com.nh.quickly.database.models.Release;
import com.nh.quickly.database.models.User;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.utils.NHArrayUtils;
import com.nh.quickly.nh.utils.NHDataBaseUtils;

/**
 * Created by Diego on 19/05/2016.
 */
final public class ContentProviderApp extends NHContentProvider {
    public static final String AUTHORITY = "com.nh.quickly.provider";

    @Override
    protected void addUris() {
        uriMatcher.addURI(AUTHORITY, Country.ContentProvider.ONE_ROW_PATH, Country.ContentProvider.ONE_ROW_CODE);
        uriMatcher.addURI(AUTHORITY, Country.ContentProvider.ALL_ROWS_PATH, Country.ContentProvider.ALL_ROWS_CODE);
        uriMatcher.addURI(AUTHORITY, Department.ContentProvider.ONE_ROW_PATH, Department.ContentProvider.ONE_ROW_CODE);
        uriMatcher.addURI(AUTHORITY, Department.ContentProvider.ALL_ROWS_PATH, Department.ContentProvider.ALL_ROWS_CODE);
        uriMatcher.addURI(AUTHORITY, City.ContentProvider.ONE_ROW_PATH, City.ContentProvider.ONE_ROW_CODE);
        uriMatcher.addURI(AUTHORITY, City.ContentProvider.ALL_ROWS_PATH, City.ContentProvider.ALL_ROWS_CODE);
        uriMatcher.addURI(AUTHORITY, Entity.ContentProvider.ONE_ROW_PATH, Entity.ContentProvider.ONE_ROW_CODE);
        uriMatcher.addURI(AUTHORITY, Entity.ContentProvider.ALL_ROWS_PATH, Entity.ContentProvider.ALL_ROWS_CODE);
        uriMatcher.addURI(AUTHORITY, Event.ContentProvider.ONE_ROW_PATH, Event.ContentProvider.ONE_ROW_CODE);
        uriMatcher.addURI(AUTHORITY, Event.ContentProvider.ALL_ROWS_PATH, Event.ContentProvider.ALL_ROWS_CODE);
        uriMatcher.addURI(AUTHORITY, User.ContentProvider.ONE_ROW_PATH, User.ContentProvider.ONE_ROW_CODE);
        uriMatcher.addURI(AUTHORITY, User.ContentProvider.ALL_ROWS_PATH, User.ContentProvider.ALL_ROWS_CODE);
        uriMatcher.addURI(AUTHORITY, EventQuickly.ContentProvider.ONE_ROW_PATH, EventQuickly.ContentProvider.ONE_ROW_CODE);
        uriMatcher.addURI(AUTHORITY, EventQuickly.ContentProvider.ALL_ROWS_PATH, EventQuickly.ContentProvider.ALL_ROWS_CODE);
        uriMatcher.addURI(AUTHORITY, Release.ContentProvider.ONE_ROW_PATH, Release.ContentProvider.ONE_ROW_CODE);
        uriMatcher.addURI(AUTHORITY, Release.ContentProvider.ALL_ROWS_PATH, Release.ContentProvider.ALL_ROWS_CODE);
    }


    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    public static int count(Context context, int code, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getReadable(context);
        Cursor cursor = null;

        if (db != null && db.isOpen()) {
            try {
                String[] projection = new String[]{"COUNT(" + BaseColumns._ID + ")"};
                switch (code) {
                    case Country.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(Country.model().getTableName(), projection, selection, selectionArgs, null, null, null, null);
                        break;
                    case Department.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(Department.model().getTableName(), projection, selection, selectionArgs, null, null, null, null);
                        break;
                    case City.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(City.model().getTableName(), projection, selection, selectionArgs, null, null, null, null);
                        break;
                    case Entity.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(Entity.model().getTableName(), projection, selection, selectionArgs, null, null, null, null);
                        break;
                    case Event.ContentProvider.ALL_ROWS_CODE:
                        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
                        sQLiteQueryBuilder.setTables(Event.model().getTableName()
                                + " INNER JOIN " + Entity.model().getTableName() + " ON " + Event.Columns.FULL_ENTITY_ID + "=" + Entity.Columns.FULL__ID
                                + " INNER JOIN " + City.model().getTableName() + " ON " + Entity.Columns.FULL_CITY_ID + "=" + City.Columns.FULL__ID
                                + " LEFT JOIN " + EventQuickly.model().getTableName() + " ON " + Event.Columns.FULL__ID + "=" + EventQuickly.Columns.FULL_EVENT_ID
                        );
                        cursor = sQLiteQueryBuilder.query(db, new String[]{"COUNT(" + Event.Columns.FULL__ID + ")"}, selection, selectionArgs, null, null, null, null);
                        break;
                    case User.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(User.model().getTableName(), projection, selection, selectionArgs, null, null, null, null);
                        break;
                    case EventQuickly.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(EventQuickly.model().getTableName(), projection, selection, selectionArgs, null, null, null, null);
                        break;
                    case Release.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(Release.model().getTableName(), projection, selection, selectionArgs, null, null, null, null);
                        break;
                }

                if (cursor != null) {
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
            } catch (NullPointerException nullPointerException) {
            } catch (IllegalArgumentException illegalArgumentException) {
            } catch (Exception exception) {
            }
        }

        return count;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String id = uri.getLastPathSegment();
        SQLiteDatabase db = DataBase.getInstanceDb(getContext()).getReadable(getContext());
        SQLiteQueryBuilder sQLiteQueryBuilder = new SQLiteQueryBuilder();
        Cursor cursor = null;

        if (db != null && db.isOpen()) {
            try {
                switch (uriMatcher.match(uri)) {
                    case Country.ContentProvider.ONE_ROW_CODE:
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", selection);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        cursor = db.query(Country.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Country.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(Country.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Department.ContentProvider.ONE_ROW_CODE:
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", selection);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        cursor = db.query(Department.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Department.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(Department.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case City.ContentProvider.ONE_ROW_CODE:
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", selection);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        cursor = db.query(City.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case City.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(City.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Entity.ContentProvider.ONE_ROW_CODE:
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", selection);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        cursor = db.query(Entity.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Entity.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(Entity.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Event.ContentProvider.ONE_ROW_CODE:
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(Event.Columns.FULL__ID + "=?", selection);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        sQLiteQueryBuilder.setTables(Event.model().getTableName()
                                + " INNER JOIN " + Entity.model().getTableName() + " ON " + Event.Columns.FULL_ENTITY_ID + "=" + Entity.Columns.FULL__ID
                                + " INNER JOIN " + City.model().getTableName() + " ON " + Entity.Columns.FULL_CITY_ID + "=" + City.Columns.FULL__ID
                                + " LEFT JOIN " + EventQuickly.model().getTableName() + " ON " + Event.Columns.FULL__ID + "=" + EventQuickly.Columns.FULL_EVENT_ID
                        );
                        cursor = sQLiteQueryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Event.ContentProvider.ALL_ROWS_CODE:
                        sQLiteQueryBuilder.setTables(Event.model().getTableName()
                                + " INNER JOIN " + Entity.model().getTableName() + " ON " + Event.Columns.FULL_ENTITY_ID + "=" + Entity.Columns.FULL__ID
                                + " INNER JOIN " + City.model().getTableName() + " ON " + Entity.Columns.FULL_CITY_ID + "=" + City.Columns.FULL__ID
                                + " LEFT JOIN " + EventQuickly.model().getTableName() + " ON " + Event.Columns.FULL__ID + "=" + EventQuickly.Columns.FULL_EVENT_ID
                        );
                        cursor = sQLiteQueryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case User.ContentProvider.ONE_ROW_CODE:
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", selection);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        cursor = db.query(User.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case User.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(User.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case EventQuickly.ContentProvider.ONE_ROW_CODE:
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", selection);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        cursor = db.query(EventQuickly.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case EventQuickly.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(EventQuickly.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Release.ContentProvider.ONE_ROW_CODE:
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", selection);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        cursor = db.query(Release.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                    case Release.ContentProvider.ALL_ROWS_CODE:
                        cursor = db.query(Release.model().getTableName(), projection, selection, selectionArgs, null, null, sortOrder, null);
                        break;
                }
            } catch (NullPointerException nullPointerException) {
            } catch (IllegalArgumentException illegalArgumentException) {
            } catch (Exception exception) {
            }
        }

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        long rowId = 0;
        SQLiteDatabase db = DataBase.getInstanceDb(getContext()).getWritable(getContext());
        String path = "";

        if (db != null && db.isOpen()) {
            try {
                switch (uriMatcher.match(uri)) {
                    case Country.ContentProvider.ALL_ROWS_CODE:
                        path = Country.model().getTableName();
                        rowId = db.insert(path, null, contentValues);
                        break;
                    case Department.ContentProvider.ALL_ROWS_CODE:
                        path = Department.model().getTableName();
                        rowId = db.insert(path, null, contentValues);
                        break;
                    case City.ContentProvider.ALL_ROWS_CODE:
                        path = City.model().getTableName();
                        rowId = db.insert(path, null, contentValues);
                        break;
                    case Entity.ContentProvider.ALL_ROWS_CODE:
                        path = Entity.model().getTableName();
                        rowId = db.insert(path, null, contentValues);
                        break;
                    case Event.ContentProvider.ALL_ROWS_CODE:
                        path = Event.model().getTableName();
                        rowId = db.insert(path, null, contentValues);
                        break;
                    case User.ContentProvider.ALL_ROWS_CODE:
                        path = User.model().getTableName();
                        rowId = db.insert(path, null, contentValues);
                        break;
                    case EventQuickly.ContentProvider.ALL_ROWS_CODE:
                        path = EventQuickly.model().getTableName();
                        rowId = db.insert(path, null, contentValues);
                        break;
                    case Release.ContentProvider.ALL_ROWS_CODE:
                        path = Release.model().getTableName();
                        rowId = db.insert(path, null, contentValues);
                        break;
                }
            } catch (NullPointerException nullPointerException) {
            } catch (IllegalArgumentException illegalArgumentException) {
            } catch (Exception exception) {
            }
        }

        return Uri.parse(String.valueOf(String.format("%s/%d", path, rowId)));
    }

    @Override
    public int delete(Uri uri, String where, String[] selectionArgs) {
        String id = uri.getLastPathSegment();
        SQLiteDatabase db = DataBase.getInstanceDb(getContext()).getWritable(getContext());
        int numberOfRowsAffected = 0;

        if (db != null && db.isOpen()) {
            try {
                switch (uriMatcher.match(uri)) {
                    case Country.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.delete(Country.model().getTableName(), where, selectionArgs);
                        break;
                    case Country.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.delete(Country.model().getTableName(), where, selectionArgs);
                        break;
                    case Department.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.delete(Department.model().getTableName(), where, selectionArgs);
                        break;
                    case Department.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.delete(Department.model().getTableName(), where, selectionArgs);
                        break;
                    case City.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.delete(City.model().getTableName(), where, selectionArgs);
                        break;
                    case City.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.delete(City.model().getTableName(), where, selectionArgs);
                        break;
                    case Entity.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.delete(Entity.model().getTableName(), where, selectionArgs);
                        break;
                    case Entity.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.delete(Entity.model().getTableName(), where, selectionArgs);
                        break;
                    case Event.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.delete(Event.model().getTableName(), where, selectionArgs);
                        break;
                    case Event.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.delete(Event.model().getTableName(), where, selectionArgs);
                        break;
                    case User.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.delete(User.model().getTableName(), where, selectionArgs);
                        break;
                    case User.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.delete(User.model().getTableName(), where, selectionArgs);
                        break;
                    case EventQuickly.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.delete(EventQuickly.model().getTableName(), where, selectionArgs);
                        break;
                    case EventQuickly.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.delete(EventQuickly.model().getTableName(), where, selectionArgs);
                        break;
                    case Release.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.delete(Release.model().getTableName(), where, selectionArgs);
                        break;
                    case Release.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.delete(Release.model().getTableName(), where, selectionArgs);
                        break;
                }
            } catch (NullPointerException nullPointerException) {
            } catch (IllegalArgumentException illegalArgumentException) {
            } catch (Exception exception) {
            }
        }

        return numberOfRowsAffected;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] selectionArgs) {
        String id = uri.getLastPathSegment();
        SQLiteDatabase db = DataBase.getInstanceDb(getContext()).getWritable(getContext());
        int numberOfRowsAffected = 0;

        if (db != null && db.isOpen()) {
            try {
                switch (uriMatcher.match(uri)) {
                    case Country.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.update(Country.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Country.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.update(Country.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Department.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.update(Department.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Department.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.update(Department.model().getTableName(), values, where, selectionArgs);
                        break;
                    case City.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.update(City.model().getTableName(), values, where, selectionArgs);
                        break;
                    case City.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.update(City.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Entity.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.update(Entity.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Entity.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.update(Entity.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Event.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.update(Event.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Event.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.update(Event.model().getTableName(), values, where, selectionArgs);
                        break;
                    case User.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.update(User.model().getTableName(), values, where, selectionArgs);
                        break;
                    case User.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.update(User.model().getTableName(), values, where, selectionArgs);
                        break;
                    case EventQuickly.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.update(EventQuickly.model().getTableName(), values, where, selectionArgs);
                        break;
                    case EventQuickly.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.update(EventQuickly.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Release.ContentProvider.ONE_ROW_CODE:
                        where = NHDataBaseUtils.concatenateWhereWithAnd(BaseColumns._ID + "=?", where);
                        selectionArgs = NHArrayUtils.mergeStringArrays(new String[]{id}, selectionArgs);
                        numberOfRowsAffected = db.update(Release.model().getTableName(), values, where, selectionArgs);
                        break;
                    case Release.ContentProvider.ALL_ROWS_CODE:
                        numberOfRowsAffected = db.update(Release.model().getTableName(), values, where, selectionArgs);
                        break;
                }
            } catch (NullPointerException nullPointerException) {
            } catch (IllegalArgumentException illegalArgumentException) {
            } catch (Exception exception) {
            }
        }

        return numberOfRowsAffected;
    }
}