package com.nh.quickly.database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.nh.quickly.database.DataBase;
import com.nh.quickly.nh.database.NHContentProvider;

/**
 * Created by Diego on 10/12/2015.
 */
public class User extends Model {
    final String tableName = "tbl_mod_user";

    final String scheme = "CREATE TABLE " + tableName + " ("
            + Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + Columns.ROLE_COUNTRY_ID + " INTEGER NOT NULL, "
            + Columns.ROLE_DEPARTMENT_ID + " INTEGER NOT NULL, "
            + Columns.ROLE_CITY_ID + " INTEGER NOT NULL, "
            + Columns.NAME + " TEXT NOT NULL, "
            + Columns.DOCUMENT_TYPE + " TEXT NOT NULL, "
            + Columns.DOCUMENT + " TEXT NOT NULL, "
            + Columns.DOCUMENT_DELIVERY_DATE + " TEXT NOT NULL, "
            + Columns.EMAIL + " TEXT NOT NULL, "
            + Columns.SEX + " TEXT NOT NULL, "
            + Columns.TERRITORY + " TEXT NOT NULL, "
            + Columns.STATUS + " TEXT NOT NULL, "
            + Columns.IMAGE + " TEXT "
            + ");";

    final String primaryKey = Columns._ID;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocument(long document) {
        this.document = document;
    }

    public long getDocument() {
        return document;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setDocumentDeliveryDate(String documentDeliveryDate) { this.documentDeliveryDate = documentDeliveryDate; }

    public String getDocumentDeliveryDate() {
        return documentDeliveryDate;
    }

    public void setRroleCountryId(int roleCountryId) {
        this.roleCountryId = roleCountryId;
    }

    public int getRroleCountryId() {
        return roleCountryId;
    }

    public void setRoleDepartmentId(int roleDepartmentId) { this.roleDepartmentId = roleDepartmentId; }

    public int getRoleDepartmentId() {
        return roleDepartmentId;
    }

    public void setRoleCityId(int cityId) {
        this.roleCityId = roleCityId;
    }

    public int getRoleCityId() {
        return roleCityId;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setTerritory(String territory) {
        this.territory = territory;
    }

    public String getTerritory() {
        return territory;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getTableName() {
        return tableName;
    }

    public String getScheme() {
        return scheme;
    }

    private int id;
    private int roleCountryId;
    private int roleDepartmentId;
    private int roleCityId;
    private String name;
    private String documentType;
    private long document;
    private String documentDeliveryDate;
    private String email;
    private String sex;
    private String territory;
    private String status;
    private String image;
    private Boolean isNew = false;

    public final static String SEX_FEMALE = "female";
    public final static String SEX_MAN = "man";

    public final static String TERRITORY_RURAL = "rural";
    public final static String TERRITORY_URBAN = "urban";

    public static User model(){
        return new User();
    }

    @Override
    public synchronized Boolean createTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(scheme);
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    @Override
    public synchronized Boolean dropTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(String.valueOf(System.out.printf(DROP_TABLE_PATTERN, tableName)));
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    public static final class ContentProvider {
        public static final String QUERY_PATH = User.model().getTableName() + "/";
        public static final int ONE_ROW_CODE = 600;
        public static final String ONE_ROW_PATH = User.model().getTableName() + "/#";
        public static final int ALL_ROWS_CODE = 601;
        public static final String ALL_ROWS_PATH = User.model().getTableName();
    }

    public static final class Columns implements BaseColumns {
        private Columns(){}

        public static final String ID = "id";
        public static final String ROLE_COUNTRY_ID = "roleCountryId";
        public static final String ROLE_DEPARTMENT_ID = "roleDepartmentId";
        public static final String ROLE_CITY_ID = "roleCityId";
        public static final String NAME = "name";
        public static final String DOCUMENT_TYPE = "documentType";
        public static final String DOCUMENT = "document";
        public static final String DOCUMENT_DELIVERY_DATE = "documentDeliveryDate";
        public static final String EMAIL = "email";
        public static final String SEX = "sex";
        public static final String TERRITORY = "territory";
        public static final String STATUS = "status";
        public static final String IMAGE = "image";

        public static final int ID_POSITION = 0;
        public static final int ROLE_COUNTRY_ID_POSITION = 1;
        public static final int ROLE_DEPARTMENT_ID_POSITION = 2;
        public static final int ROLE_CITY_ID_POSITION = 3;
        public static final int NAME_POSITION = 4;
        public static final int DOCUMENT_TYPE_POSITION = 5;
        public static final int DOCUMENT_POSITION = 6;
        public static final int DOCUMENT_DELIVERY_DATE_POSITION = 7;
        public static final int EMAIL_POSITION = 8;
        public static final int SEX_POSITION = 9;
        public static final int TERRITORY_POSITION = 10;
        public static final int STATUS_POSITION = 11;
        public static final int IMAGE_POSITION = 12;

        public static final String DEFAULT_SORT_ORDER = _ID + " DESC";
        public static final String SORT_BY_NAME_ASC  = NAME + " ASC";
        public static final String SORT_BY_NAME_DESC  = NAME + " DESC";

        public static final String[] getColumns() {
            return new String[]{
                    _ID,
                    ROLE_COUNTRY_ID,
                    ROLE_DEPARTMENT_ID,
                    ROLE_CITY_ID,
                    NAME,
                    DOCUMENT_TYPE,
                    DOCUMENT,
                    DOCUMENT_DELIVERY_DATE,
                    EMAIL,
                    SEX,
                    TERRITORY,
                    STATUS,
                    IMAGE
            };
        };
    }
}