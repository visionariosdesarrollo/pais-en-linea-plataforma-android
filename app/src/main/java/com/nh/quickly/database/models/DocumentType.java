package com.nh.quickly.database.models;

import android.content.Context;

import com.nh.quickly.R;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Diego on 10/12/2015.
 */
public class DocumentType extends Model {
    public final static String DOCUMENT_TYPE_CC = "cc";
    public final static String DOCUMENT_TYPE_NIT = "nit";
    public final static String DOCUMENT_TYPE_PASAPORTE = "pasaporte";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private ArrayList<DocumentType> documentTypes = new ArrayList<DocumentType>();

    private String id;
    private String name;

    private DocumentType instantiateDocumentType(String id, String text){
        DocumentType documentType = new DocumentType();
        documentType.setId(id);
        documentType.setName(text);

        return documentType;
    }

    private void getDocumentTypes(Context context) {
        documentTypes.add(instantiateDocumentType(DOCUMENT_TYPE_CC, context.getResources().getString(R.string.user_document_type_cc)));
        documentTypes.add(instantiateDocumentType(DOCUMENT_TYPE_NIT, context.getResources().getString(R.string.user_document_type_nit)));
        documentTypes.add(instantiateDocumentType(DOCUMENT_TYPE_PASAPORTE, context.getResources().getString(R.string.user_document_type_passport)));
    }

    public ArrayList<DocumentType> all(Context context){
        getDocumentTypes(context);

        return documentTypes;
    }

    private DocumentType serachDocumentType(String documentTypeId) {
        DocumentType documentType = new DocumentType();

        if (documentTypes.size() > 0) {
            Iterator<DocumentType> i = documentTypes.iterator();

            while( i.hasNext() ){
                DocumentType itemDocumentType = (DocumentType) i.next();
                if(itemDocumentType.getId() == documentTypeId)
                    documentType = itemDocumentType;
            }
        }

        return documentType;
    }

    public DocumentType one(Context context, String documentTypeId){
        getDocumentTypes(context);

        return serachDocumentType(documentTypeId);
    }

    @Override
    public Boolean createTable(Context context) {
        return null;
    }

    @Override
    public Boolean dropTable(Context context) {
        return null;
    }
}