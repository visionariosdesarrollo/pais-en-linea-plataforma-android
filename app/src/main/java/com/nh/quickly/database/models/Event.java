package com.nh.quickly.database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.nh.quickly.database.DataBase;
import com.nh.quickly.nh.database.models.NHNotificationManagerModel;
import com.nh.quickly.nh.database.models.NHSyncModel;

import java.util.ArrayList;

/**
 * Created by Diego on 10/12/2015.
 */
public class Event extends Model {
    public final static String TAG = Event.class.toString();
    public static final int NOTIFICATION_ID = 100;
    public static final int LIMIT_COMMENT = 8;
    final String tableName = "tbl_mod_event";

    final String scheme = "CREATE TABLE " + tableName + " ("
            + Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + Columns.ENTITY_ID + " INTEGER NOT NULL, "
            + Columns.NAME + " TEXT NOT NULL, "
            + Columns.TYPE + " TEXT, "
            + Columns.EVENT_DATE_START + " TEXT NOT NULL, "
            + Columns.EVENT_TIME_START + " TEXT NOT NULL, "
            + Columns.EVENT_DATE_END + " TEXT NOT NULL, "
            + Columns.EVENT_TIME_END + " TEXT NOT NULL, "
            + Columns.SITE + " TEXT NOT NULL, "
            + Columns.BODY + " TEXT NOT NULL, "
            + Columns.EVENT_TIME_STAMP_START + " LONG NOT NULL, "
            + Columns.EVENT_TIME_STAMP_END + " LONG NOT NULL, "
            + Columns.EVENT_TIME_STAMP_END_LIMIT + " LONG NOT NULL, "
            + Columns.STATUS + " TEXT NOT NULL, "
            + Columns.STATUS_NOTIFICATION_MANAGER + " INTEGER NOT NULL DEFAULT " + NHNotificationManagerModel.STATUS_NEW + ", "
            + Columns.STATUS_SYNC + " INTEGER NOT NULL DEFAULT " + NHSyncModel.STATUS_NEW + " "
            + ");";

    final String primaryKey = Columns._ID;

    public String getTableName() {
        return tableName;
    }

    public String getScheme() {
        return scheme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList getType() {
        return type;
    }

    public void setType(ArrayList type) {
        this.type = type;
    }

    public String getEventDateStart() {
        return eventDateStart;
    }

    public void setEventDateStart(String eventDateStart) {
        this.eventDateStart = eventDateStart;
    }

    public String getEventTimeStart() {
        return eventTimeStart;
    }

    public void setEventTimeStart(String eventTimeStart) {
        this.eventTimeStart = eventTimeStart;
    }

    public String getEventDateEnd() {
        return eventDateEnd;
    }

    public void setEventDateEnd(String eventDateEnd) {
        this.eventDateEnd = eventDateEnd;
    }

    public String getEventTimeEnd() {
        return eventTimeEnd;
    }

    public void setEventTimeEnd(String eventTimeEnd) {
        this.eventTimeEnd = eventTimeEnd;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getEventTimeStampStart() {
        return eventTimeStampStart;
    }

    public void setEventTimeStampStart(long eventTimeStampStart) {
        this.eventTimeStampStart = eventTimeStampStart;
    }

    public long getEventTimeStampEnd() {
        return eventTimeStampEnd;
    }

    public void setEventTimeStampEnd(long eventTimeStampEnd) {
        this.eventTimeStampEnd = eventTimeStampEnd;
    }

    public long getEventTimeStampEndLimit() {
        return eventTimeStampEndLimit;
    }

    public void setEventTimeStampEndLimit(long eventTimeStampEndLimit) {
        this.eventTimeStampEndLimit = eventTimeStampEndLimit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private int id;
    private int entityId;
    private String name;
    private ArrayList type;
    private String eventDateStart;
    private String eventTimeStart;
    private String eventDateEnd;
    private String eventTimeEnd;
    private String site;
    private String body;
    private long eventTimeStampStart;
    private long eventTimeStampEnd;
    private long eventTimeStampEndLimit;
    private String status;

    public static Event model() {
        return new Event();
    }

    @Override
    public synchronized Boolean createTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if (db != null && db.isOpen()) {
            try {
                db.execSQL(scheme);
                error = true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
        }

        return error;
    }

    @Override
    public synchronized Boolean dropTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if (db != null && db.isOpen()) {
            try {
                db.execSQL(String.valueOf(System.out.printf(DROP_TABLE_PATTERN, tableName)));
                error = true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                db.close();
            }
        }

        return error;
    }

    public static final class ContentProvider {
        public static final String QUERY_PATH = Event.model().getTableName() + "/";
        public static final int ONE_ROW_CODE = 500;
        public static final String ONE_ROW_PATH = Event.model().getTableName() + "/#";
        public static final int ALL_ROWS_CODE = 501;
        public static final String ALL_ROWS_PATH = Event.model().getTableName();
    }

    public static final class Columns implements BaseColumns {
        private Columns() {
        }

        public static final String ID = "id";
        public static final String ENTITY_ID = "entityId";
        public static final String NAME = "name";
        public static final String TYPE = "type";
        public static final String EVENT_DATE_START = "eventDateStart";
        public static final String EVENT_TIME_START = "eventTimeStart";
        public static final String EVENT_DATE_END = "eventDateEnd";
        public static final String EVENT_TIME_END = "eventTimeEnd";
        public static final String SITE = "site";
        public static final String BODY = "body";
        public static final String EVENT_TIME_STAMP_START = "eventTimeStampStart";
        public static final String EVENT_TIME_STAMP_END = "eventTimeStampEnd";
        public static final String EVENT_TIME_STAMP_END_LIMIT = "eventTimeStampEndLimit";
        public static final String STATUS = "status";
        public static final String STATUS_NOTIFICATION_MANAGER = NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER;
        public static final String STATUS_SYNC = NHSyncModel.Columns.STATUS_SYNC;

        public static final int ID_POSITION = 0;
        public static final int ENTITY_ID_POSITION = 1;
        public static final int NAME_POSITION = 2;
        public static final int TYPE_POSITION = 3;
        public static final int EVENT_DATE_START_POSITION = 4;
        public static final int EVENT_TIME_START_POSITION = 5;
        public static final int EVENT_DATE_END_POSITION = 6;
        public static final int EVENT_TIME_POSITION = 7;
        public static final int SITE_POSITION = 8;
        public static final int BODY_POSITION = 9;
        public static final int EVENT_TIME_STAMP_START_POSITION = 10;
        public static final int EVENT_TIME_STAMP_END_POSITION = 11;
        public static final int EVENT_TIME_STAMP_END_LIMIT_POSITION = 12;
        public static final int STATUS_POSITION = 13;
        public static final int STATUS_NOTIFICATION_MANAGER_POSITION = 14;
        public static final int STATUS_SYNC_POSITION = 15;

        public static final String FULL__ID = Event.model().getTableName() + "." + _ID;
        public static final String FULL_ENTITY_ID = Event.model().getTableName() + "." + ENTITY_ID;
        public static final String FULL_NAME = Event.model().getTableName() + "." + NAME;
        public static final String FULL_TYPE = Event.model().getTableName() + "." + TYPE;
        public static final String FULL_EVENT_DATE_START = Event.model().getTableName() + "." + EVENT_DATE_START;
        public static final String FULL_EVENT_TIME_START = Event.model().getTableName() + "." + EVENT_TIME_START;
        public static final String FULL_EVENT_DATE_END = Event.model().getTableName() + "." + EVENT_DATE_END;
        public static final String FULL_EVENT_TIME_END = Event.model().getTableName() + "." + EVENT_TIME_END;
        public static final String FULL_SITE = Event.model().getTableName() + "." + SITE;
        public static final String FULL_BODY = Event.model().getTableName() + "." + BODY;
        public static final String FULL_EVENT_TIME_STAMP_START = Event.model().getTableName() + "." + EVENT_TIME_STAMP_START;
        public static final String FULL_EVENT_TIME_STAMP_END = Event.model().getTableName() + "." + EVENT_TIME_STAMP_END;
        public static final String FULL_EVENT_TIME_STAMP_END_LIMIT = Event.model().getTableName() + "." + EVENT_TIME_STAMP_END_LIMIT;
        public static final String FULL_STATUS = Event.model().getTableName() + "." + STATUS;
        public static final String FULL_STATUS_NOTIFICATION_MANAGER = Event.model().getTableName() + "." + STATUS_NOTIFICATION_MANAGER;
        public static final String FULL_STATUS_SYNC = Event.model().getTableName() + "." + STATUS_SYNC;

        public static final String ENTITY_FULL__ID = Entity.Columns.FULL__ID + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.ID;
        public static final String ENTITY_FULL_CITY_ID = Entity.Columns.FULL_CITY_ID + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.CITY_ID;
        public static final String ENTITY_FULL_NAME = Entity.Columns.FULL_NAME + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.NAME;
        public static final String ENTITY_FULL_DESCRIPTION = Entity.Columns.FULL_DESCRIPTION + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.DESCRIPTION;
        public static final String ENTITY_FULL_EMAIL = Entity.Columns.FULL_EMAIL + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.EMAIL;
        public static final String ENTITY_FULL_PHONE_ONE = Entity.Columns.FULL_PHONE_ONE + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.PHONE_ONE;
        public static final String ENTITY_FULL_PHONE_TWO = Entity.Columns.FULL_PHONE_TWO + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.PHONE_TWO;
        public static final String ENTITY_FULL_IMAGE = Entity.Columns.FULL_IMAGE + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.IMAGE;
        public static final String ENTITY_FULL_STATUS = Entity.Columns.FULL_STATUS + " AS " + Entity.model().getTableName() + "_" + Entity.Columns.STATUS;

        public static final int ENTITY_FULL__ID_POSITION = 16;
        public static final int ENTITY_FULL_CITY_ID_POSITION = 17;
        public static final int ENTITY_FULL_NAME_POSITION = 18;
        public static final int ENTITY_FULL_DESCRIPTION_POSITION = 19;
        public static final int ENTITY_FULL_EMAIL_POSITION = 20;
        public static final int ENTITY_FULL_PHONE_ONE_POSITION = 21;
        public static final int ENTITY_FULL_PHONE_TWO_POSITION = 22;
        public static final int ENTITY_FULL_IMAGE_POSITION = 23;
        public static final int ENTITY_FULL_STATUS_POSITION = 24;

        public static final String EVENT_QUICKLY_FULL__ID = EventQuickly.Columns.FULL__ID + " AS " + EventQuickly.model().getTableName() + "_" + EventQuickly.Columns.ID;
        public static final String EVENT_QUICKLY_FULL_USER_ID = EventQuickly.Columns.FULL_USER_ID + " AS " + EventQuickly.model().getTableName() + "_" + EventQuickly.Columns.USER_ID;
        public static final String EVENT_QUICKLY_FULL_EVENT_ID = EventQuickly.Columns.FULL_EVENT_ID + " AS " + EventQuickly.model().getTableName() + "_" + EventQuickly.Columns.EVENT_ID;
        public static final String EVENT_QUICKLY_FULL_STATUS = EventQuickly.Columns.FULL_STATUS + " AS " + EventQuickly.model().getTableName() + "_" + EventQuickly.Columns.STATUS;
        public static final String EVENT_QUICKLY_FULL_STATUS_SYNC = EventQuickly.Columns.FULL_STATUS_SYNC + " AS " + EventQuickly.model().getTableName() + "_" + EventQuickly.Columns.STATUS_SYNC;

        public static final int EVENT_QUICKLY_FULL__ID_POSITION = 25;
        public static final int EVENT_QUICKLY_FULL_USER_ID_POSITION = 26;
        public static final int EVENT_QUICKLY_FULL_EVENT_ID_POSITION = 27;
        public static final int EVENT_QUICKLY_FULL_STATUS_POSITION = 28;
        public static final int EVENT_QUICKLY_FULL_STATUS_SYNC_POSITION = 29;

        public static final String SORT_BY_NAME_ASC = NAME + " ASC";
        public static final String SORT_BY_NAME_DESC = NAME + " DESC";
        public static final String EVENT_TIME_STAMP_START_ASC = EVENT_TIME_STAMP_START + " ASC";
        public static final String EVENT_TIME_STAMP_START_DESC = EVENT_TIME_STAMP_START + " DESC";

        public static final String SORT_BY_FULL_NAME_ASC = FULL_NAME + " ASC";
        public static final String SORT_BY_FULL_NAME_DESC = FULL_NAME + " DESC";
        public static final String SORT_BY_FULL_EVENT_TIME_STAMP_START_ASC = FULL_EVENT_TIME_STAMP_START + " ASC";
        public static final String SORT_BY_FULL_EVENT_TIME_STAMP_START_DESC = FULL_EVENT_TIME_STAMP_START + " DESC";

        public static final String[] getColumns() {
            return new String[]{
                    _ID,
                    ENTITY_ID,
                    NAME,
                    TYPE,
                    EVENT_DATE_START,
                    EVENT_TIME_START,
                    EVENT_DATE_END,
                    EVENT_TIME_END,
                    SITE,
                    BODY,
                    EVENT_TIME_STAMP_START,
                    EVENT_TIME_STAMP_END,
                    EVENT_TIME_STAMP_END_LIMIT,
                    STATUS,
                    STATUS_NOTIFICATION_MANAGER,
                    STATUS_SYNC
            };
        }

        public static final String[] getFullColumns() {
            return new String[]{
                    FULL__ID,
                    FULL_ENTITY_ID,
                    FULL_NAME,
                    FULL_TYPE,
                    FULL_EVENT_DATE_START,
                    FULL_EVENT_TIME_START,
                    FULL_EVENT_DATE_END,
                    FULL_EVENT_TIME_END,
                    FULL_SITE,
                    FULL_BODY,
                    FULL_EVENT_TIME_STAMP_START,
                    FULL_EVENT_TIME_STAMP_END,
                    FULL_EVENT_TIME_STAMP_END_LIMIT,
                    FULL_STATUS,
                    FULL_STATUS_NOTIFICATION_MANAGER,
                    FULL_STATUS_SYNC
            };
        }

        public static final String[] getFullColumnsEntity() {
            return new String[]{
                    ENTITY_FULL__ID,
                    ENTITY_FULL_CITY_ID,
                    ENTITY_FULL_NAME,
                    ENTITY_FULL_DESCRIPTION,
                    ENTITY_FULL_EMAIL,
                    ENTITY_FULL_PHONE_ONE,
                    ENTITY_FULL_PHONE_TWO,
                    ENTITY_FULL_IMAGE,
                    ENTITY_FULL_STATUS
            };
        }

        public static final String[] getFullColumnsEventQuickly() {
            return new String[]{
                    EVENT_QUICKLY_FULL__ID,
                    EVENT_QUICKLY_FULL_USER_ID,
                    EVENT_QUICKLY_FULL_EVENT_ID,
                    EVENT_QUICKLY_FULL_STATUS,
                    EVENT_QUICKLY_FULL_STATUS_SYNC
            };
        }
    }
}