package com.nh.quickly.database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.nh.quickly.database.DataBase;
import com.nh.quickly.nh.database.NHContentProvider;

/**
 * Created by Diego on 10/12/2015.
 */
public class Entity extends Model {
    final String tableName = "tbl_mod_entity";

    final String scheme = "CREATE TABLE " + tableName + " ("
            + Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + Columns.CITY_ID + " INTEGER NOT NULL, "
            + Columns.NAME + " TEXT NOT NULL, "
            + Columns.DESCRIPTION + " TEXT, "
            + Columns.EMAIL + " TEXT NOT NULL, "
            + Columns.PHONE_ONE + " TEXT NOT NULL, "
            + Columns.PHONE_TWO + " TEXT, "
            + Columns.IMAGE + " TEXT, "
            + Columns.STATUS + " TEXT NOT NULL "
            + ");";

    final String primaryKey = Columns._ID;

    public String getTableName() {
        return tableName;
    }

    public String getScheme() {
        return scheme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneOne() {
        return phoneOne;
    }

    public void setPhoneOne(String phoneOne) {
        this.phoneOne = phoneOne;
    }

    public String getPhoneTwo() {
        return phoneTwo;
    }

    public void setPhoneTwo(String phoneTwo) {
        this.phoneTwo = phoneTwo;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private int id;
    private int cityId;
    private String name;
    private String description;
    private String email;
    private String phoneOne;
    private String phoneTwo;
    private String image;
    private String status;

    public static Entity model(){
        return new Entity();
    }

    @Override
    public synchronized Boolean createTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(scheme);
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    @Override
    public synchronized Boolean dropTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(String.valueOf(System.out.printf(DROP_TABLE_PATTERN, tableName)));
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    public static final class ContentProvider {
        public static final String QUERY_PATH = Entity.model().getTableName() + "/";
        public static final int ONE_ROW_CODE = 400;
        public static final String ONE_ROW_PATH = Entity.model().getTableName() + "/#";
        public static final int ALL_ROWS_CODE = 401;
        public static final String ALL_ROWS_PATH = Entity.model().getTableName();
    }

    public static final class Columns implements BaseColumns {
        private Columns(){}

        public static final String ID = "id";
        public static final String CITY_ID = "cityId";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String EMAIL = "email";
        public static final String PHONE_ONE = "phoneOne";
        public static final String PHONE_TWO = "phoneTwo";
        public static final String IMAGE = "image";
        public static final String STATUS = "status";

        public static final String FULL__ID = Entity.model().getTableName() + "." + _ID;
        public static final String FULL_CITY_ID = Entity.model().getTableName() + "." + CITY_ID;
        public static final String FULL_NAME = Entity.model().getTableName() + "." + NAME;
        public static final String FULL_DESCRIPTION = Entity.model().getTableName() + "." + DESCRIPTION;
        public static final String FULL_EMAIL = Entity.model().getTableName() + "." + EMAIL;
        public static final String FULL_PHONE_ONE = Entity.model().getTableName() + "." + PHONE_ONE;
        public static final String FULL_PHONE_TWO = Entity.model().getTableName() + "." + PHONE_TWO;
        public static final String FULL_IMAGE = Entity.model().getTableName() + "." + IMAGE;
        public static final String FULL_STATUS = Entity.model().getTableName() + "." + STATUS;

        public static final int ID_POSITION = 0;
        public static final int CITY_ID_POSITION = 1;
        public static final int NAME_POSITION = 2;
        public static final int DESCRIPTION_POSITION = 3;
        public static final int EMAIL_POSITION = 4;
        public static final int PHONE_ONE_POSITION = 5;
        public static final int PHONE_TWO_POSITION = 6;
        public static final int IMAGE_POSITION = 7;
        public static final int STATUS_POSITION = 8;

        public static final String DEFAULT_SORT_ORDER = _ID + " DESC";
        public static final String SORT_BY_NAME_ASC  = NAME + " ASC";
        public static final String SORT_BY_NAME_DESC  = NAME + " DESC";

        public static final String[] getColumns() {
            return new String[]{
                    _ID,
                    CITY_ID,
                    NAME,
                    DESCRIPTION,
                    EMAIL,
                    PHONE_ONE,
                    PHONE_TWO,
                    IMAGE,
                    STATUS
            };
        };

        public static final String[] getFullColumns() {
            return new String[]{
                    FULL__ID,
                    FULL_CITY_ID,
                    FULL_NAME,
                    FULL_DESCRIPTION,
                    FULL_EMAIL,
                    FULL_PHONE_ONE,
                    FULL_PHONE_TWO,
                    FULL_IMAGE,
                    FULL_STATUS
            };
        };
    }
}