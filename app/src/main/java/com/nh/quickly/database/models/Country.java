package com.nh.quickly.database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.nh.quickly.database.DataBase;
import com.nh.quickly.nh.database.NHContentProvider;

/**
 * Created by Diego on 10/12/2015.
 */
public class Country extends Model {
    final String tableName = "tbl_sys_config_country";

    final String scheme = "CREATE TABLE " + tableName + " ("
            + Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + Columns.NAME + " TEXT NOT NULL, "
            + Columns.CODE + " TEXT NOT NULL "
            + ");";

    final String primaryKey = Columns._ID;

    public String getTableName() {
        return tableName;
    }

    public String getScheme() {
        return scheme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private int id;
    private String name;
    private String code;

    public static Country model(){
        return new Country();
    }

    @Override
    public synchronized Boolean createTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(scheme);
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    @Override
    public synchronized Boolean dropTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(String.valueOf(System.out.printf(DROP_TABLE_PATTERN, tableName)));
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    public static final class ContentProvider {
        public static final String QUERY_PATH = Country.model().getTableName() + "/";
        public static final int ONE_ROW_CODE = 100;
        public static final String ONE_ROW_PATH = Country.model().getTableName() + "/#";
        public static final int ALL_ROWS_CODE = 101;
        public static final String ALL_ROWS_PATH = Country.model().getTableName();
    }

    public static final class Columns implements BaseColumns {
        private Columns(){}

        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CODE = "code";

        public static final int ID_POSITION = 0;
        public static final int NAME_POSITION = 1;
        public static final int CODE_POSITION = 2;

        public static final String DEFAULT_SORT_ORDER = _ID + " DESC";
        public static final String SORT_BY_NAME_ASC  = NAME + " ASC";
        public static final String SORT_BY_NAME_DESC  = NAME + " DESC";

        public static final String[] getColumns() {
            return new String[]{
                    _ID,
                    NAME,
                    CODE
            };
        };
    }
}