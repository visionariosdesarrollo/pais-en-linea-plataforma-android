package com.nh.quickly.database.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;

import com.nh.quickly.database.DataBase;
import com.nh.quickly.nh.database.NHContentProvider;

/**
 * Created by Diego on 10/12/2015.
 */
public class City extends Model {
    final String tableName = "tbl_sys_config_country_department_city";

    final String scheme = "CREATE TABLE " + tableName + " ("
            + Columns._ID + " INTEGER PRIMARY KEY NOT NULL, "
            + Columns.DEPARTMENT_ID + " TEXT NOT NULL, "
            + Columns.NAME + " TEXT NOT NULL, "
            + Columns.DESCRIPTION + " TEXT, "
            + Columns.IMAGE + " TEXT NOT NULL "
            + ");";

    final String primaryKey = Columns._ID;

    public String getTableName() {
        return tableName;
    }

    public String getScheme() {
        return scheme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private int id;
    private int departmentId;
    private String name;
    private String description;
    private String image;

    public City(int id, int departmentId, @Nullable String name, @Nullable String description, @Nullable String image){
        if(id > 0){
            this.id = id;
            this.departmentId = departmentId;
            this.name = name;
            this.description = description;
            this.image = image;
        }
    }

    public static City model(){
        return new City(0, 0, null, null, null);
    }

    @Override
    public synchronized Boolean createTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(scheme);
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    @Override
    public synchronized Boolean dropTable(Context context) {
        Boolean error = false;
        SQLiteDatabase db = DataBase.getInstanceDb(context).getWritable(context);

        if(db != null && db.isOpen())
        {
            try {
                db.execSQL(String.valueOf(System.out.printf(DROP_TABLE_PATTERN, tableName)));
                error = true;
            } catch (Exception e){
                e.printStackTrace();
            }
            finally {
                db.close();
            }
        }

        return error;
    }

    public static final class ContentProvider {
        public static final String QUERY_PATH = City.model().getTableName() + "/";
        public static final int ONE_ROW_CODE = 300;
        public static final String ONE_ROW_PATH = City.model().getTableName() + "/#";
        public static final int ALL_ROWS_CODE = 301;
        public static final String ALL_ROWS_PATH = City.model().getTableName();
    }

    public static final class Columns implements BaseColumns {
        private Columns(){}

        public static final String ID = "id";
        public static final String DEPARTMENT_ID = "departmentId";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
        public static final String IMAGE = "image";

        public static final int ID_POSITION = 0;
        public static final int DEPARTMENT_ID_POSITION = 1;
        public static final int NAME_POSITION = 2;
        public static final int DESCRIPTION_POSITION = 3;
        public static final int IMAGE_POSITION = 4;

        public static final String FULL__ID = City.model().getTableName() + "." + _ID;
        public static final String FULL_DEPARTMENT_ID = City.model().getTableName() + "." + DEPARTMENT_ID;
        public static final String FULL_NAME = City.model().getTableName() + "." + NAME;
        public static final String FULL_DESCRIPTION = City.model().getTableName() + "." + DESCRIPTION;
        public static final String FULL_IMAGE = City.model().getTableName() + "." + IMAGE;

        public static final String DEFAULT_SORT_ORDER = _ID + " DESC";
        public static final String SORT_BY_NAME_ASC  = NAME + " ASC";
        public static final String SORT_BY_NAME_DESC  = NAME + " DESC";

        public static final String[] getColumns() {
            return new String[]{
                    _ID,
                    DEPARTMENT_ID,
                    NAME,
                    DESCRIPTION,
                    IMAGE
            };
        };
    }
}