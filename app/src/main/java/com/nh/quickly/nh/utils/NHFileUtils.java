package com.nh.quickly.nh.utils;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.nh.os.NHFileSaveInStorageAsyncTask;

import java.io.File;

/**
 * Created by Diego on 02/07/2016.
 */
public class NHFileUtils {
    @Nullable
    public static File cacheDir(Context context, String directory) {
        File cacheDir;
        try {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
                cacheDir = new File(Environment.getExternalStorageDirectory(), directory);
            else
                cacheDir = context.getCacheDir();

            if (!cacheDir.exists())
                cacheDir.mkdirs();
        }
        catch (Exception exception) {
            cacheDir = null;
        }

        return cacheDir;
    }

    public static void saveInStorage(Context context, String url, String directory, String file, @Nullable Handler handler) {
        if(!existFile(context, directory, file))
            new NHFileSaveInStorageAsyncTask(context, null).execute(new String[]{url, directory, file});
    }

    public static boolean existFile(Context context, String directory, String file){
        File fileExists = new File(NHFileUtils.cacheDir(context.getApplicationContext(), directory), file);
        return fileExists.exists();
    }
}
