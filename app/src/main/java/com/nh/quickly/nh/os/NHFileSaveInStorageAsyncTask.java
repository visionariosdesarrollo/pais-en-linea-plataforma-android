package com.nh.quickly.nh.os;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.nh.utils.NHFileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;

/**
 * Created by Diego on 02/07/2016.
 */
public class NHFileSaveInStorageAsyncTask extends AsyncTask<String, String, String> {
    Context context;
    Handler handler;

    public NHFileSaveInStorageAsyncTask(Context context, @Nullable Handler handler) {
        this.context = context;
        this.handler = handler;
    }

    @Override
    protected String doInBackground(String... params) {
        if(!TextUtils.isEmpty(params[0])
                && !TextUtils.isEmpty(params[1])
                    && !TextUtils.isEmpty(params[2])) {
            Log.d(App.LOG_TAG, "NH::NHFileSaveInStorageAsyncTask::doInBackground::if");
            getBitmap(params[0], params[1], params[2]);
        } else
            cancel(true);

        return null;
    }

    synchronized void getBitmap(String url, String directory, String fileName) {
        try {
            File file = new File(NHFileUtils.cacheDir(context.getApplicationContext(), directory), fileName);
            if (!file.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
                if (bitmap == null) {
                    try {
                        bitmap = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
                        saveFile(bitmap, file);
                    } catch (FileNotFoundException fileNotFoundException) {
                    } catch (Exception exception) {}
                }
            }
        }
        catch (Exception exception) {}
    }

    void saveFile(Bitmap bitmap, File file) {
        FileOutputStream fileOutputStream = null;
        try {
            Log.d(App.LOG_TAG, "NH::NHFileSaveInStorageAsyncTask::doInBackground::saveFile::1");
            fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 80, fileOutputStream);
            Log.d(App.LOG_TAG, "NH::NHFileSaveInStorageAsyncTask::doInBackground::saveFile::2");
        }
        catch (Exception e) {
            Log.d(App.LOG_TAG, "NH::NHFileSaveInStorageAsyncTask::doInBackground::saveFile::3::" + e.getMessage());
        }
        finally {
            try {
                if (fileOutputStream != null)
                    fileOutputStream.close();
            } catch (Exception exception) {}
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if(handler != null)
            handler.sendEmptyMessage(0);
    }
}
