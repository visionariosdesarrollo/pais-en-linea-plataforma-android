package com.nh.quickly.nh.sync;

import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.Context;
import android.os.Build;

/**
 * Created by Diego on 16/06/2016.
 */
abstract public class NHAbstractThreadedSyncAdapter extends AbstractThreadedSyncAdapter {
    public NHAbstractThreadedSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }
}
