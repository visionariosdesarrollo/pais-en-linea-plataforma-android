package com.nh.quickly.nh.sync.volley;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.util.LruCache;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Diego on 25/05/2016.
 */
public class NHVolley implements NHVolleyInterface {
    static NHVolley volley = null;
    RequestQueue requestQueue;
    ImageLoader imageLoader;
    int cacheSize = 4 * 1024 * 1024; // 4MiB

    NHVolley(Context context) {
        requestQueue = Volley.newRequestQueue(context);
        imageLoader();
    }

    public static synchronized NHVolley getInstance(Context context) {
        if (volley == null)
            volley = new NHVolley(context);
        return volley;
    }

    void imageLoader(){
        imageLoader = new ImageLoader(requestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(cacheSize);

            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                synchronized (cache) {
                    if (cache.get(url) == null) {
                        cache.put(url, bitmap);
                    }
                }
            }
        });
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    @Override
    public NHVolley addToQueue(Request request, @Nullable String tag) {
        if(request != null) {
            request.setTag(tag);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    60000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            requestQueue.add(request);
        }

        return volley;
    }

    @Override
    public void cancellAllRequestQueue(@Nullable String tag) {
        try {
            if (requestQueue != null)
                requestQueue.cancelAll(tag);
        }catch (IllegalArgumentException illegalArgumentException){}
    }
}