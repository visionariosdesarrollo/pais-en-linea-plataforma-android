package com.nh.quickly.nh.utils.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Diego on 10/10/2016.
 */

public class NHString extends NHValidator {
    private static String expression = "^[A-Za-z, ,á,é,í,ó,ú,Á,É,Í,Ó,Ú]+$";

    public static boolean isString(String string) {
        boolean isValid = false;
        CharSequence inputStr = string;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}