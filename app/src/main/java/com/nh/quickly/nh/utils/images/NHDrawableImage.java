package com.nh.quickly.nh.utils.images;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;

import com.nh.quickly.App;

/**
 * Created by Diego on 21/06/2016.
 */
abstract public class NHDrawableImage extends Drawable{
    final Bitmap bitmap;
    final Paint paint;
    final RectF rectF;
    int bitmapWidth;
    int bitmapHeight;

    abstract String getType();

    public NHDrawableImage(Bitmap bitmap, @Nullable int bitmapWidth, @Nullable int bitmapHeight) {
        this.bitmap = bitmap;
        rectF = new RectF();
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);

        final BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        paint.setShader(shader);

        if(bitmapWidth == 0)
            bitmapWidth = bitmap.getWidth();

        if(bitmapHeight == 0)
            bitmapHeight = bitmap.getHeight();

        if(getType() == NHRoundImage.TAG){
            if(bitmapWidth != bitmapHeight)
                bitmapHeight = bitmapWidth;
        } else if(getType() == NHRectangleImage.TAG){
            if(bitmapWidth != bitmapHeight)
                bitmapHeight = bitmapWidth;
        }

        this.bitmapWidth = bitmapWidth;
        this.bitmapHeight = bitmapHeight;
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        rectF.set(bounds);
    }

    @Override
    public void setAlpha(int alpha) {
        if (paint.getAlpha() != alpha) {
            paint.setAlpha(alpha);
            invalidateSelf();
        }
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        paint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSPARENT;
    }

    @Override
    public int getIntrinsicWidth() {
        return bitmapWidth;
    }

    @Override
    public int getIntrinsicHeight() {
        return bitmapHeight;
    }

    public void setAntiAlias(boolean aa) {
        paint.setAntiAlias(aa);
        invalidateSelf();
    }

    @Override
    public void setFilterBitmap(boolean filter) {
        paint.setFilterBitmap(filter);
        invalidateSelf();
    }

    @Override
    public void setDither(boolean dither) {
        paint.setDither(dither);
        invalidateSelf();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
