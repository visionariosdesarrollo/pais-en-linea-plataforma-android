package com.nh.quickly.nh.database;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.nh.quickly.database.ContentProviderApp;

abstract public class NHContentProvider extends ContentProvider {
    public static final String SCHEME_PATTERN = "content://%s/";
    public static final String SCHEME = String.format(SCHEME_PATTERN, ContentProviderApp.AUTHORITY);

    protected UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    protected NHContentProvider() {
        super();
        addUris();
    }

    protected abstract void addUris();
}