package com.nh.quickly.nh.database.models;

import android.content.Context;

interface NHModelInterface {
    Boolean createTable(Context context);
    Boolean dropTable(Context context);
}