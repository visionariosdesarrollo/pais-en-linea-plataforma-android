package com.nh.quickly.nh.widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

/**
 * Created by Diego on 08/06/2016.
 */
public class NHProgressBarIndeterminate implements NHProgressBarInterface {
    public final static int CENTER_ON_WINDOW = 1;

    Context context;
    RelativeLayout parentView;
    ProgressBar progressBar;
    int animationDrawable;
    int position = CENTER_ON_WINDOW;

    public int getAnimationDrawable() {
        return animationDrawable;
    }

    public void setAnimationDrawable(int animationDrawable) {
        this.animationDrawable = animationDrawable;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public NHProgressBarIndeterminate(Context context, RelativeLayout parentView){
        this.parentView = parentView;
        this.context = context;
    }

    public void createProgressBar() {
        if(progressBar == null) {
            progressBar = new ProgressBar(context);
            progressBar.setIndeterminate(true);
            progressBar.setVisibility(View.INVISIBLE);

            if (animationDrawable > 0) {
                Drawable drawable = context.getResources().getDrawable(animationDrawable);
                progressBar.setIndeterminateDrawable(drawable);
            }

            position();
        }
    }

    public void position() {
        try {
            switch (position) {
                default:
                    RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(ViewPager.LayoutParams.WRAP_CONTENT, ViewPager.LayoutParams.WRAP_CONTENT);
                    relativeLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                    parentView.addView(progressBar, relativeLayoutParams);
                    break;
            }
        } catch (NullPointerException nullPointerException) {}
    }

    public void visible() {
        createProgressBar();
        progressBar.setVisibility(View.VISIBLE);
    }

    public void invisible() {
        createProgressBar();
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void toggle() {
        if (progressBar.getVisibility() == View.INVISIBLE)
            invisible();
        else
            visible();
    }
}