package com.nh.quickly.nh.widgets;

/**
 * Created by Diego on 10/06/2016.
 */
public interface NHProgressBarInterface {
    int getAnimationDrawable();
    void setAnimationDrawable(int animationDrawable);
    int getPosition();
    void setPosition(int position);
    void createProgressBar();
    void position();
    void visible();
    void invisible();
    void toggle();
}
