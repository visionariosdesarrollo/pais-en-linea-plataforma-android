package com.nh.quickly.nh.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Diego on 16/06/2016.
 */
public interface NHDataBaseInterface {
    SQLiteDatabase getWritable(Context context);
    SQLiteDatabase getReadable(Context context);
    void onCreate(SQLiteDatabase db);
    void onUpgrade(SQLiteDatabase db, int version, int mNewVersion);
}
