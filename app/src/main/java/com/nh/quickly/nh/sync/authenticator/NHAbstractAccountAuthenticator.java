package com.nh.quickly.nh.sync.authenticator;

import android.accounts.AbstractAccountAuthenticator;
import android.content.Context;

/**
 * Created by Diego on 16/06/2016.
 */
public abstract class NHAbstractAccountAuthenticator extends AbstractAccountAuthenticator {
    public NHAbstractAccountAuthenticator(Context context) {
        super(context);
    }
}
