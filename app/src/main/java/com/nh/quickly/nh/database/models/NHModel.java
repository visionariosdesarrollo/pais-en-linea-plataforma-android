package com.nh.quickly.nh.database.models;

abstract public class NHModel implements NHModelInterface{
    public static final String DROP_TABLE_PATTERN = "DROP TABLE IF EXISTS %s;";
}