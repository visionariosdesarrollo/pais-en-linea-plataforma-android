package com.nh.quickly.nh.sync.volley;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import com.android.volley.toolbox.NetworkImageView;
import com.nh.quickly.App;
import com.nh.quickly.nh.utils.images.NHRoundImage;

/**
 * Created by Diego on 02/07/2016.
 */
public class NHRoundNetworkImageView extends NetworkImageView {

    public NHRoundNetworkImageView(Context context) {
        super(context);
    }

    public NHRoundNetworkImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NHRoundNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        if(bm==null) return;

        NHRoundImage roundedImage = new NHRoundImage(bm, 0, 0);
        setImageDrawable(roundedImage);
    }
}
