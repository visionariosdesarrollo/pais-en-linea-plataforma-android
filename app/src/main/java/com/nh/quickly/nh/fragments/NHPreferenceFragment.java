package com.nh.quickly.nh.fragments;

import android.support.v4.preference.PreferenceFragment;
import android.view.View;
import android.widget.RelativeLayout;

import com.nh.quickly.R;
import com.nh.quickly.nh.sync.volley.NHVolley;
import com.nh.quickly.nh.widgets.NHProgressBarIndeterminate;

/**
 * Created by Diego on 30/06/2016.
 */
abstract public class NHPreferenceFragment extends PreferenceFragment implements NHFragmentInterface {
    protected View rootView;
    protected RelativeLayout parentView;
    private NHProgressBarIndeterminate progressBarIndeterminate;

    @Override
    public void onDestroy() {
        super.onDestroy();
        NHVolley.getInstance(getActivity()).cancellAllRequestQueue(getAutomaticTag());
    }

    @Override
    public void startProgressBar(){
        if(parentView != null) {
            progressBarIndeterminate = new NHProgressBarIndeterminate(getActivity(), parentView);
            progressBarIndeterminate.setAnimationDrawable(R.drawable.preload_indeterminate);
        }
    }

    @Override
    public void onProgressBarVisible(){
        progressBarIndeterminate.visible();
    }

    @Override
    public void onProgressBarInvisible(){
        progressBarIndeterminate.invisible();
    }

    @Override
    public void onProgressBarToggle(){
        progressBarIndeterminate.toggle();
    }

    @Override
    public void onConnectionStart() {
        try {
            if(progressBarIndeterminate != null) {
                onProgressBarVisible();
                getActivity().setProgressBarIndeterminateVisibility(true);
            }
        } catch (NullPointerException nullPointerException) {}
    }

    @Override
    public void onConnectionFinished() {
        try {
            if(progressBarIndeterminate != null) {
                onProgressBarInvisible();
                getActivity().setProgressBarIndeterminateVisibility(false);
            }
        } catch (NullPointerException nullPointerException) {}
    }

    @Override
    public void onConnectionFailed() {
        try {
            if(progressBarIndeterminate != null) {
                onProgressBarInvisible();
                getActivity().setProgressBarIndeterminateVisibility(false);
            }
        } catch (NullPointerException nullPointerException) {}
    }
}
