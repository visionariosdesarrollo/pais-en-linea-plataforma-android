package com.nh.quickly.nh.utils;

import android.util.Log;

import com.nh.quickly.App;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Diego on 19/06/2016.
 */
public class NHJsonUtils {
    public static String[] convertJSONArrayToStringArray(JSONArray jsonArray){
        String[] stringArray = new String[0];
        try {
            stringArray = new String[jsonArray.length()];
            if(jsonArray.length() > 0)
                for(int i = 0, count = jsonArray.length(); i < count; i++)
                    stringArray[i] = jsonArray.get(i).toString();
        }
        catch (JSONException e) {}
        catch (NullPointerException nullPointerException) {}
        catch (IllegalArgumentException illegalArgumentException) {}

        return stringArray;
    }
}
