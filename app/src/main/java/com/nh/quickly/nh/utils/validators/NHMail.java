package com.nh.quickly.nh.utils.validators;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Diego on 11/01/2016.
 */
public class NHMail extends NHValidator {
    private static String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

    public static boolean isMail(String email) {
        boolean isValid = false;

        CharSequence inputEmail = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputEmail);
        if (matcher.matches())
            isValid = true;

        return isValid;
    }
}
