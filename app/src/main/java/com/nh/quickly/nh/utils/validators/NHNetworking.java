package com.nh.quickly.nh.utils.validators;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Diego on 25/06/2016.
 */
public class NHNetworking extends NHValidator {
    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        boolean _isConnected = networkInfo != null;

        return _isConnected;
    }
}
