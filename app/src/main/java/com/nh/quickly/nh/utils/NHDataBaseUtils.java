package com.nh.quickly.nh.utils;

import android.text.TextUtils;

/**
 * Created by Diego on 15/06/2016.
 */
public class NHDataBaseUtils {
    public static String concatenateWhereWithAnd(String a, String b) {
        if (TextUtils.isEmpty(a)) {
            return b;
        }
        if (TextUtils.isEmpty(b)) {
            return a;
        }

        return "(" + a + ") AND (" + b + ")";
    }

    public static String concatenateWhereWithOr(String a, String b) {
        if (TextUtils.isEmpty(a)) {
            return b;
        }
        if (TextUtils.isEmpty(b)) {
            return a;
        }

        return "(" + a + ") OR (" + b + ")";
    }
}
