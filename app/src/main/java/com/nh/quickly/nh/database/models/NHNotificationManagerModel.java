package com.nh.quickly.nh.database.models;

import android.provider.BaseColumns;

/**
 * Created by Diego on 24/06/2016.
 */
abstract public class NHNotificationManagerModel extends NHModel {
    public static int STATUS_NEW = 0;
    public static int STATUS_NOTIFIED = 1;
    public static int STATUS_PENDING = 2;
    public static int STATUS_VIEWED = 3;

    public static final class Columns implements BaseColumns {
        private Columns(){}

        public static final String STATUS_NOTIFICATION_MANAGER = "statusNotificationManager";
    }
}
