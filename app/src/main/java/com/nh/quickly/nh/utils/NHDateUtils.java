package com.nh.quickly.nh.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Diego on 11/01/2016.
 */
public class NHDateUtils {
    static final String FORMAT = "yyyy-MM-dd hh:mm:ss";
    public static final int TIMESTAMP_GAP = 1000;

    public static long currentTimeStampForPhp(){
        return System.currentTimeMillis() / TIMESTAMP_GAP;
    }

    public static long timeStampFromPhpToJava(long timestamp){
        return timestamp * TIMESTAMP_GAP;
    }

    public static long timeStampFromJavaToPhp(long timestamp){
        return timestamp / TIMESTAMP_GAP;
    }

    public static Date parse(String stringDate){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT);
        Date date = null;
        try {
            date = simpleDateFormat.parse(stringDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date dateFromTimestamp(long timestamp){
        Date date = new Date(timestamp);
        return date;
    }

    public static Date dateFromTimestampFromPhpToJava(long timestamp){
        Date date = new Date(timeStampFromPhpToJava(timestamp));
        return date;
    }

    public static String simpleDateFormat(String format, Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }
}
