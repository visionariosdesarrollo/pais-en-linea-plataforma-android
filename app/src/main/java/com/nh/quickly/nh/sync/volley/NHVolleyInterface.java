package com.nh.quickly.nh.sync.volley;

import android.support.annotation.Nullable;

import com.android.volley.Request;

/**
 * Created by Diego on 14/06/2016.
 */
public interface NHVolleyInterface {
    NHVolley addToQueue(Request request, @Nullable String tag);
    void cancellAllRequestQueue(@Nullable String tag);
}
