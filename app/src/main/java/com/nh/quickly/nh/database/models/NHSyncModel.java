package com.nh.quickly.nh.database.models;

import android.content.Context;
import android.provider.BaseColumns;

import com.nh.quickly.nh.database.models.NHModel;

/**
 * Created by Diego on 19/06/2016.
 */
public class NHSyncModel extends NHModel {
    public static int STATUS_NEW = 0;
    public static int STATUS_PENDING = 1;
    public static int STATUS_SYNC = 2;

    @Override
    public Boolean createTable(Context context) {
        return null;
    }

    @Override
    public Boolean dropTable(Context context) {
        return null;
    }

    public static final class Columns implements BaseColumns {
        private Columns(){}

        public static final String STATUS_SYNC = "statusSync";
    }
}
