package com.nh.quickly.nh.fragments;

/**
 * Created by Diego on 25/05/2016.
 */
public interface NHFragmentInterface {
    void onDestroy();
    String getAutomaticTag();
    void onConnectionStart();
    void onConnectionFinished();
    void onConnectionFailed();
    void startProgressBar();
    void onProgressBarVisible();
    void onProgressBarInvisible();
    void onProgressBarToggle();
}