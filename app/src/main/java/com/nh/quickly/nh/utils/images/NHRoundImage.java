package com.nh.quickly.nh.utils.images;

/**
 * Created by Diego on 08/01/2016.
 */
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.fragments.menu.options.CalendarEventsFragment;

final public class NHRoundImage extends NHDrawableImage {
    public static String TAG = NHRoundImage.class.getName();

    public String getType() {
        return TAG;
    }

    public NHRoundImage(Bitmap bitmap, @Nullable int bitmapWidth, @Nullable int bitmapHeight) {
        super(bitmap, bitmapWidth, bitmapHeight);
    }

    @Override
    public void draw(Canvas canvas) { canvas.drawOval(rectF, paint); }
}