package com.nh.quickly.nh.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nh.quickly.database.DataBase;

abstract public class NHDataBase extends SQLiteOpenHelper implements NHDataBaseInterface{
    protected static DataBase db;
    protected Context context;
    static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

    protected NHDataBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public static String getDateTimeFormat() {
        return DATE_TIME_FORMAT + ":00";
    };

    @Override
    public SQLiteDatabase getWritable(Context context) {
        return db.getWritableDatabase();
    }

    @Override
    public SQLiteDatabase getReadable(Context context) {
        return db.getReadableDatabase();
    }
}