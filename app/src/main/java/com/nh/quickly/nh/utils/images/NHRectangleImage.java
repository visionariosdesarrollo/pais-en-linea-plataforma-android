package com.nh.quickly.nh.utils.images;

/**
 * Created by Diego on 08/01/2016.
 */
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.Nullable;

final public class NHRectangleImage extends NHDrawableImage {
    public static String TAG = NHRectangleImage.class.getName();

    public String getType() {
        return TAG;
    }

    public NHRectangleImage(Bitmap bitmap, @Nullable int bitmapWidth, @Nullable int bitmapHeight) {
        super(bitmap, bitmapWidth, bitmapHeight);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRect(rectF, paint);
    }
}
