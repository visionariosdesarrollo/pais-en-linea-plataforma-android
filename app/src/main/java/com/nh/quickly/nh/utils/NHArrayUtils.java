package com.nh.quickly.nh.utils;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Diego on 24/05/2016.
 */
public class NHArrayUtils {
    public static String[] mergeStringArrays(@Nullable String array1[], @Nullable String array2[]) {
        if (array1 == null || array1.length == 0)
            return array2;
        if (array2 == null || array2.length == 0)
            return array1;
        List array1List = Arrays.asList(array1);
        List array2List = Arrays.asList(array2);
        List result = new ArrayList(array1List);
        List tmp = new ArrayList(array1List);
        tmp.retainAll(array2List);
        result.removeAll(tmp);
        result.addAll(array2List);
        return ((String[]) result.toArray(new String[result.size()]));
    }

    public static String implodeString(String glue, String[] pieces) {
        String piece = "";
        if(pieces != null) {
            for (int i = 0; i < pieces.length; i++) {
                piece +=  "\"" + pieces[i] + "\"";
                if(i < (pieces.length - 1))
                    piece += glue;
            }
        }
        return piece;
    }

    public static String implodeInteger(String glue, Integer[] pieces) {
        String piece = "";
        if(pieces != null) {
            for (int i = 0; i < pieces.length; i++) {
                piece += "\"" + pieces[i] + "\"";
                if(i < (pieces.length - 1))
                    piece += glue;
            }
        }
        return piece;
    }
}
