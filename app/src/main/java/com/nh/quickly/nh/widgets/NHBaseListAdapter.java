package com.nh.quickly.nh.widgets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Diego on 25/10/2015.
 */

public abstract class NHBaseListAdapter extends BaseAdapter {

    private ArrayList<?> rows;
    private int R_layout_idView;
    private Context context;

    public NHBaseListAdapter(Context context, int R_layout_idView, ArrayList<?> rows) {
        super();
        this.context = context;
        this.rows = rows;
        this.R_layout_idView = R_layout_idView;
    }

    @Override
    public View getView(int posicion, View view, ViewGroup pariente) {
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R_layout_idView, null);
        }
        onRow(rows.get(posicion), view);
        return view;
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Object getItem(int posicion) {
        return rows.get(posicion);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

    public abstract void onRow (Object row, View view);
}