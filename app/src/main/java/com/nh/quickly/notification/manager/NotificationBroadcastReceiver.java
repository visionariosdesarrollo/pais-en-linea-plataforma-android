package com.nh.quickly.notification.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nh.quickly.App;

/**
 * Created by Diego on 11/05/2016.
 */

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    NotificationWakefulBroadcastReceiver notifierWakefulBroadcastReceiver = new NotificationWakefulBroadcastReceiver();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(App.LOG_TAG, "NH::NotificationBroadcastReceiver::onReceive");
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction()))
            notifierWakefulBroadcastReceiver.start(context);
    }
}