package com.nh.quickly.notification.manager;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.activities.HomeFragmentActivity;
import com.nh.quickly.database.ContentProviderApp;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.Release;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.database.models.NHNotificationManagerModel;
import com.nh.quickly.nh.sync.volley.NHVolley;
import com.nh.quickly.nh.utils.NHArrayUtils;

import java.util.ArrayList;
import java.util.HashMap;

import me.leolin.shortcutbadger.ShortcutBadger;

public class NotificationService extends IntentService {
    public final static String TAG = NotificationService.class.toString();
    static String TICKER = "ticker";
    static String CONTENT_TITLE = "contentTitle";
    static String CONTENT_TEXT = "contentText";
    static String CONTENT_INFO = "contentInfo";
    PendingIntent contentIntent;
    NotificationManager notificationManager;
    NotificationCompat.Builder notificationCompatBuilder;
    ContentResolver contentResolver;
    Intent intent;
    HashMap<String, String> data;
    ContentValues contentValues;
    Drawable largeIconDrawable;
    Bitmap largeIconBitmap;

    public NotificationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        contentResolver = getContentResolver();
        startNotification();
        NotificationWakefulBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(final HashMap<String, String> data, final int smallIcon, final Bitmap largeIconBitmap, final int notificationColor, final String tag, final int notificationID, int shortcutBadger) {
        final long[] notificationVibrate = new long[]{1000, 1000, 1000, 1000, 1000, 1000};
        final Uri notificationSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.quickly);

        notificationCompatBuilder = new NotificationCompat.Builder(this)
                .setTicker(data.get(TICKER))
                .setContentTitle(data.get(CONTENT_TITLE))
                .setContentText(data.get(CONTENT_TEXT))
                .setContentInfo(data.get(CONTENT_INFO))
                .setSmallIcon(smallIcon)
                .setLargeIcon(largeIconBitmap)
                //.setStyle(new NotificationCompat.BigTextStyle().bigText(data.get("contentText")))
                .setAutoCancel(true)
                .setLights(notificationColor, 1000, 500)
                .setVibrate(notificationVibrate)
                .setSound(notificationSound, AudioAttributes.CONTENT_TYPE_MUSIC);

        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationCompatBuilder.setContentIntent(contentIntent);
        notificationManager.notify(tag, notificationID, notificationCompatBuilder.build());

        if(shortcutBadger > 0)
            ShortcutBadger.applyCount(getApplicationContext(), shortcutBadger);
    }

    private void sendNotificationEvent() {
        try {
            final Cursor cursorEvents = contentResolver.query(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH), NHArrayUtils.mergeStringArrays(Event.Columns.getFullColumns(), Event.Columns.getFullColumnsEntity()), NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER + "=?", new String[]{String.valueOf(NHNotificationManagerModel.STATUS_NEW)}, null);
            if (cursorEvents != null && cursorEvents.getCount() > 0) {
                cursorEvents.moveToFirst();
                switch (cursorEvents.getCount()) {
                    case 1:
                        data = new HashMap<String, String>() {{
                            put(TICKER, cursorEvents.getString(Event.Columns.ENTITY_FULL_NAME_POSITION));
                            put(CONTENT_TITLE, getResources().getString(R.string.app_name));
                            put(CONTENT_TEXT, cursorEvents.getString(Event.Columns.ENTITY_FULL_NAME_POSITION));
                            put(CONTENT_INFO, getResources().getString(R.string.event));
                        }};

                        intent.putExtra(Event.TAG, cursorEvents.getLong(Event.Columns.ID_POSITION));

                        largeIconDrawable = getResources().getDrawable(R.drawable.imagen_defecto_actividad);
                        largeIconBitmap = ((BitmapDrawable) largeIconDrawable).getBitmap();

                        contentValues = new ContentValues();
                        contentValues.put(NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_NOTIFIED);
                        int numberModifieds = contentResolver.update(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH + cursorEvents.getInt(Event.Columns.ID_POSITION)), contentValues, null, null);

                        if(!TextUtils.isEmpty(cursorEvents.getString(Event.Columns.ENTITY_FULL_IMAGE_POSITION))) {
                            ImageRequest imageRequest = new ImageRequest(
                                    App.URL_API_ENTITIES_IMAGES + cursorEvents.getString(Event.Columns.ENTITY_FULL_IMAGE_POSITION),
                                    new Response.Listener<Bitmap>() {
                                        @Override
                                        public void onResponse(Bitmap bitmap) {
                                            sendNotification(data, R.drawable.ic_logo_quickly_notificacion_actividad, bitmap, Color.rgb(255, 115, 11), Event.TAG, (int) cursorEvents.getLong(Event.Columns.ID_POSITION), cursorEvents.getCount());
                                        }
                                    }, 0, 0, null,
                                    new Response.ErrorListener() {
                                        public void onErrorResponse(VolleyError error) {
                                            sendNotification(data, R.drawable.ic_logo_quickly_notificacion_actividad, largeIconBitmap, Color.rgb(255, 115, 11), Event.TAG, (int) cursorEvents.getLong(Event.Columns.ID_POSITION), cursorEvents.getCount());
                                        }
                                    });
                            NHVolley.getInstance(getApplicationContext()).addToQueue(imageRequest, Event.TAG);
                        } else
                            sendNotification(data, R.drawable.ic_logo_quickly_notificacion_actividad, largeIconBitmap, Color.rgb(255, 115, 11), Event.TAG, (int) cursorEvents.getLong(Event.Columns.ID_POSITION), cursorEvents.getCount());
                        break;
                    default:
                        ArrayList<ContentProviderOperation> arrayListContentProviderOperation = new ArrayList<ContentProviderOperation>();

                        data = new HashMap<String, String>() {{
                            put(TICKER, getResources().getString(R.string.notification_manager_ticker_all_events));
                            put(CONTENT_TITLE, getResources().getString(R.string.app_name));
                            put(CONTENT_TEXT, String.format(getResources().getString(R.string.notification_manager_content_title_all_events), String.valueOf(cursorEvents.getCount())));
                            put(CONTENT_INFO, getResources().getString(R.string.event));
                        }};

                        contentValues = new ContentValues();
                        contentValues.put(NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_NOTIFIED);
                        while (cursorEvents.moveToNext()) {
                            arrayListContentProviderOperation.add(ContentProviderOperation.newUpdate(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH + cursorEvents.getInt(Event.Columns.ID_POSITION)))
                                    .withValues(contentValues)
                                    .withYieldAllowed(true)
                                    .build());
                        }

                        ContentProviderResult[] contentProviderResult = contentResolver.applyBatch(ContentProviderApp.AUTHORITY, arrayListContentProviderOperation);
                        arrayListContentProviderOperation.clear();

                        largeIconDrawable = getResources().getDrawable(R.drawable.ic_logo_quickly_notificacion_actividad);
                        largeIconBitmap = ((BitmapDrawable) largeIconDrawable).getBitmap();

                        sendNotification(data, R.drawable.ic_logo_quickly_notificacion_actividad, largeIconBitmap, Color.rgb(255, 115, 11), Event.TAG, (int) (Math.random() * Event.NOTIFICATION_ID), cursorEvents.getCount());
                }
            }
        }
        catch (NullPointerException nullPointerException) {}
        catch (IllegalArgumentException illegalArgumentException) {}
        catch (Exception exception) {}
    }

    private void sendNotificationRelease() {
        try {
            final Cursor cursorReleases = contentResolver.query(Uri.parse(NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH), Release.Columns.getColumns(), NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER + "=?", new String[]{String.valueOf(NHNotificationManagerModel.STATUS_NEW)}, null);
            if (cursorReleases != null && cursorReleases.getCount() > 0) {
                cursorReleases.moveToFirst();
                switch (cursorReleases.getCount()) {
                    case 1:
                        data = new HashMap<String, String>() {{
                            put(TICKER, cursorReleases.getString(Release.Columns.NAME_POSITION));
                            put(CONTENT_TITLE, getResources().getString(R.string.app_name));
                            put(CONTENT_TEXT, cursorReleases.getString(Release.Columns.USER_NAME_POSITION));
                            put(CONTENT_INFO, getResources().getString(R.string.release));
                        }};

                        intent.putExtra(Release.TAG, cursorReleases.getLong(Release.Columns.ID_POSITION));

                        largeIconDrawable = getResources().getDrawable(R.drawable.release_image_list_default);
                        largeIconBitmap = ((BitmapDrawable) largeIconDrawable).getBitmap();

                        contentValues = new ContentValues();
                        contentValues.put(NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_NOTIFIED);
                        int numberModifieds = contentResolver.update(Uri.parse(NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH + cursorReleases.getInt(Release.Columns.ID_POSITION)), contentValues, null, null);

                        if(!TextUtils.isEmpty(cursorReleases.getString(Release.Columns.USER_IMAGE_POSITION))) {
                            ImageRequest imageRequest = new ImageRequest(
                                    App.URL_API_USERS_IMAGES + cursorReleases.getString(Release.Columns.USER_IMAGE_POSITION),
                                    new Response.Listener<Bitmap>() {
                                        @Override
                                        public void onResponse(Bitmap bitmap) {
                                            sendNotification(data, R.drawable.ic_logo_quickly_notificacion_comunicado, bitmap, Color.WHITE, Release.TAG, (int) cursorReleases.getLong(Release.Columns.ID_POSITION), cursorReleases.getCount());
                                        }
                                    }, 0, 0, null,
                                    new Response.ErrorListener() {
                                        public void onErrorResponse(VolleyError error) {
                                            sendNotification(data, R.drawable.ic_logo_quickly_notificacion_comunicado, largeIconBitmap, Color.WHITE, Release.TAG, (int) cursorReleases.getLong(Release.Columns.ID_POSITION), cursorReleases.getCount());
                                        }
                                    });
                            NHVolley.getInstance(getApplicationContext()).addToQueue(imageRequest, Event.TAG);
                        } else
                            sendNotification(data, R.drawable.ic_logo_quickly_notificacion_comunicado, largeIconBitmap, Color.WHITE, Release.TAG, (int) cursorReleases.getLong(Release.Columns.ID_POSITION), cursorReleases.getCount());
                        break;
                    default:
                        ArrayList<ContentProviderOperation> arrayListContentProviderOperation = new ArrayList<ContentProviderOperation>();

                        data = new HashMap<String, String>() {{
                            put(TICKER, getResources().getString(R.string.notification_manager_ticker_all_releases));
                            put(CONTENT_TITLE, getResources().getString(R.string.app_name));
                            put(CONTENT_TEXT, String.format(getResources().getString(R.string.notification_manager_content_title_all_releases), String.valueOf(cursorReleases.getCount())));
                            put(CONTENT_INFO, getResources().getString(R.string.release));
                        }};

                        contentValues = new ContentValues();
                        contentValues.put(NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_NOTIFIED);
                        while (cursorReleases.moveToNext()) {
                            arrayListContentProviderOperation.add(ContentProviderOperation.newUpdate(Uri.parse(NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH + cursorReleases.getInt(Release.Columns.ID_POSITION)))
                                    .withValues(contentValues)
                                    .withYieldAllowed(true)
                                    .build());
                        }

                        ContentProviderResult[] contentProviderResult = contentResolver.applyBatch(ContentProviderApp.AUTHORITY, arrayListContentProviderOperation);
                        arrayListContentProviderOperation.clear();

                        largeIconDrawable = getResources().getDrawable(R.drawable.release_image_list_default);
                        largeIconBitmap = ((BitmapDrawable) largeIconDrawable).getBitmap();

                        sendNotification(data, R.drawable.ic_logo_quickly_notificacion_comunicado, largeIconBitmap, Color.WHITE, Release.TAG, (int) (Math.random() * Release.NOTIFICATION_ID), cursorReleases.getCount());
                }
            }
        }
        catch (NullPointerException nullPointerException) {}
        catch (IllegalArgumentException illegalArgumentException) {}
        catch (Exception exception) {}
    }

    private void startNotification() {
        notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        intent = new Intent(this, HomeFragmentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        sendNotificationEvent();
        sendNotificationRelease();
    }
}