package com.nh.quickly.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.util.TimeUtils;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.Login;
import com.nh.quickly.database.models.User;
import com.nh.quickly.fragments.BlanckFragment;
import com.nh.quickly.fragments.RegisterFragment;
import com.nh.quickly.nh.activities.NHActivity;
import com.nh.quickly.nh.utils.NHScreenUtils;
import com.nh.quickly.nh.utils.validators.NHNetworking;

import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends NHActivity implements  Animation.AnimationListener{
    private Timer timer = new Timer();
    //private ProgressDialogFragment progressDialogFragment;
    private ImageView brand, typography;
    private ProgressBar progressBar;
    int numberOfAnimation = 1;
    int brandEndY;
    int typographyHeight;
    int typographyTop;
    private Handler handlerUserRegisterReload = new Handler(){
        @Override
        public void handleMessage(Message msg) {
        //userRegisterReload();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        brand = (ImageView) findViewById(R.id.brand);
        typography = (ImageView) findViewById(R.id.typography);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        //instanceProgressDialog();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        startAnimations();
    }

    private void startAnimations(){
        typographyHeight = typography.getHeight();
        typographyTop = typography.getTop();
        typography.setVisibility(View.INVISIBLE);
        startAnimationBrand();
    }

    private void startAnimationBrand(){
        Animation animationBrandFadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        animationBrandFadeIn.setDuration(getResources().getInteger(R.integer.brand_animation_duration));
        animationBrandFadeIn.setAnimationListener(this);
        brand.setAnimation(animationBrandFadeIn);
        animationBrandFadeIn.start();
    }

    private void startTimerReconnection() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handlerUserRegisterReload.sendEmptyMessage(0);
            }
        }, 1000);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        if(numberOfAnimation == 1) {
            numberOfAnimation = 2;
            brand.setVisibility(View.VISIBLE);
        }else if(numberOfAnimation == 22) {
            numberOfAnimation = 3;
            typography.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        TranslateAnimation translateAnimationBrand = new TranslateAnimation(0, 0, 0, brandEndY);

        if(numberOfAnimation == 2) {
            numberOfAnimation = 22;
            translateAnimationBrand = new TranslateAnimation(0, 0, (NHScreenUtils.screenHeight(this) / 2), 0);
            translateAnimationBrand.setDuration(getResources().getInteger(R.integer.typography_translate_duration));
            typography.setAnimation(translateAnimationBrand);
            translateAnimationBrand.setAnimationListener(this);
            translateAnimationBrand.start();
        }else if(numberOfAnimation == 3) {
            SystemClock.sleep(getResources().getInteger(R.integer.progress_bar_sleep_duration));
            progressBar.setVisibility(View.VISIBLE);
        }
/*
        if(numberOfAnimation == 1){
            numberOfAnimation = 2;
            brandEndY = (-1 * typographyHeight);
            translateAnimationBrand.setDuration(getResources().getInteger(R.integer.brand_translate_duration));
            brand.setAnimation(translateAnimationBrand);
            translateAnimationBrand.setAnimationListener(this);
            translateAnimationBrand.start();
        } else if(numberOfAnimation == 2) {
        */
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    /*
        //implements View.OnClickListener, DocumentTypeListFragment.Callbacks, DepartmentListFragment.Callbacks, CityListFragment.Callbacks, DatePickerDialogFragment.Callbacks, RegisterFragment.Callbacks, DataSynchronizationFragment.Callbacks {
    private Timer timerReconnection = new Timer();
    //private ProgressDialogFragment progressDialogFragment;
    private Handler handlerUserRegisterReload = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            userRegisterReload();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_one_column);
        //instanceProgressDialog();
    }

    private void instanceProgressDialog(){
        progressDialogFragment = new ProgressDialogFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.progress_dialog, progressDialogFragment).commit();
    };

    @Override
    public void onResume() {
        super.onResume();
        clearFragments();
        changeMainFragment();
    }

    private void startTimerReconnection() {
        timerReconnection.schedule(new TimerTask() {
            @Override
            public void run() {
                handlerUserRegisterReload.sendEmptyMessage(0);
            }
        }, 1000);
    }

    private void stopTimerReconnection() {
        if (timerReconnection != null) {
            timerReconnection.cancel();
        }
    }

    private void clearFragments() {
        getSupportFragmentManager().beginTransaction().replace(R.id.column, new BlanckFragment()).commit();
    }

    private void startBindService() {
        Intent service = new Intent(this, DataService.class);
        startService(service);
    }

    private void changeHomeActivity() {
        startBindService();
        Intent intent = new Intent(this, HomeFragmentActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void changeMainFragment() {
        User user = Login.getInstanceUser(this);

        if(user != null)
            changeHomeActivity();
        else
            userRegister();
    }

    private Boolean checkNumberOfDepartments(){
        Boolean checker = true;

        if(new Department().count(MainActivity.this) == 0) {
            checker = false;
            progressDialogFragment.show(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, this);
        }

        return checker;
    }

    private Boolean checkNumberOfCities(){
        Boolean checker = true;

        if (new City().count(MainActivity.this) == 0) {
            checker = false;
            progressDialogFragment.show(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, this);
        }

        return checker;
    }

    private void welcome() {
        if(checkNumberOfDepartments() && checkNumberOfCities())
            if(getResources().getBoolean(R.bool.two_column))
                getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).replace(R.id.column_right, new WelcomeFragment()).commit();
            else
                getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).replace(R.id.column, new WelcomeFragment()).commit();
    }

    private void userRegister() {
        if(getResources().getBoolean(R.bool.two_column)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.column_left, new MainFragment()).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.column_right, new TypographyFragment()).commit();
        } else
            getSupportFragmentManager().beginTransaction().replace(R.id.column, new MainFragment()).commit();

        userRegisterReload();
    }

    private void userRegisterReload() {
        if(NHNetworking.isConnected(this))
            new DepartmentsAsyncTask().execute();
        else
            progressDialogFragment.show(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, MainActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            Log.i(App.LOG_TAG, "popping backstack");
            fragmentManager.popBackStack();
        }

        else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            super.onBackPressed();
        }

    }

    @Override
    public void onClick(View v) {
        progressDialogFragment.gone();
        startTimerReconnection();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimerReconnection();
    }

    @Override
    public void onDocumentTypeListItemClick(String documentTypeId) {
        RegisterFragment registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentByTag(RegisterFragment.TAG);

        if(registerFragment != null) {
            registerFragment.changeDocumentType(documentTypeId);
        }
    }

    @Override
    public void onDepartmenListItemClick(int departmentId) {
        RegisterFragment registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentByTag(RegisterFragment.TAG);

        if(registerFragment != null) {
            registerFragment.changeDepartment(departmentId);
        }
    }

    @Override
    public void onCityListItemClick(int cityId) {
        RegisterFragment registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentByTag(RegisterFragment.TAG);

        if(registerFragment != null) {
            registerFragment.changeCity(cityId);
        }
    }

    @Override
    public void onDatePickerDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        RegisterFragment registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentByTag(RegisterFragment.TAG);

        if(registerFragment != null) {
            registerFragment.changeDocumentDeliveryDate(view);
        }
    }

    @Override
    public void onProgressDialogFragmentShow(Boolean progressBarShow, int rIdMessageText, int rIdhandlerTextTxt, View.OnClickListener handlerTxtOnClickListener) {
        progressDialogFragment.show(progressBarShow, rIdMessageText, rIdhandlerTextTxt, handlerTxtOnClickListener);
    }

    @Override
    public void onProgressDialogFragmentShowString(Boolean progressBarShow, String messageText, int rIdhandlerTextTxt, View.OnClickListener handlerTxtOnClickListener) {
        progressDialogFragment.showString(progressBarShow, messageText, rIdhandlerTextTxt, handlerTxtOnClickListener);
    }

    @Override
    public void onProgressDialogGone() {
        progressDialogFragment.gone();
    }

    public class DepartmentsAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialogFragment.show(true, R.string.synchronizing_departments, 0, null);
        }

        @Override
        protected String doInBackground(String... arguments) {
            try {
                return HttpRequest.get(App.URL_API_DEPARTMENTS, true, new String[]{"countryId", String.valueOf(SynchronizeDataJsonToDataBase.COUNTRY)})
                        .accept(HttpRequest.CONTENT_TYPE_JSON)
                        .body();
            } catch (HttpRequest.HttpRequestException httpRequestException) {
                publishProgress();
                cancel(true);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... arguments) {
            super.onProgressUpdate(arguments);
            progressDialogFragment.show(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, MainActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(MainActivity.this).departments(response);
                new CitiesAsyncTask().execute();
            } catch (Exception exception) {
                progressDialogFragment.show(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, MainActivity.this);
            }
        }
    }

    public class CitiesAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialogFragment.show(true, R.string.synchronizing_cities, 0, null);
        }

        @Override
        protected String doInBackground(String... arguments) {
            try {
                return HttpRequest.get(App.URL_API_CITIES, true, new String[]{"countryId", String.valueOf(SynchronizeDataJsonToDataBase.COUNTRY)})
                        .accept(HttpRequest.CONTENT_TYPE_JSON)
                        .body();
            } catch (HttpRequest.HttpRequestException httpRequestException) {
                publishProgress();
                cancel(true);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... arguments) {
            super.onProgressUpdate(arguments);
            progressDialogFragment.show(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, MainActivity.this);
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(MainActivity.this).cities(response);
                progressDialogFragment.gone();
                welcome();
            } catch (Exception exception) {
                progressDialogFragment.show(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, MainActivity.this);
            }
        }
    }*/
}