package com.nh.quickly.activities;

import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.ContentProviderApp;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.fragments.HomeContentFragment;
import com.nh.quickly.fragments.menu.SubMenuFragment;
import com.nh.quickly.fragments.menu.options.CalendarEventsFragment;
import com.nh.quickly.fragments.menu.options.DescriptionEventFragment;
import com.nh.quickly.fragments.menu.options.EventsActivesFragment;
import com.nh.quickly.fragments.menu.options.EventsFragment;
import com.nh.quickly.fragments.menu.options.PreferencesFragment;
import com.nh.quickly.fragments.menu.options.ShudleUserEventsFragment;
import com.nh.quickly.nh.fragments.NHFragmentActivity;
import com.nh.quickly.notification.manager.NotificationWakefulBroadcastReceiver;
import com.nh.quickly.sync.SyncAccount;

import java.util.Date;

import me.leolin.shortcutbadger.ShortcutBadger;

public class HomeFragmentActivity extends NHFragmentActivity implements SubMenuFragment.Callbacks, ShudleUserEventsFragment.Callbacks, EventsFragment.Callbacks, EventsActivesFragment.Callbacks, CalendarEventsFragment.Callbacks {
    public final static String TAG = HomeFragmentActivity.class.toString();
    NotificationWakefulBroadcastReceiver notificationWakefulBroadcastReceiver = new NotificationWakefulBroadcastReceiver();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_one_column);

        startApp();
    }

    void startApp() {
        App.preparateSounds(getApplicationContext());
        sync();
        removeShortcutBadger();
        startFragmentHome();
    }

    void sync() {
        ContentResolver.setIsSyncable(SyncAccount.getInstance(this), ContentProviderApp.AUTHORITY, 1);
        ContentResolver.setSyncAutomatically(SyncAccount.getInstance(this), ContentProviderApp.AUTHORITY, true);
        ContentResolver.addPeriodicSync(SyncAccount.getInstance(this), ContentProviderApp.AUTHORITY, Bundle.EMPTY, SyncAccount.SYNC_INTERVAL);
    }

    void startNotificationManager() {
        notificationWakefulBroadcastReceiver.start(this);
        notificationManagerCancelAll();
    }

    void cancelNotificationManager() {
        notificationWakefulBroadcastReceiver.cancel(this);
        notificationManagerCancelAll();
    }

    void notificationManagerCancelAll() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    public void startFragmentHome() {
        getSupportFragmentManager().beginTransaction().replace(R.id.column, HomeContentFragment.getInstance(), HomeContentFragment.TAG).commit();
    }

    void removeShortcutBadger() {
        ShortcutBadger.removeCount(getApplicationContext());
    }

    public String getAutomaticTag() {
        return TAG;
    }

    @Override
    public void onClickListItemSelected(long id) {
        Bundle arguments = new Bundle();
        arguments.putLong(Event.Columns.ID, id);

        DescriptionEventFragment descriptionEventFragment = DescriptionEventFragment.getInstance();
        descriptionEventFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).add(R.id.page, descriptionEventFragment, DescriptionEventFragment.TAG).addToBackStack(DescriptionEventFragment.TAG).commit();
    }

    @Override
    public void onDescriptionFragment(long id) {
        onClickListItemSelected(id);
    }

    @Override
    public void onResume() {
        super.onResume();
        cancelNotificationManager();
    }

    @Override
    public void onPause() {
        super.onPause();
        startNotificationManager();
    }

    @Override
    public void onSelectDate(Date date, View view) {
        SubMenuFragment subMenuFragment = (SubMenuFragment) getSupportFragmentManager().findFragmentByTag(SubMenuFragment.TAG);
        EventsFragment eventsFragment = (EventsFragment) getSupportFragmentManager().findFragmentByTag(EventsFragment.TAG);
        if (subMenuFragment != null && eventsFragment != null) {
            subMenuFragment.onSelectDate(date, view);
            Message message =  new Message();
            message.what = EventsFragment.HANDLER_RELOAD_EVENTS_FOR_TIME;
            message.obj = (long) date.getTime();
            eventsFragment.handler.handleMessage(message);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d(App.LOG_TAG, "NH::EN LA GRAN ACTIVIDAD");
        Log.d(App.LOG_TAG, "NH::requestCode::" + requestCode);
        Log.d(App.LOG_TAG, "NH::resultCode::" + resultCode + "::RESULT_OK::" + RESULT_OK);
        if(requestCode == 100 && resultCode == RESULT_OK){
            Log.d(App.LOG_TAG, "NH::EN LA GRAN ACTIVIDAD" + data.getStringArrayExtra(RecognizerIntent.EXTRA_RESULTS));
        }
    }

/*
    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager(); //use getSupportFragmentManager() for support fragments
        Log.d(App.LOG_TAG, "NH::HomeFragmentActivity::onBackPressed::count::" + getSupportFragmentManager().getBackStackEntryCount());
        if (fragmentManager.getBackStackEntryCount() > 0) {
            //switch (getSupportFragmentManager().getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName());
            Log.d(App.LOG_TAG, "NH::HomeFragmentActivity::onBackPressed::if::name::" + getSupportFragmentManager().getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName());
            fragmentManager.popBackStackImmediate();
        } else {
            Log.d(App.LOG_TAG, "NH::HomeFragmentActivity::onBackPressed::else");
            super.onBackPressed();
        }
    }
*/
}