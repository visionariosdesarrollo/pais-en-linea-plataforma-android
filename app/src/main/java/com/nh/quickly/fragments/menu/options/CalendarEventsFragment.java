package com.nh.quickly.fragments.menu.options;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.fragments.NHFragment;
import com.nh.quickly.nh.utils.NHDataBaseUtils;
import com.nh.quickly.nh.utils.NHDateUtils;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Diego on 26/10/2015.
 */
public class CalendarEventsFragment extends NHFragment {
    public final static String TAG = CalendarEventsFragment.class.getName();
    public final static String YEAR = "year";
    public final static String MONTH = "month";
    public final static int POSITION = 2;
    private static CalendarEventsFragment calendarEventsFragment;
    private CaldroidFragment callDroidFragment;
    public Callbacks callbacks;
    AsyncTask calendarEventsAsyncTask;
    ContentResolver contentResolver;
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    onReload();
                    break;
            }
        }
    };
    int year;
    int month;

    public interface Callbacks {
        public void onSelectDate(Date date, View view);
    }

    public static CalendarEventsFragment getInstance() {
        if(calendarEventsFragment == null)
            calendarEventsFragment = new CalendarEventsFragment();

        return calendarEventsFragment;
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentResolver = getActivity().getContentResolver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.events_calendar_fragment, viewGroup, false);
        startCalendar();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    void startCalendar() {
        callDroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar calendar = Calendar.getInstance();
        args.putInt(CaldroidFragment.THEME_RESOURCE, R.style.CaldroidDefaultDark);
        args.putInt(CaldroidFragment.SHOW_NAVIGATION_ARROWS, R.style.CaldroidDefaultDark);
        args.putInt(CaldroidFragment.MONTH, calendar.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, calendar.get(Calendar.YEAR));
        callDroidFragment.setArguments(args);
        callDroidFragment.setMinDate(Calendar.getInstance().getTime());

        callDroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onCaldroidViewCreated() {
                onReload();
            }

            @Override
            public void onSelectDate(Date date, View view) {
                Log.d(App.LOG_TAG, "NH::callDroidFragment::setCaldroidListener::year::" + date.getYear() + "::month::" + date.getMonth() + "::day::" + date.getDay());
                callbacks.onSelectDate(date, view);
            }

            @Override
            public void onChangeMonth(int changeMonth, int changeYear) {
                Log.d(App.LOG_TAG, "NH::callDroidFragment::setCaldroidListener::changeYear::" + changeYear + "::changeMonth::" + changeMonth);
                year = changeYear;
                month = changeMonth;
            }
        });

        getFragmentManager().beginTransaction()
                .add(R.id.calendar, callDroidFragment)
                .commit();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " should implement Callbacks");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(calendarEventsAsyncTask != null) {
            calendarEventsAsyncTask.cancel(true);
            calendarEventsAsyncTask = null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    HashMap<Date, Integer> events(int year, int month) {
        HashMap<Date, Integer> daysCalendar = new HashMap<Date, Integer>();

        City city = PreferencesFragment.getCity(getContext());
        if (city != null) {
            String startDate = String.valueOf(NHDateUtils.currentTimeStampForPhp());
            String selection = NHDataBaseUtils.concatenateWhereWithAnd(
                    Event.Columns.FULL_EVENT_TIME_STAMP_END + ">=?",
                    Entity.Columns.FULL_CITY_ID + "=?"
            );

            Cursor cursorEvents = getContext().getContentResolver().query(
                    Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH),
                    Event.Columns.getFullColumns(),
                    selection,
                    new String[]{startDate, String.valueOf(city.getId())},
                    Event.Columns.SORT_BY_FULL_EVENT_TIME_STAMP_START_ASC
            );

            if (cursorEvents != null && cursorEvents.getCount() > 0) {
                cursorEvents.moveToFirst();
                do {
                    long eventTimeStampStart = cursorEvents.getLong(Event.Columns.EVENT_TIME_STAMP_START_POSITION);
                    Date dateStart = NHDateUtils.dateFromTimestampFromPhpToJava(eventTimeStampStart);
                    if (!daysCalendar.containsKey(dateStart))
                        daysCalendar.put(dateStart, R.color.calendar_days_events);
                } while (cursorEvents.moveToNext());

                if(daysCalendar.size() > 0) {
                    callDroidFragment.setBackgroundResourceForDates(daysCalendar);
                    callDroidFragment.refreshView();
                }
            }
        }

        return daysCalendar;
    }

    public void onReload() {
        try {
            /*
            if(calendarEventsAsyncTask != null)
                calendarEventsAsyncTask.cancel(true);

            Log.d(App.LOG_TAG, "NH::callDroidFragment::onReload");
            calendarEventsAsyncTask = new CalendarEventsAsyncTask(getContext()).execute(new Integer[]{year, month});
            */
            events(year, month);
        } catch (Exception exception) {
            Log.d(App.LOG_TAG, "NH::callDroidFragment::onReload::" + exception.getMessage());
        }
    }

    private class CalendarEventsAsyncTask extends AsyncTask<Integer, Long, HashMap<Date, Integer>> {
        public Boolean inAction = false;
        Context context;

        public CalendarEventsAsyncTask(Context context) {
            this.context = context;
        }

        HashMap<Date, Integer> events(int year, int month) {
            HashMap<Date, Integer> daysCalendar = new HashMap<Date, Integer>();

            City city = PreferencesFragment.getCity(context);
            if (city != null) {
                String startDate = String.valueOf(NHDateUtils.currentTimeStampForPhp());
                String selection = NHDataBaseUtils.concatenateWhereWithAnd(
                        Event.Columns.FULL_EVENT_TIME_STAMP_END + ">=?",
                        Entity.Columns.FULL_CITY_ID + "=?"
                );

                Cursor cursorEvents = context.getContentResolver().query(
                        Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH),
                        Event.Columns.getFullColumns(),
                        selection,
                        new String[]{startDate, String.valueOf(city.getId())},
                        Event.Columns.SORT_BY_FULL_EVENT_TIME_STAMP_START_ASC
                );

                Log.d(App.LOG_TAG, "NH::CalendarEventsAsyncTask::cursorEvents::0");
                if (cursorEvents != null && cursorEvents.getCount() > 0) {
                    Log.d(App.LOG_TAG, "NH::CalendarEventsAsyncTask::cursorEvents::" + cursorEvents.getCount());

                    cursorEvents.moveToFirst();
                    do {
                        long eventTimeStampStart = cursorEvents.getLong(Event.Columns.EVENT_TIME_STAMP_START_POSITION);
                        Date dateStart = NHDateUtils.dateFromTimestampFromPhpToJava(eventTimeStampStart);
                        if (!daysCalendar.containsKey(dateStart))
                            daysCalendar.put(dateStart, R.color.calendar_days_events);
                    } while (cursorEvents.moveToNext());
                }
            }

            return daysCalendar;
        }

        @Override
        protected void onPreExecute() {
            inAction = true;
        }

        @Override
        protected HashMap<Date, Integer> doInBackground(Integer... params) {
            Log.d(App.LOG_TAG, "NH::CalendarEventsAsyncTask::doInBackground");
            return events(params[0], params[1]);
        }

        @Override
        protected void onCancelled() {
            inAction = false;
        }

        @Override
        protected void onPostExecute(HashMap<Date, Integer> result) {
            Log.d(App.LOG_TAG, "NH::CalendarEventsAsyncTask::onPostExecute");

            if (result.size() > 0){
                callDroidFragment.setBackgroundResourceForDates(result);
                callDroidFragment.refreshView();
            }

            inAction = false;
        }
    }
}
