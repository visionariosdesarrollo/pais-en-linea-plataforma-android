package com.nh.quickly.fragments.menu;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.nh.quickly.R;
import com.nh.quickly.database.ContentProviderApp;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.EventQuickly;
import com.nh.quickly.database.models.Release;
import com.nh.quickly.fragments.menu.options.CalendarEventsFragment;
import com.nh.quickly.fragments.menu.options.EventsFragment;
import com.nh.quickly.fragments.menu.options.ReleasesFragment;
import com.nh.quickly.fragments.menu.options.ShudleUserEventsFragment;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.database.models.NHNotificationManagerModel;
import com.nh.quickly.nh.fragments.NHFragment;
import com.nh.quickly.nh.fragments.NHFragmentPagerAdapter;
import com.roomorama.caldroid.CaldroidFragment;

import java.util.Date;

public class SubMenuFragment extends NHFragment implements FragmentTabHost.OnTabChangeListener {
    public final static String TAG = MenuFragment.class.toString();
    private static SubMenuFragment subMenuFragment;
    FragmentTabHost fragmentTabHost;
    ViewPager viewPager;
    SubmenuPagerAdapter submenuPagerAdapter;
    public Callbacks callbacks;
    ContentResolver contentResolver;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EventsFragment.POSITION:
                    onChangeEventsCountInTab();
                    break;
                case ReleasesFragment.POSITION:
                    onChangeReleasesCountInTab();
                    break;
            }
        }
    };

    public static SubMenuFragment getInstance(){
        if(subMenuFragment == null)
            subMenuFragment = new SubMenuFragment();

        return subMenuFragment;
    }

    public interface Callbacks {
        public void onDescriptionFragment(long id);
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        contentResolver = getActivity().getContentResolver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.submenu_fragment, viewGroup, false);
        fragmentTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        submenuPagerAdapter = new SubmenuPagerAdapter(getChildFragmentManager(), getContext());
        startTabs();
        startViewPager();
        optionDefault(EventsFragment.POSITION);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getActivity().getIntent().getExtras();
        if(bundle != null && bundle.containsKey(Event.TAG)) {
            long eventId = bundle.getLong(Event.TAG);
            bundle.remove(Event.TAG);
            callbacks.onDescriptionFragment(eventId);

        } else if(bundle != null && bundle.containsKey(Release.TAG)) {
            bundle.remove(Release.TAG);
            optionDefault(ReleasesFragment.POSITION);
        }
    }

    public void onSelectDate(Date date, View view) {
        EventsFragment eventsFragment = (EventsFragment) getActivity().getSupportFragmentManager().findFragmentByTag(EventsFragment.TAG);

        if (eventsFragment != null) {
            eventsFragment.onSelectDate(date, view);
            optionDefault(EventsFragment.POSITION);
            //EventsFragment.getInstance().onReload();
        }
    }

    void startTabs(){
        fragmentTabHost.setup(getActivity(), getActivity().getSupportFragmentManager(), android.R.id.tabcontent);
        addCustomTab(getActivity().getApplicationContext(), ShudleUserEventsFragment.TAG, getResources().getDrawable(R.drawable.menu_option_shudle_user_events), ShudleUserEventsFragment.class, getResources().getString(R.string.tab_option_submenu_shudle_user_events));
        addCustomTab(getActivity().getApplicationContext(), EventsFragment.TAG, getResources().getDrawable(R.drawable.menu_option_events), EventsFragment.class, getResources().getString(R.string.tab_option_submenu_events));
        addCustomTab(getActivity().getApplicationContext(), CalendarEventsFragment.TAG, getResources().getDrawable(R.drawable.menu_option_calendar_events), CalendarEventsFragment.class, getResources().getString(R.string.tab_option_submenu_calendar_events));
        addCustomTab(getActivity().getApplicationContext(), ReleasesFragment.TAG, getResources().getDrawable(R.drawable.menu_option_releases), ReleasesFragment.class, getResources().getString(R.string.tab_option_submenu_releases));
        fragmentTabHost.setOnTabChangedListener(this);
    }

    void startViewPager(){
        viewPager.setAdapter(submenuPagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                fragmentTabHost.setCurrentTab(position);
            }
        });
    }

    void optionDefault(int optionId){
        fragmentTabHost.setCurrentTab(optionId);
        viewPager.setCurrentItem(optionId);
    }

    void addCustomTab(Context context, String tag, Drawable drawable, Class<?> classs, CharSequence contentDescription) {
        View view = LayoutInflater.from(context).inflate(R.layout.submenu_fragment_tab, null);
        ImageView image = (ImageView) view.findViewById(R.id.icon);

        image.setImageDrawable(drawable);
        image.setContentDescription(contentDescription);

        TabHost.TabSpec tabSpec = fragmentTabHost.newTabSpec(tag);
        tabSpec.setIndicator(view);
        fragmentTabHost.addTab(tabSpec, classs, null);
    }

    @Override
    public void onTabChanged(String tabId) {
        if(tabId == ShudleUserEventsFragment.TAG) {
            fragmentTabHost.setCurrentTab(ShudleUserEventsFragment.POSITION);
            viewPager.setCurrentItem(ShudleUserEventsFragment.POSITION);
        } else if(tabId == EventsFragment.TAG) {
            fragmentTabHost.setCurrentTab(EventsFragment.POSITION);
            viewPager.setCurrentItem(EventsFragment.POSITION);
        } else if(tabId == CalendarEventsFragment.TAG) {
            fragmentTabHost.setCurrentTab(CalendarEventsFragment.POSITION);
            viewPager.setCurrentItem(CalendarEventsFragment.POSITION);
        } else if(tabId == ReleasesFragment.TAG) {
            fragmentTabHost.setCurrentTab(ReleasesFragment.POSITION);
            viewPager.setCurrentItem(ReleasesFragment.POSITION);
        }

        View tabView = fragmentTabHost.getCurrentTabView();
        tabView.findViewById(R.id.count).setVisibility(View.GONE);
    }

    void onChangeEventsCountInTab(){
        if(fragmentTabHost != null && (fragmentTabHost.getCurrentTab() != EventsFragment.POSITION)) {
            int count = ContentProviderApp.count(getContext(), Event.ContentProvider.ALL_ROWS_CODE, NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER + "=?", new String[]{String.valueOf(NHNotificationManagerModel.STATUS_PENDING)});
            View tabsViewGroup = (View) fragmentTabHost.getTabWidget().getChildTabViewAt(EventsFragment.POSITION);
            TextView countTextView = (TextView) tabsViewGroup.findViewById(R.id.count);
            if(countTextView != null) {
                if(count > 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_VIEWED);
                    int numberModifieds = contentResolver.update(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH), contentValues, null, null);

                    countTextView.setText(String.valueOf(count));
                    countTextView.setVisibility(View.VISIBLE);
                } else
                    countTextView.setVisibility(View.GONE);
            }
        }
    }

    void onChangeReleasesCountInTab(){
        if(fragmentTabHost != null && (fragmentTabHost.getCurrentTab() != ReleasesFragment.POSITION)) {
            int count = ContentProviderApp.count(getContext(), Release.ContentProvider.ALL_ROWS_CODE, NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER + "=?", new String[]{String.valueOf(NHNotificationManagerModel.STATUS_PENDING)});
            View tabsViewGroup = (View) fragmentTabHost.getTabWidget().getChildTabViewAt(ReleasesFragment.POSITION);
            TextView countTextView = (TextView) tabsViewGroup.findViewById(R.id.count);
            if(countTextView != null) {
                if(count > 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_VIEWED);
                    int numberModifieds = contentResolver.update(Uri.parse(NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH), contentValues, null, null);

                    countTextView.setText(String.valueOf(count));
                    countTextView.setVisibility(View.VISIBLE);
                } else
                    countTextView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar Callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    public static class SubmenuPagerAdapter extends NHFragmentPagerAdapter {
        Context context;
        public SubmenuPagerAdapter(FragmentManager fragmentManager, Context context) {
            super(fragmentManager);
            this.context = context;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
                case ShudleUserEventsFragment.POSITION:
                    return context.getResources().getString(R.string.tab_option_submenu_shudle_user_events);
                case EventsFragment.POSITION:
                    return context.getResources().getString(R.string.tab_option_submenu_events);
                case CalendarEventsFragment.POSITION:
                    return context.getResources().getString(R.string.tab_option_submenu_calendar_events);
                case ReleasesFragment.POSITION:
                    return context.getResources().getString(R.string.tab_option_submenu_releases);
                default:
                    return "";
            }
        }

        @Override
        public Fragment getItem(int index) {
            switch (index){
                case ShudleUserEventsFragment.POSITION:
                    return ShudleUserEventsFragment.getInstance();
                case EventsFragment.POSITION:
                    return EventsFragment.getInstance();
                case CalendarEventsFragment.POSITION:
                    return CalendarEventsFragment.getInstance();
                case ReleasesFragment.POSITION:
                    return ReleasesFragment.getInstance();
                default:
                    return new Fragment();
            }
        }
    }
}