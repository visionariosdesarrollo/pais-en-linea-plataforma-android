package com.nh.quickly.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nh.quickly.R;
import com.nh.quickly.activities.HomeFragmentActivity;
import com.nh.quickly.nh.fragments.NHFragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;

/**
 * Created by Diego on 26/10/2015.
 */
public class DataSynchronizationFragment extends NHFragment {
    public final static String TAG = DataSynchronizationFragment.class.toString();
    private View rootView;
    public Callbacks callbacks;
    private TextView synchronizationTxt;
    private TextView synchronizationAuxTxt;
    private ProgressBar synchronizationPb;
    private Handler handlerDataSynchronizationReload= new Handler(){
        @Override
        public void handleMessage(Message msg) {
            dataSynchronizationFragment();
        }
    };

    private Handler handlerSetText= new Handler(){
        @Override
        public void handleMessage(Message message) {
            synchronizationPb.setVisibility(View.VISIBLE);
            synchronizationTxt.setText((String) message.obj);
            synchronizationAuxTxt.setVisibility(View.VISIBLE);
        }
    };

    private Handler handlerChangeHomeActivity= new Handler(){
        @Override
        public void handleMessage(Message message) {
            changeHomeActivity();
        }
    };

    private Handler handlerError= new Handler(){
        @Override
        public void handleMessage(Message message) {
            synchronizationPb.setVisibility(View.GONE);
            synchronizationTxt.setText(R.string.data_synchronization_error);
            synchronizationAuxTxt.setVisibility(View.GONE);
        }
    };

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            handlerDataSynchronizationReload.sendEmptyMessage(0);
        }
    };

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    public interface Callbacks {
        public void onProgressDialogFragmentShow(Boolean progressBarShow, int rIdMessageText, int rIdhandlerTextTxt, View.OnClickListener handlerTxtOnClickListener);
        public void onProgressDialogGone();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.data_synchronization, viewGroup, false);

        callbacks.onProgressDialogGone();

        synchronizationTxt = (TextView) rootView.findViewById(R.id.synchronization_txt);
        synchronizationAuxTxt = (TextView) rootView.findViewById(R.id.synchronization_aux_txt);
        synchronizationPb = (ProgressBar) rootView.findViewById(R.id.synchronization_pb);

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        dataSynchronizationFragment();
    }

    private void dataSynchronizationFragment(){
        //new EntitiesAsyncTask().execute();
    }

    private void changeHomeActivity() {
        Intent intent = new Intent(getActivity(), HomeFragmentActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("userIsNew", true); //Your id
        startActivity(intent);
        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar Callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }
/*
    public class EntitiesAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Message message = Message.obtain();
            message.obj = getResources().getString(R.string.synchronizing_entities);
            message.setTarget(handlerSetText);
            message.sendToTarget();
        }

        @Override
        protected String doInBackground(String... arguments) {
            try {
                return HttpRequest.get(App.URL_API_ENTITIES, true, new String[]{"cityId", String.valueOf(Login.getInstanceUser(getContext()).getCity().getId())})
                        .accept(HttpRequest.CONTENT_TYPE_JSON)
                        .body();
            } catch (HttpRequest.HttpRequestException httpRequestException) {
                publishProgress();
                cancel(true);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... arguments) {
            super.onProgressUpdate(arguments);
            callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(getContext()).entities(response);
                new EventsAsyncTask().execute();
            } catch (Exception exception) {
                callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
            }
        }
    }

    public class EventsAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Message message = Message.obtain();
            message.obj = getResources().getString(R.string.synchronizing_events);
            message.setTarget(handlerSetText);
            message.sendToTarget();
        }

        @Override
        protected String doInBackground(String... arguments) {
            try {
                return HttpRequest.get(App.URL_API_EVENTS, true, new String[]{"cityId", String.valueOf(Login.getInstanceUser(getContext()).getCity().getId())})
                        .accept(HttpRequest.CONTENT_TYPE_JSON)
                        .body();
            } catch (HttpRequest.HttpRequestException httpRequestException) {
                publishProgress();
                cancel(true);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... arguments) {
            super.onProgressUpdate(arguments);
            callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(getContext()).events(response);
                new FilesAsyncTask().execute();
            } catch (Exception exception) {
                callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
            }
        }
    }

    private class FilesAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Message message = Message.obtain();
            message.obj = getResources().getString(R.string.synchronizing_entities_images);
            message.setTarget(handlerSetText);
            message.sendToTarget();
        }

        @Override
        protected String doInBackground(String... params) {
            ArrayList<Entity> arrayListEntities = new Entity().all(getContext());
            if(arrayListEntities.size() > 0) {
                Iterator<Entity> entities = arrayListEntities.iterator();

                while (entities.hasNext()) {
                    String image = entities.next().getImage();
                    if((image != null) && (image.length() > 0)) {
                        String url = App.URL_API_DIRECTORY_ENTITIES_IMAGES + image;
                        getBitmap(url, image);
                    }
                }
            }

            return null;
        }

        private synchronized void getBitmap(String url, String name) {
            File file = new File(App.cacheDir(getContext()), name);

            if(!file.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath()); // Is the bitmap in our cache?

                if (bitmap == null) { // Nope, have to download it
                    try {
                        bitmap = BitmapFactory.decodeStream(new URL(url)
                                .openConnection().getInputStream());

                        writeFile(bitmap, file); // Save bitmap to cache for later
                    } catch (FileNotFoundException fileNotFoundException) {
                        fileNotFoundException.printStackTrace();
                    } catch (Exception exception) {
                        // TODO: handle exception
                        exception.printStackTrace();
                    }
                }
            }
        }

        private void writeFile(Bitmap bmp, File f) {
            FileOutputStream out = null;

            try {
                out = new FileOutputStream(f);
                bmp.compress(Bitmap.CompressFormat.PNG, 80, out);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null)
                        out.close();
                } catch (Exception exception) {
                    // TODO: handle exception
                    exception.printStackTrace();
                }
            }
        }

        @Override
        protected void onPostExecute(String response) {
            new DepartmentsAsyncTask().execute();
        }
    }

    public class DepartmentsAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Message message = Message.obtain();
            message.obj = getResources().getString(R.string.synchronizing_departments);
            message.setTarget(handlerSetText);
            message.sendToTarget();
        }

        @Override
        protected String doInBackground(String... arguments) {
            try {
                return HttpRequest.get(App.URL_API_DEPARTMENTS, true, new String[]{"countryId", String.valueOf(SynchronizeDataJsonToDataBase.COUNTRY)})
                        .accept(HttpRequest.CONTENT_TYPE_JSON)
                        .body();
            } catch (HttpRequest.HttpRequestException httpRequestException) {
                publishProgress();
                cancel(true);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... arguments) {
            super.onProgressUpdate(arguments);
            callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(getContext()).departments(response);
                new CitiesAsyncTask().execute();
            } catch (Exception exception) {
                callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
            }
        }
    }

    public class CitiesAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Message message = Message.obtain();
            message.obj = getResources().getString(R.string.synchronizing_cities);
            message.setTarget(handlerSetText);
            message.sendToTarget();
        }

        @Override
        protected String doInBackground(String... arguments) {
            try {
                return HttpRequest.get(App.URL_API_CITIES, true, new String[]{"countryId", String.valueOf(SynchronizeDataJsonToDataBase.COUNTRY)})
                        .accept(HttpRequest.CONTENT_TYPE_JSON)
                        .body();
            } catch (HttpRequest.HttpRequestException httpRequestException) {
                publishProgress();
                cancel(true);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... arguments) {
            super.onProgressUpdate(arguments);
            callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(getContext()).cities(response);
                new EventsOfUserAsyncTask().execute();
            } catch (Exception exception) {
                callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
            }
        }
    }

    public class EventsOfUserAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Message message = Message.obtain();
            message.obj = getResources().getString(R.string.synchronizing_activities_of_user);
            message.setTarget(handlerSetText);
            message.sendToTarget();
        }

        @Override
        protected String doInBackground(String... arguments) {
            try {
                return HttpRequest.get(App.URL_API_EVENTS_OF_USER, true, new String[]{"userId", String.valueOf(Login.getInstanceUser(getContext()).getId())})
                        .accept(HttpRequest.CONTENT_TYPE_JSON)
                        .body();
            } catch (HttpRequest.HttpRequestException httpRequestException) {
                publishProgress();
                cancel(true);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... arguments) {
            super.onProgressUpdate(arguments);
            callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(getContext()).eventsOfUser(response);
                handlerChangeHomeActivity.sendEmptyMessage(0);
            } catch (Exception exception) {
                callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, onClickListener);
            }
        }
    }
    */
}
