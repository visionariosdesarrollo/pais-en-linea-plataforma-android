package com.nh.quickly.fragments.menu.options;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.EventQuickly;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.database.models.NHNotificationManagerModel;
import com.nh.quickly.nh.database.models.NHSyncModel;
import com.nh.quickly.nh.fragments.NHFragment;
import com.nh.quickly.nh.sync.volley.NHRoundNetworkImageView;
import com.nh.quickly.nh.sync.volley.NHVolley;
import com.nh.quickly.nh.utils.NHArrayUtils;
import com.nh.quickly.nh.utils.NHDateUtils;
import com.nh.quickly.notification.manager.NotificationBroadcastReceiver;

import java.lang.annotation.Target;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Diego on 26/10/2015.
 */
public class DescriptionEventFragment extends NHFragment implements View.OnClickListener, TextToSpeech.OnUtteranceCompletedListener {
    public static String TAG = DescriptionEventFragment.class.getName();
    private ContentResolver contentResolver;
    private Cursor cursorEvent;
    private City city;
    private long eventId;
    private Button attend, comment, cancel;
    private TextToSpeech textToSpeech;
    private String textToSpeechText;
    private TextView commentMessage;
    private ImageButton back, speaker;
    private LinearLayout emptyView;
    private DescriptionEventsObserver descriptionEventsObserver;
    private final String SPEAK_ID = "SPEAK";
    private HashMap<String, String> textToSpeechParams;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    onReload();
                    break;
            }
        }
    };

    public static DescriptionEventFragment getInstance() {
        return new DescriptionEventFragment();
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentResolver = getActivity().getContentResolver();
        city = PreferencesFragment.getCity(getContext());

        descriptionEventsObserver = new DescriptionEventsObserver(handler);
        getActivity().getContentResolver().registerContentObserver(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH), false, descriptionEventsObserver);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        rootView = layoutInflater.inflate(R.layout.event_description_fragment, viewGroup, false);
        emptyView = (LinearLayout) rootView.findViewById(R.id.empty);
        back = (ImageButton) rootView.findViewById(R.id.back);
        attend = (Button) rootView.findViewById(R.id.attend);
        comment = (Button) rootView.findViewById(R.id.comment);
        commentMessage = (TextView) rootView.findViewById(R.id.commentMessage);
        cancel = (Button) rootView.findViewById(R.id.cancel);
        speaker = (ImageButton) rootView.findViewById(R.id.speaker);

        emptyView.setVisibility(View.VISIBLE);

        if (getArguments().containsKey(Event.Columns.ID)) {
            eventId = getArguments().getLong(Event.Columns.ID);
            start();
            addListenToOnTouch();
        }

        return rootView;
    }

    private void start() {
        hideViews();
        startButtons();
        startTextToSpeech();
        changeEventSelected();
        //startSpeech();
    }

    private void hideViews() {
        attend.setVisibility(View.GONE);
        cancel.setVisibility(View.GONE);
        comment.setVisibility(View.GONE);
        commentMessage.setVisibility(View.GONE);
    }

    private void startButtons() {
        back.setOnClickListener(this);
        attend.setOnClickListener(this);
        cancel.setOnClickListener(this);
        comment.setOnClickListener(this);
        speaker.setOnClickListener(this);
        emptyView.setOnClickListener(this);
    }

    private void startTextToSpeech() {
        textToSpeech = new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                textToSpeechParams = new HashMap<String, String>();
                textToSpeechParams.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, SPEAK_ID);

                if (status != TextToSpeech.ERROR)
                    textToSpeech.setLanguage(Locale.ROOT);
            }
        });

        textToSpeech.setOnUtteranceCompletedListener(this);
    }

    private void startSpeech() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "ACEPTO TEXTO DE ENTRADA");
        try {
            startActivityForResult(intent, 100);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getContext(), "NO ACEPTO TEXTO DE ENTRADA", Toast.LENGTH_SHORT);
        }
    }

    private void cursorClose() {
        if (cursorEvent != null)
            cursorEvent.close();
    }

    private void onBack() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void onCancel() {
        int numberModifieds = 0;

        if (cursorEvent != null
                && cursorEvent.getInt(Event.Columns.EVENT_QUICKLY_FULL__ID_POSITION) > 0) {
            ContentValues eventQuicklyContentValues = new ContentValues();
            eventQuicklyContentValues.put(EventQuickly.Columns.STATUS_SYNC, NHSyncModel.STATUS_PENDING);
            eventQuicklyContentValues.put(EventQuickly.Columns.STATUS, EventQuickly.STATUS_NO_SCHEDULED);
            numberModifieds = contentResolver.update(
                    Uri.parse(NHContentProvider.SCHEME + EventQuickly.ContentProvider.QUERY_PATH + cursorEvent.getInt(Event.Columns.EVENT_QUICKLY_FULL__ID_POSITION)),
                    eventQuicklyContentValues,
                    null,
                    null
            );

            if (numberModifieds > 0) {
                getActivity().getContentResolver().notifyChange(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH), null);
            }
        }

        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.slide_out_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_in_right)
                .remove(DescriptionEventFragment.this)
                .disallowAddToBackStack()
                .commit();

        onBack();
    }

    private void onAttend() {
        Boolean result = false;
        ContentValues eventQuicklyContentValues = new ContentValues();
        eventQuicklyContentValues.put(EventQuickly.Columns.STATUS_SYNC, NHSyncModel.STATUS_PENDING);

        if (cursorEvent != null
                && cursorEvent.getInt(Event.Columns.EVENT_QUICKLY_FULL__ID_POSITION) > 0) {
            eventQuicklyContentValues.put(EventQuickly.Columns.STATUS, EventQuickly.STATUS_SCHEDULED);
            int numberModifieds = contentResolver.update(
                    Uri.parse(NHContentProvider.SCHEME + EventQuickly.ContentProvider.QUERY_PATH + cursorEvent.getInt(Event.Columns.EVENT_QUICKLY_FULL__ID_POSITION)),
                    eventQuicklyContentValues,
                    null,
                    null
            );

            result = numberModifieds > 0;
        } else {
            eventQuicklyContentValues.put(EventQuickly.Columns.USER_ID, 1);
            eventQuicklyContentValues.put(EventQuickly.Columns.EVENT_ID, cursorEvent.getInt(Event.Columns.ID_POSITION));
            eventQuicklyContentValues.put(EventQuickly.Columns.STATUS, EventQuickly.STATUS_SCHEDULED);
            Uri newEventQuickly = contentResolver.insert(
                    Uri.parse(NHContentProvider.SCHEME + EventQuickly.ContentProvider.QUERY_PATH),
                    eventQuicklyContentValues
            );

            result = ((newEventQuickly != null) && (Integer.valueOf(newEventQuickly.getLastPathSegment()) > 0));
        }

        if (result) {
            getActivity().getContentResolver().notifyChange(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH), null);

            App.quicklySoundPlay(getContext());
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.quickly, R.anim.quickly)
                    .remove(DescriptionEventFragment.this)
                    .disallowAddToBackStack()
                    .commit();
        } else {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_out_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_in_right)
                    .remove(DescriptionEventFragment.this)
                    .disallowAddToBackStack()
                    .commit();
        }

        onBack();
    }

    private void onComment() {
        long time = new Date().getTime() / NHDateUtils.TIMESTAMP_GAP;

        if (cursorEvent != null
                && ((time >= cursorEvent.getLong(Event.Columns.EVENT_TIME_STAMP_END_POSITION))
                && (time <= cursorEvent.getLong(Event.Columns.EVENT_TIME_STAMP_END_LIMIT_POSITION)))) {
            commentMessage.setVisibility(View.GONE);
            return;
        } else
            commentMessage.setVisibility(View.VISIBLE);

    }

    private void onEventStatusToViewed() {
        if (cursorEvent != null && (cursorEvent.getInt(Event.Columns.STATUS_NOTIFICATION_MANAGER_POSITION) != NHNotificationManagerModel.STATUS_VIEWED)) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(Event.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_VIEWED);
            int numberModifieds = contentResolver.update(
                    Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH + eventId),
                    contentValues,
                    null,
                    null
            );
        }
    }

    private void changeEventSelected() {
        try {
            cursorEvent = contentResolver.query(
                    Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH + eventId),
                    NHArrayUtils.mergeStringArrays(NHArrayUtils.mergeStringArrays(Event.Columns.getFullColumns(), Event.Columns.getFullColumnsEntity()), Event.Columns.getFullColumnsEventQuickly()),
                    null,
                    null,
                    null
            );

            if (cursorEvent != null && cursorEvent.getCount() > 0) {
                cursorEvent.moveToFirst();
                onEventStatusToViewed();

                Date dateStart = NHDateUtils.dateFromTimestampFromPhpToJava(cursorEvent.getLong(Event.Columns.EVENT_TIME_STAMP_START_POSITION));
                Date dateEnd = NHDateUtils.dateFromTimestampFromPhpToJava(cursorEvent.getLong(Event.Columns.EVENT_TIME_STAMP_END_POSITION));

                String dayText = NHDateUtils.simpleDateFormat("EEEE", dateStart);
                dayText = dayText.substring(0, 1).toUpperCase() + dayText.substring(1).toLowerCase();
                String monthText = NHDateUtils.simpleDateFormat("MMMM", dateStart).toLowerCase();
                monthText = monthText.substring(0, 1).toUpperCase() + monthText.substring(1).toLowerCase();

                String dateText = String.format(getActivity().getResources().getString(R.string.event_description_format_date), dayText, NHDateUtils.simpleDateFormat(" d ", dateStart), monthText);
                String timeText = String.format(getActivity().getResources().getString(R.string.event_row_list_default_time_start_format), NHDateUtils.simpleDateFormat("HH:mm", dateStart), NHDateUtils.simpleDateFormat("a", dateStart));

                ((TextView) rootView.findViewById(R.id.eventDateStart)).setText(String.format(getActivity().getResources().getString(R.string.event_description_format_date), dayText, NHDateUtils.simpleDateFormat(" d ", dateStart), monthText));
                ((TextView) rootView.findViewById(R.id.eventTimeStart)).setText(NHDateUtils.simpleDateFormat("HH:mm", dateStart));
                ((TextView) rootView.findViewById(R.id.eventTimeStartMeridian)).setText(NHDateUtils.simpleDateFormat("a", dateStart).toLowerCase());
                ((TextView) rootView.findViewById(R.id.eventTimeEnd)).setText(NHDateUtils.simpleDateFormat("HH:mm", dateEnd));
                ((TextView) rootView.findViewById(R.id.eventTimeEndMeridian)).setText(NHDateUtils.simpleDateFormat("a", dateEnd).toLowerCase());
                ((TextView) rootView.findViewById(R.id.name)).setText(cursorEvent.getString(Event.Columns.NAME_POSITION));
                ((TextView) rootView.findViewById(R.id.site)).setText(cursorEvent.getString(Event.Columns.SITE_POSITION));
                ((TextView) rootView.findViewById(R.id.body)).setText(cursorEvent.getString(Event.Columns.BODY_POSITION));
                ((TextView) rootView.findViewById(R.id.entityName)).setText(cursorEvent.getString(Event.Columns.ENTITY_FULL_NAME_POSITION));
                textToSpeechText = String.format(getActivity().getResources().getString(R.string.event_speaker_format), cursorEvent.getString(Event.Columns.NAME_POSITION), cursorEvent.getString(Event.Columns.BODY_POSITION), dateText, timeText, cursorEvent.getString(Event.Columns.SITE_POSITION), cursorEvent.getString(Event.Columns.ENTITY_FULL_NAME_POSITION), city.getDescription());

                NHRoundNetworkImageView eventDescriptionImage = (NHRoundNetworkImageView) rootView.findViewById(R.id.eventDescriptionImage);
                eventDescriptionImage.setErrorImageResId(R.drawable.imagen_defecto_actividad);
                String urlImage = cursorEvent.getString(Event.Columns.ENTITY_FULL_IMAGE_POSITION);
                if (!TextUtils.isEmpty(urlImage)) {
                    eventDescriptionImage.setImageUrl(App.URL_API_ENTITIES_IMAGES + urlImage, NHVolley.getInstance(getActivity()).getImageLoader());
                } else
                    eventDescriptionImage.setImageResource(R.drawable.imagen_defecto_actividad);

                emptyView.setVisibility(View.GONE);
                TextView eventStatus = (TextView) rootView.findViewById(R.id.status);
                if (TextUtils.equals(cursorEvent.getString(Event.Columns.STATUS_POSITION), Event.STATUS_CANCELED)) {
                    eventStatus.setText(getActivity().getApplication().getResources().getString(R.string.event_description_default_status));
                    eventStatus.setTextColor(getActivity().getApplication().getResources().getColor(R.color.red_notification_bar));
                    eventStatus.setVisibility(TextView.VISIBLE);
                    return;
                } else {
                    eventStatus.setVisibility(View.GONE);
                    if (cursorEvent.getInt(Event.Columns.EVENT_QUICKLY_FULL__ID_POSITION) > 0) {
                        if (TextUtils.equals(cursorEvent.getString(Event.Columns.EVENT_QUICKLY_FULL_STATUS_POSITION), EventQuickly.STATUS_SCHEDULED)) {
                            long time = new Date().getTime() / NHDateUtils.TIMESTAMP_GAP;

                            if (time <= cursorEvent.getLong(Event.Columns.EVENT_TIME_STAMP_END_POSITION)) {
                                cancel.setVisibility(View.VISIBLE);
                                comment.setVisibility(View.VISIBLE);
                                return;
                            } else if ((time >= cursorEvent.getLong(Event.Columns.EVENT_TIME_STAMP_END_POSITION))
                                    && (time <= cursorEvent.getLong(Event.Columns.EVENT_TIME_STAMP_END_LIMIT_POSITION))) {
                                comment.setVisibility(View.VISIBLE);
                                return;
                            }
                        } else if (TextUtils.equals(cursorEvent.getString(Event.Columns.EVENT_QUICKLY_FULL_STATUS_POSITION), EventQuickly.STATUS_NO_SCHEDULED)) {
                            attend.setVisibility(View.VISIBLE);
                            return;
                        }
                    } else {
                        attend.setVisibility(View.VISIBLE);
                        return;
                    }
                }
            }

            emptyView.setVisibility(View.VISIBLE);
        } catch (NullPointerException nullPointerException) {
        } catch (IllegalArgumentException illegalArgumentException) {
        } catch (Exception exception) {
        }
    }

    private void addListenToOnTouch() {
        View.OnTouchListener listenersOnTouch = new View.OnTouchListener() {
            final int touchLeft = 150;
            private float lastGetX, lastGetY;

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        lastGetX = Math.abs(event.getX());
                        lastGetY = Math.abs(event.getY());
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        float getX = Math.abs(event.getX());
                        float getY = Math.abs(event.getY());
                        if ((lastGetX - touchLeft) > getX)
                            onBack();
                        break;
                    }
                }
                return true;
            }
        };
        rootView.setOnTouchListener(listenersOnTouch);
    }

    private void textToSpeechStop(){
        if(textToSpeech != null){
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.attend:
                onAttend();
                break;
            case R.id.cancel:
                onCancel();
                break;
            case R.id.comment:
                onComment();
                break;
            case R.id.speaker:
                if(textToSpeech.isSpeaking())
                    textToSpeechStop();

                speaker.setEnabled(false);
                textToSpeech.speak(textToSpeechText, TextToSpeech.QUEUE_FLUSH, textToSpeechParams);
                break;
            case R.id.empty:
            default:
                onBack();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        textToSpeechStop();
        cursorClose();
        contentResolver.unregisterContentObserver(descriptionEventsObserver);

    }

    public void onReload() {
        start();
    }

    @Override
    public void onUtteranceCompleted(String utteranceId) {
        if (utteranceId.equals(SPEAK_ID)) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    speaker.setEnabled(true);
                }
            });
        }
    }

    class DescriptionEventsObserver extends ContentObserver {
        public DescriptionEventsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri changeUri) {
            handler.sendEmptyMessage(0);
        }
    }
}