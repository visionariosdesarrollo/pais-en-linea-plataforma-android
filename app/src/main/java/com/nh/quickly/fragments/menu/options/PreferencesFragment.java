package com.nh.quickly.fragments.menu.options;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.fragments.HomeContentFragment;
import com.nh.quickly.fragments.menu.MenuFragment;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.fragments.NHPreferenceFragment;
import com.nh.quickly.preferences.CitiesListPreference;
import com.nh.quickly.preferences.DepartmentsListPreference;

/**
 * Created by Diego on 26/10/2015.
 */
public class PreferencesFragment extends NHPreferenceFragment {
    public static String TAG = CalendarEventsFragment.class.getName();
    DepartmentsListPreference department;
    CitiesListPreference city;
    PreferencesFragmentSharedPreferenceChangeListene preferencesFragmentSharedPreferenceChangeListene;

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferencesFragmentSharedPreferenceChangeListene = new PreferencesFragmentSharedPreferenceChangeListene();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(preferencesFragmentSharedPreferenceChangeListene);

        addPreferencesFromResource(R.xml.preferences);
        start();
    }

    @Nullable
    static City setCityPreference(Context context, int cityId){
        City city = null;

        if(cityId <= 0) {
            SharedPreferences sharedPreferences  = context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS);
            String cityIdPreferences = sharedPreferences.getString(CitiesListPreference.KEY, "");

            if(!TextUtils.isEmpty(cityIdPreferences))
                cityId = Integer.valueOf(cityIdPreferences);
        }

        if(cityId > 0) {
            Cursor cityCursor = context.getContentResolver().query(
                    Uri.parse(NHContentProvider.SCHEME + City.ContentProvider.QUERY_PATH + cityId),
                    City.Columns.getColumns(),
                    null,
                    null,
                    null
            );

            if(cityCursor != null && cityCursor.getCount() > 0) {
                cityCursor.moveToFirst();

                city  = new City(
                        cityCursor.getInt(City.Columns.ID_POSITION),
                        cityCursor.getInt(City.Columns.DEPARTMENT_ID_POSITION),
                        cityCursor.getString(City.Columns.NAME_POSITION),
                        cityCursor.getString(City.Columns.DESCRIPTION_POSITION),
                        cityCursor.getString(City.Columns.IMAGE_POSITION)
                );
                cityCursor.close();
            }
        }

        return city;
    }

    @Nullable
    public static City getCity(Context context){
        return setCityPreference(context, 0);
    }

    void start(){
        department = (DepartmentsListPreference) findPreference(DepartmentsListPreference.KEY);
        city = (CitiesListPreference) findPreference(CitiesListPreference.KEY);
        setOnListener();

        if(!TextUtils.isEmpty(department.getValue()))
            setCity(Integer.valueOf(department.getValue()));
        else
            setCity(0);
    }

    void setCity(int departmentId){
        if(departmentId > 0) {
            if (!city.setCities(departmentId)) {
                city.setEnabled(true);
                return;
            }
        }

        city.setEnabled(false);
    }

    void setOnListener(){
        department.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String value = (String) newValue;
                if(!TextUtils.isEmpty(value))
                    setCity(Integer.valueOf(value));

                return true;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(preferencesFragmentSharedPreferenceChangeListene);
    }

    class PreferencesFragmentSharedPreferenceChangeListene implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if(TextUtils.equals(CitiesListPreference.KEY, key)) {
                HomeContentFragment.getInstance().handler.sendEmptyMessage(HomeContentFragment.HANDLER_RELOAD_MENU);
                HomeContentFragment.getInstance().handler.sendEmptyMessage(HomeContentFragment.HANDLER_RELOAD_SYNC);
            }
        }
    }
}