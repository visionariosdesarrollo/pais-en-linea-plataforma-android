package com.nh.quickly.fragments.menu.options.adapters;

import android.content.Context;
import android.database.Cursor;
;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.Model;
import com.nh.quickly.nh.sync.volley.NHVolley;
import com.nh.quickly.nh.utils.NHDateUtils;

import java.util.Date;

/**
 * Created by Diego on 25/10/2015.
 */
public class EventCursorAdapter extends CursorAdapter {
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public EventCursorAdapter(Context context) {
        super(context, null, false);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mLayoutInflater.inflate(R.layout.events_row_list, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        try {
            Date date = NHDateUtils.dateFromTimestampFromPhpToJava(cursor.getLong(Event.Columns.EVENT_TIME_STAMP_START_POSITION));

            String dayText = NHDateUtils.simpleDateFormat("E", date);
            dayText = dayText.substring(0, 1).toUpperCase() + dayText.substring(1).toLowerCase();
            String monthText = NHDateUtils.simpleDateFormat("MMM", date).toLowerCase();
            monthText = monthText.substring(0, 1).toUpperCase() + monthText.substring(1).toLowerCase();

            ((TextView) view.findViewById(R.id.eventDateStart)).setText(String.format(context.getResources().getString(R.string.event_row_list_format_date), dayText, NHDateUtils.simpleDateFormat(" d ", date), monthText));
            ((TextView) view.findViewById(R.id.eventDateStart)).setText(dayText + NHDateUtils.simpleDateFormat(" d ", date) + monthText);
            ((TextView) view.findViewById(R.id.eventTimeStart)).setText(NHDateUtils.simpleDateFormat("HH:mm", date));
            ((TextView) view.findViewById(R.id.eventTimeStartMeridiam)).setText(NHDateUtils.simpleDateFormat("a", date).toLowerCase());
            ((TextView) view.findViewById(R.id.eventName)).setText(cursor.getString(Event.Columns.NAME_POSITION));
            ((TextView) view.findViewById(R.id.entityName)).setText(cursor.getString(Event.Columns.ENTITY_FULL_NAME_POSITION));

            TextView eventStatus = (TextView) view.findViewById(R.id.eventStatus);
            if (TextUtils.equals(cursor.getString(Event.Columns.STATUS_POSITION), Model.STATUS_CANCELED)) {
                eventStatus.setText(cursor.getString(Event.Columns.STATUS_POSITION));
                eventStatus.setVisibility(TextView.VISIBLE);
            } else
                eventStatus.setVisibility(TextView.GONE);

            NetworkImageView eventListImage = (NetworkImageView) view.findViewById(R.id.eventListImage);
            eventListImage.setErrorImageResId(R.drawable.imagen_defecto_listado);
            String urlImage = cursor.getString(Event.Columns.ENTITY_FULL_IMAGE_POSITION);
            if (!TextUtils.isEmpty(urlImage))
                eventListImage.setImageUrl(App.URL_API_ENTITIES_IMAGES + urlImage, NHVolley.getInstance(context).getImageLoader());
            else
                eventListImage.setImageResource(R.drawable.imagen_defecto_listado);
        }
        catch (NullPointerException nullPointerException) {}
        catch (IllegalArgumentException illegalArgumentException) {}
        catch (Exception exception) {}
    }
}