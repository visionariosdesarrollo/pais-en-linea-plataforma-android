package com.nh.quickly.fragments.menu.options.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.Release;
import com.nh.quickly.nh.sync.volley.NHVolley;

;

/**
 * Created by Diego on 25/10/2015.
 */
public class ReleasesCursorAdapter extends CursorAdapter {
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public ReleasesCursorAdapter(Context context) {
        super(context, null, false);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mLayoutInflater.inflate(R.layout.releases_row_list, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        try {
            String role = cursor.getString(Release.Columns.USER_ROLE_POSITION);
            role = role.substring(0, 1).toUpperCase() + role.substring(1).toLowerCase();

            ((TextView) view.findViewById(R.id.userName)).setText(cursor.getString(Release.Columns.USER_NAME_POSITION));
            ((TextView) view.findViewById(R.id.userRole)).setText(role);
            ((TextView) view.findViewById(R.id.name)).setText(cursor.getString(Release.Columns.NAME_POSITION));
            ((TextView) view.findViewById(R.id.body)).setText(cursor.getString(Release.Columns.BODY_POSITION));

            NetworkImageView userImage = (NetworkImageView) view.findViewById(R.id.userImage);
            userImage.setErrorImageResId(R.drawable.release_image_list_default);
            String urlImage = cursor.getString(Release.Columns.USER_IMAGE_POSITION);
            if (!TextUtils.isEmpty(urlImage))
                userImage.setImageUrl(App.URL_API_USERS_IMAGES + urlImage, NHVolley.getInstance(context).getImageLoader());
            else
                userImage.setImageResource(R.drawable.imagen_defecto_listado);
        }
        catch (NullPointerException nullPointerException) {}
        catch (IllegalArgumentException illegalArgumentException) {}
        catch (Exception exception) {}
    }
}