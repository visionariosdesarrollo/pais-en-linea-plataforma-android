package com.nh.quickly.fragments;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.ContentProviderApp;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.Release;
import com.nh.quickly.fragments.menu.MenuFragment;
import com.nh.quickly.fragments.menu.SubMenuFragment;
import com.nh.quickly.fragments.menu.options.CalendarEventsFragment;
import com.nh.quickly.fragments.menu.options.EventsActivesFragment;
import com.nh.quickly.fragments.menu.options.EventsFragment;
import com.nh.quickly.fragments.menu.options.PreferencesFragment;
import com.nh.quickly.fragments.menu.options.ReleasesFragment;
import com.nh.quickly.fragments.menu.options.ShudleUserEventsFragment;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.database.models.NHNotificationManagerModel;
import com.nh.quickly.nh.fragments.NHFragment;
import com.nh.quickly.nh.utils.NHFileUtils;
import com.nh.quickly.sync.SyncAccount;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Diego on 26/10/2015.
 */
public class HomeContentFragment extends NHFragment {
    public final static String TAG = HomeContentFragment.class.toString();
    private static HomeContentFragment homeContentFragment;
    EventsObserver homeContentFragmentEventsObserver;
    ReleasesObserver homeContentFragmentReleasesObserver;
    public static final int HANDLER_RELOAD_EVENTS = 0;
    public static final int HANDLER_EVENTS_STATUS_TO_PENDING = 1;
    public static final int HANDLER_RELOAD_RELEASES = 2;
    public static final int HANDLER_RELEASES_STATUS_TO_PENDING = 3;
    public static final int HANDLER_RELOAD_SYNC = 4;
    public static final int HANDLER_RELOAD_MENU = 5;
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    onReloadEvents();
                    break;
                case 1:
                    onEventsStatusToPending();
                    break;
                case 2:
                    onReloadReleases();
                    break;
                case 3:
                    onReleasesStatusToPending();
                    break;
                case 4:
                    onReloadSync();
                    break;
                case 5:
                    onReloadMenu();
                    break;
            }
        }
    };
    RelativeLayout welcome;
    final long SLLEP = 3000;
    Timer timer;
    TimerTask timerTask;
    ContentResolver contentResolver;
    MenuFragment menuFragment;

    public static HomeContentFragment getInstance() {
        if (homeContentFragment == null)
            homeContentFragment = new HomeContentFragment();

        return homeContentFragment;
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentResolver = getActivity().getContentResolver();

        onEventsStatusToPending();

        homeContentFragmentEventsObserver = new EventsObserver(handler);
        contentResolver.registerContentObserver(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH), false, homeContentFragmentEventsObserver);

        homeContentFragmentReleasesObserver = new ReleasesObserver(handler);
        contentResolver.registerContentObserver(Uri.parse(NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH), false, homeContentFragmentReleasesObserver);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        super.onCreateView(layoutInflater, viewGroup, bundle);
        rootView = layoutInflater.inflate(R.layout.home_content_fragment, viewGroup, false);
        welcome = (RelativeLayout) rootView.findViewById(R.id.welcome);

        changeFragment();
        start();

        return rootView;
    }

    void start() {
        City city = PreferencesFragment.getCity(getContext());
        ImageView cityImage = (ImageView) rootView.findViewById(R.id.cityImage);
        cityImage.setImageResource(R.drawable.ic_logo_quickly_top);
        if (city != null) {
            if (NHFileUtils.existFile(getContext(), App.directoryMediaImagesCities(getContext()), city.getImage())) {
                cityImage.setImageResource(R.drawable.ic_logo_quickly_top);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                File file = new File(NHFileUtils.cacheDir(getContext(), App.directoryMediaImagesCities(getContext())), city.getImage());
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
                cityImage.setImageBitmap(bitmap);
            }
        }
        startTimer();
    }

    void changeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.page, SubMenuFragment.getInstance(), SubMenuFragment.TAG).disallowAddToBackStack().commit();
    }

    void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, SLLEP);
    }

    void stopTimerTask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        welcome.setVisibility(View.GONE);
                        stopTimerTask();
                    }
                });
            }
        };
    }

    void onReloadEvents() {
        ShudleUserEventsFragment.getInstance().handler.sendEmptyMessage(0);
        EventsFragment.getInstance().handler.sendEmptyMessage(0);
        CalendarEventsFragment.getInstance().handler.sendEmptyMessage(0);
        EventsActivesFragment.getInstance().handler.sendEmptyMessage(0);
    }

    void onReloadMenu() {
        Log.d(App.LOG_TAG, "NH::onReloadMenu::0");

        if(menuFragment == null) {
            Log.d(App.LOG_TAG, "NH::onReloadMenu::1");
            menuFragment = (MenuFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.menu_fragment);
        }

        if(menuFragment != null) {
            Log.d(App.LOG_TAG, "NH::onReloadMenu::2");
            menuFragment.handler.sendEmptyMessage(0);
        }
    }

    void onReloadReleases() {
        ReleasesFragment.getInstance().handler.sendEmptyMessage(0);
    }

    void onEventsStatusToPending() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_PENDING);
        int numberModifieds = contentResolver.update(
                Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH),
                contentValues,
                NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER + "!=?",
                new String[]{String.valueOf(NHNotificationManagerModel.STATUS_VIEWED)}
        );

        onReloadEvents();
    }

    void onReleasesStatusToPending() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER, NHNotificationManagerModel.STATUS_PENDING);
        int numberModifieds = contentResolver.update(
                Uri.parse(NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH),
                contentValues,
                NHNotificationManagerModel.Columns.STATUS_NOTIFICATION_MANAGER + "!=?",
                new String[]{String.valueOf(NHNotificationManagerModel.STATUS_VIEWED)}
        );

        onReloadReleases();
    }

    void onReloadSync() {
        getActivity().getContentResolver().notifyChange(Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH), null);
        getActivity().getContentResolver().notifyChange(Uri.parse(NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH), null);

        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(SyncAccount.getInstance(getActivity()), ContentProviderApp.AUTHORITY, settingsBundle);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        contentResolver.unregisterContentObserver(homeContentFragmentEventsObserver);
        contentResolver.unregisterContentObserver(homeContentFragmentReleasesObserver);
    }

    class EventsObserver extends ContentObserver {
        public EventsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri changeUri) {
            handler.sendEmptyMessage(HANDLER_EVENTS_STATUS_TO_PENDING);
        }
    }

    class ReleasesObserver extends ContentObserver {
        public ReleasesObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri changeUri) {
            handler.sendEmptyMessage(HANDLER_RELEASES_STATUS_TO_PENDING);
        }
    }
}