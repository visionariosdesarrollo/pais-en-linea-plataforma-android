package com.nh.quickly.fragments.menu.options;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.EventQuickly;
import com.nh.quickly.database.models.Release;
import com.nh.quickly.fragments.menu.options.adapters.ReleasesCursorAdapter;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.fragments.NHListFragment;
import com.nh.quickly.nh.utils.NHArrayUtils;
import com.nh.quickly.nh.utils.NHDataBaseUtils;
import com.nh.quickly.nh.utils.NHDateUtils;
import com.nh.quickly.nh.utils.NHFileUtils;
import com.nh.quickly.preferences.CitiesListPreference;

import java.io.File;

/**
 * Created by Diego on 26/10/2015.
 */
public class ReleasesFragment extends NHListFragment  implements LoaderManager.LoaderCallbacks<Cursor> {
    public static String TAG = ReleasesFragment.class.getName();
    public final static int POSITION = 3;
    private static  ReleasesFragment releasesFragment;
    LinearLayout emptyView;
    LinearLayout city;
    ImageView cityImage;
    ContentResolver contentResolver;
    ReleasesCursorAdapter releaseCursorAdapter;
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 0:
                    Log.d(App.LOG_TAG, "NH::ReleasesFragment::handler");
                    onReload();
                    break;
            }
        }
    };

    public static ReleasesFragment getInstance(){
        if(releasesFragment == null)
            releasesFragment = new ReleasesFragment();

        return releasesFragment;
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentResolver = getActivity().getContentResolver();

        getLoaderManager().initLoader(Release.ContentProvider.ALL_ROWS_CODE, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.releases_list_fragment, viewGroup, false);
        emptyView = (LinearLayout) rootView.findViewById(R.id.empty);
        city = (LinearLayout) rootView.findViewById(R.id.city);
        cityImage = (ImageView) rootView.findViewById(R.id.cityImage);

        releaseCursorAdapter = new ReleasesCursorAdapter(getActivity());
        setListAdapter(releaseCursorAdapter);

        city.setVisibility(View.GONE);

        return rootView;
    }

    void city(){
        City city = PreferencesFragment.getCity(getActivity());
        cityImage.setImageResource(R.drawable.ic_logo_quickly_top);

        if (city != null
                && !TextUtils.isEmpty(city.getImage())) {
            if(NHFileUtils.existFile(getContext(), App.directoryMediaImagesCities(getContext()), city.getImage())) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                File file = new File(NHFileUtils.cacheDir(getContext(), App.directoryMediaImagesCities(getContext())), city.getImage());
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
                Double resized =  Double.parseDouble(getContext().getResources().getString(R.string.release_image_resized));
                Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth() * resized),  (int)(bitmap.getHeight() * resized), false);
                cityImage.setImageBitmap(bitmapResized);
                this.city.setVisibility(View.VISIBLE);

                return;
            }
        }

        this.city.setVisibility(View.GONE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(App.LOG_TAG, "NH::ReleasesFragment::onCreateLoader");
        City city = PreferencesFragment.getCity(getContext());

        if (city != null) {
            switch (id) {
                case Release.ContentProvider.ALL_ROWS_CODE:
                    String time = String.valueOf(NHDateUtils.currentTimeStampForPhp());
                    String selection = NHDataBaseUtils.concatenateWhereWithAnd(
                            NHDataBaseUtils.concatenateWhereWithAnd(
                                    Release.Columns.RELEASE_TIME_STAMP_START + "<=?",
                                    Release.Columns.RELEASE_TIME_STAMP_END + ">=?"
                            ),
                            Release.Columns.CITY_ID + "=?"
                    );
                    String[] selectionArgs = new String[]{time, time, String.valueOf(city.getId())};

                    Log.d(App.LOG_TAG, "NH:App::EventsFragment::onCreateLoader::" + selection);

                    return new CursorLoader(
                            getActivity().getApplicationContext(),
                            Uri.parse(NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH),
                            Release.Columns.getColumns(),
                            selection,
                            selectionArgs,
                            Release.Columns.RELEASE_TIME_STAMP_START_DESC
                    );
            }
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        city();

        if (cursor == null
                || (cursor != null && cursor.getCount() == 0))
            emptyView.setVisibility(View.VISIBLE);
        else
            emptyView.setVisibility(View.GONE);

        releaseCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        emptyView.setVisibility(View.VISIBLE);
        releaseCursorAdapter.swapCursor(null);
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
    }

    public void onReload(){
        try {
            Log.d(App.LOG_TAG, "NH::ReleasesFragment::onReload");
            getActivity().getSupportLoaderManager().restartLoader(Release.ContentProvider.ALL_ROWS_CODE, null, this);
        } catch (Exception exception) {
        }
    }
}