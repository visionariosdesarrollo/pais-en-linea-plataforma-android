package com.nh.quickly.fragments.menu.options;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Layout;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.fragments.menu.options.adapters.EventActivesCursorAdapter;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.fragments.NHListFragment;
import com.nh.quickly.nh.utils.NHArrayUtils;
import com.nh.quickly.nh.utils.NHDataBaseUtils;
import com.nh.quickly.nh.utils.NHDateUtils;

import java.util.Date;

/**
 * Created by Diego on 26/10/2015.
 */
public class EventsActivesFragment extends NHListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static String TAG = EventsActivesFragment.class.getName();
    public final static int POSITION = 100;
    private static EventsActivesFragment eventsActivesFragment;
    public Callbacks callbacks;
    ImageButton close;
    EventActivesCursorAdapter eventActivesCursorAdapter;
    ContentResolver contentResolver;
    Boolean sliderClosedActive = false;

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    onReload();
                    break;
            }
        }
    };

    public static EventsActivesFragment getInstance() {
        if(eventsActivesFragment == null)
            eventsActivesFragment = new EventsActivesFragment();

        return eventsActivesFragment;
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    public interface Callbacks {
        public void onClickListItemSelected(long id);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentResolver = getActivity().getContentResolver();

        getLoaderManager().initLoader(Event.ContentProvider.ALL_ROWS_CODE, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        rootView = layoutInflater.inflate(R.layout.events_actives_list_fragment, viewGroup, false);
        close = (ImageButton) rootView.findViewById(R.id.close);

        eventActivesCursorAdapter = new EventActivesCursorAdapter(getActivity());
        setListAdapter(eventActivesCursorAdapter);

        start();

        return rootView;
    }

    private int getRelativeLeft(View view) {
        if (view.getParent() == view.getRootView())
            return view.getLeft();
        else
            return view.getLeft() + getRelativeLeft((View) view.getParent());
    }

    private int getRelativeTop(View view) {
        if (view.getParent() == view.getRootView())
            return view.getTop();
        else
            return view.getTop() + getRelativeTop((View) view.getParent());
    }

    void start(){
        addListenToOnTouch();
    }

    private int _xDelta;
    private int _yDelta;
    void addListenToOnTouch() {
        /*
        close.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                final int Y = (int) event.getRawY();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        _xDelta = X - lParams.leftMargin;
                        _yDelta = Y - lParams.topMargin;
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                                .getLayoutParams();
                        layoutParams.leftMargin = X - _xDelta;
                        layoutParams.topMargin = Y - _yDelta;
                        view.setLayoutParams(layoutParams);
                        break;
                }
                rootView.invalidate();
                return true;
            }
        });


        View.OnTouchListener listenersOnTouch = new View.OnTouchListener() {
            private float lastGetY;

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE: {

                        Log.d(App.LOG_TAG, "NH::MOVIMIENTO::pantalla::" + rootView.getHeight() + "::posicion::" + lastGetY);
                        if(sliderClosedActive) {
                            lastGetY = Math.abs(event.getY());
                            ViewGroup.LayoutParams params = rootView.getLayoutParams();
                            params.height = (int) (rootViewHeight + lastGetY);
                            rootView.setLayoutParams(params);
                        }

                        break;
                    }
                    case MotionEvent.ACTION_UP: {

                        float getY = Math.abs(event.getY());
                        if (getY > (rootViewHeight - touchTop)) {
                            //Animation animation = new Animation() {
                            Log.d(App.LOG_TAG, "NH::MOVIMIENTO::ES MAYOR");
                        } else
                            Log.d(App.LOG_TAG, "NH::MOVIMIENTO::NO ES MAYOR");

                        break;
                    }
                }
                return true;
            }
        };

        rootView.setOnTouchListener(listenersOnTouch);




        close.setOnTouchListener(new View.OnTouchListener() {
            float rootViewHeight;
            ViewGroup.LayoutParams params;
            int initialRodPosition;
            int touchPositionY;

            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE: {
                        if(initialRodPosition == 0)
                            initialRodPosition = getRelativeTop(close);

                        rootViewHeight = rootView.getHeight();
                        Log.d(App.LOG_TAG, "NH::MOVIMIENTO::posicion de la barra::" + getRelativeTop(close));
                        Log.d(App.LOG_TAG, "NH::rootViewHeight::" + rootViewHeight);

                        lastGetY = Math.abs(event.getY());
                        Log.d(App.LOG_TAG, "NH::MOVIMIENTO::pantalla::" + rootViewHeight + "::posicion::" + Math.abs(event.getY())+ "::resta::" +(lastGetY - rootViewHeight));

                        ViewGroup.LayoutParams params = rootView.getLayoutParams();
                        params.height = (int) ((rootViewHeight + close.getHeight()) - lastGetY);
                        rootView.setLayoutParams(params);

                        touchPositionY = initialRodPosition - Math.round(event.getY());
                        Log.d(App.LOG_TAG, "NH::MOVIMIENTO::contenedor::" + rootViewHeight + "::toque::" + Math.abs(event.getY())+ "::barra::" + initialRodPosition + "::nueva posicion::" + touchPositionY);
                        //if(touchPositionY < initialRodPosition) {
                            params = rootView.getLayoutParams();
                            params.height = touchPositionY;
                            rootView.setLayoutParams(params);
                        //}
                        break;
                    }
                    case MotionEvent.ACTION_DOWN: {

                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        Log.d(App.LOG_TAG, "NH::MOVIMIENTO::ESTOR FUERA");
                        //sliderClosedActive = false;
                        break;
                    }
                }
                return true;
            }
        });
*/
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        City city = PreferencesFragment.getCity(getContext());
        if (city != null) {
            switch (id) {
                case Event.ContentProvider.ALL_ROWS_CODE:
                    String time = String.valueOf(NHDateUtils.currentTimeStampForPhp());
                    String selection = NHDataBaseUtils.concatenateWhereWithAnd(
                            NHDataBaseUtils.concatenateWhereWithAnd(
                                    Event.Columns.FULL_EVENT_TIME_STAMP_START + "<=?", Event.Columns.FULL_EVENT_TIME_STAMP_END + ">=?"
                            ),
                            Entity.Columns.FULL_CITY_ID + "=?"
                    );

                    return new CursorLoader(
                            getActivity(),
                            Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH),
                            NHArrayUtils.mergeStringArrays(NHArrayUtils.mergeStringArrays(Event.Columns.getFullColumns(), Event.Columns.getFullColumnsEntity()), Event.Columns.getFullColumnsEventQuickly()),
                            selection,
                            new String[]{time, time, String.valueOf(city.getId())},
                            Event.Columns.SORT_BY_FULL_EVENT_TIME_STAMP_START_ASC
                    );
            }
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        eventActivesCursorAdapter.changeCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        eventActivesCursorAdapter.changeCursor(null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " should implement Callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        callbacks.onClickListItemSelected(id);
    }

    public void onReload() {
        try {
            getLoaderManager().restartLoader(Event.ContentProvider.ALL_ROWS_CODE, null, this);
        } catch (Exception exception) {
        }
    }
}