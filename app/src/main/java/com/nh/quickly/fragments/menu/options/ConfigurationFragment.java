package com.nh.quickly.fragments.menu.options;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.fragments.NHFragment;

/**
 * Created by Diego on 26/10/2015.
 */
public class ConfigurationFragment extends NHFragment {
    public static String TAG = CalendarEventsFragment.class.getName();
    View rootView;
    Spinner entities;
    TextView comment;
    Button send;

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        rootView = layoutInflater.inflate(R.layout.configuration_fragment, viewGroup, false);

        entities = (Spinner) rootView.findViewById(R.id.entities);
        comment = (TextView) rootView.findViewById(R.id.comment);
        send = (Button) rootView.findViewById(R.id.send);

        start();

        return rootView;
    }

    void start(){
        Cursor  entitiesContentResolver = getActivity().getContentResolver().query(Uri.parse(NHContentProvider.SCHEME + Entity.ContentProvider.QUERY_PATH), Entity.Columns.getColumns(), null, null, Entity.Columns.SORT_BY_NAME_ASC);
        String entitiesArray[];
        if(entitiesContentResolver != null && entitiesContentResolver.getCount() > 0) {
            entitiesContentResolver.moveToFirst();
            entitiesArray = new String[entitiesContentResolver.getCount()];
            int i = 0;
            do {
                entitiesArray[i] = entitiesContentResolver.getString(Entity.Columns.NAME_POSITION);
                i++;
            }while (entitiesContentResolver.moveToNext());
        } else
            entitiesArray = new String[]{"Seleccione secretaria"};

        ArrayAdapter<String> spinnerArrayAdapterEntities = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, entitiesArray);
        spinnerArrayAdapterEntities.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        entities.setAdapter(spinnerArrayAdapterEntities);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(comment.getText())) {
                    comment.setError("Por favor ingrese el comentario a ser enviado.");
                    return;
                }
                comment.setError(null);
                comment.setText(null);
                Toast toast1 = Toast.makeText(getActivity(),
                                "Tu sugerencia ha sido enviada con éxito.", Toast.LENGTH_SHORT);

                toast1.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL,0,0);
                toast1.show();
            }
        });
    }
}