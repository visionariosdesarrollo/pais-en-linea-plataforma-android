package com.nh.quickly.fragments.helpers;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.nh.quickly.R;
import com.nh.quickly.database.models.DocumentType;
import com.nh.quickly.nh.widgets.NHBaseListAdapter;

import java.util.ArrayList;

/**
 * Created by Diego on 15/04/2016.
 */
public class DocumentTypeListFragment extends ListFragment {
    public final static String TAG = DocumentTypeListFragment.class.toString();
    private View rootView;
    private ArrayList<DocumentType> documentTypes = new ArrayList<DocumentType>();
    public Callbacks callbacks;
    public String documentTypeId;

    public interface Callbacks {
        public void onDocumentTypeListItemClick(String documentTypeId);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        if(getArguments() != null)
            documentTypeId = getArguments().getString("documentTypeId");

        documentTypes = new DocumentType().all(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.list_spinner_fragment, viewGroup, false);
        ((TextView) rootView.findViewById(R.id.title_txt)).setText(getResources().getText(R.string.register_select_city));

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        start();
    }

    public void start() {
        setListAdapter(new NHBaseListAdapter(getActivity(), R.layout.general_list_row, documentTypes) {
            @Override
            public void onRow(Object documentType, View view) {
                DocumentType _documentType = (DocumentType) documentType;

                ((TextView) view.findViewById(R.id.id_txt)).setText(String.valueOf(_documentType.getId()));
                ((TextView) view.findViewById(R.id.name_txt)).setText(_documentType.getName());
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar Callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        TextView itemId = (TextView) view.findViewById(R.id.id_txt);
        callbacks.onDocumentTypeListItemClick(String.valueOf(itemId.getText()));

        getFragmentManager().popBackStack();
    }
}