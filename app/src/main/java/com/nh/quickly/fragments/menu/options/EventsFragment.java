package com.nh.quickly.fragments.menu.options;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Layout;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.EventQuickly;
import com.nh.quickly.fragments.menu.options.adapters.EventCursorAdapter;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.fragments.NHListFragment;
import com.nh.quickly.nh.utils.NHArrayUtils;
import com.nh.quickly.nh.utils.NHDataBaseUtils;
import com.nh.quickly.nh.utils.NHDateUtils;

import java.util.Calendar;
import java.util.Date;

import hirondelle.date4j.DateTime;

/**
 * Created by Diego on 26/10/2015.
 */
public class EventsFragment extends NHListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static String TAG = EventsFragment.class.getName();
    public final static int POSITION = 1;
    private static EventsFragment eventsFragment;
    public Callbacks callbacks;
    public static final int HANDLER_RELOAD_EVENTS = 0;
    public static final int HANDLER_RELOAD_EVENTS_FOR_TIME = 1;
    static final String RELOAD_DATE_TIME = "onReloadDateTime";

    EventCursorAdapter eventCursorAdapter;
    LinearLayout emptyView, information;
    TextView informationText;
    ImageButton informationCancel;
    View footerList;
    ContentResolver contentResolver;
    Date date;
    long time;
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLER_RELOAD_EVENTS:
                    onReload();
                    break;
                case HANDLER_RELOAD_EVENTS_FOR_TIME:
                    onReloadDay((long) msg.obj);
                    break;
            }
        }
    };

    public static EventsFragment getInstance() {
        if (eventsFragment == null)
            eventsFragment = new EventsFragment();

        return eventsFragment;
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    public interface Callbacks {
        public void onClickListItemSelected(long id);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentResolver = getActivity().getContentResolver();

        getLoaderManager().initLoader(Event.ContentProvider.ALL_ROWS_CODE, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        rootView = layoutInflater.inflate(R.layout.events_list_fragment, viewGroup, false);
        emptyView = (LinearLayout) rootView.findViewById(R.id.empty);
        information = (LinearLayout) rootView.findViewById(R.id.information);
        informationText = (TextView) rootView.findViewById(R.id.text);
        informationCancel = (ImageButton) rootView.findViewById(R.id.cancel);

        eventCursorAdapter = new EventCursorAdapter(getActivity());
        setListAdapter(eventCursorAdapter);

        startListeners();

        return rootView;
    }

    void startListeners(){
        informationCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time = 0;
                information.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        City city = PreferencesFragment.getCity(getContext());

        if (city != null) {
            Log.d(App.LOG_TAG, "NH::EVENTS::onCreateLoader::city.getId()::" + city.getId());

            switch (id) {
                case Event.ContentProvider.ALL_ROWS_CODE:
                    String selection;
                    String[] selectionArgs;

                    if (time > 0) {
                        Calendar calendarCurrentDate = Calendar.getInstance();
                        Calendar calendarDate = Calendar.getInstance();
                        calendarDate.setTime(NHDateUtils.dateFromTimestamp(time));

                        if((calendarCurrentDate.get(Calendar.DAY_OF_MONTH) == calendarDate.get(Calendar.DAY_OF_MONTH))
                                && (calendarCurrentDate.get(Calendar.MONTH) == calendarDate.get(Calendar.MONTH))
                                && (calendarCurrentDate.get(Calendar.YEAR) == calendarDate.get(Calendar.YEAR))) {
                            calendarDate.set(Calendar.HOUR_OF_DAY, calendarCurrentDate.get(Calendar.HOUR_OF_DAY));
                            calendarDate.set(Calendar.MINUTE, calendarCurrentDate.get(Calendar.MINUTE));
                            calendarDate.set(Calendar.SECOND, calendarCurrentDate.get(Calendar.SECOND));
                        } else {
                            calendarDate.set(Calendar.HOUR_OF_DAY, 0);
                            calendarDate.set(Calendar.MINUTE, 0);
                            calendarDate.set(Calendar.SECOND, 1);
                        }
                        String startDateForPhp = String.valueOf((calendarDate.getTime()).getTime() / NHDateUtils.TIMESTAMP_GAP);

                        Date informationDate = calendarDate.getTime();
                        informationText.setText(String.format(getResources().getString(R.string.events_list_information_text_date),
                                NHDateUtils.simpleDateFormat("d", informationDate),
                                NHDateUtils.simpleDateFormat("LLLL", informationDate),
                                NHDateUtils.simpleDateFormat("YYYY", informationDate)));

                        calendarDate.set(Calendar.HOUR_OF_DAY, 23);
                        calendarDate.set(Calendar.MINUTE, 59);
                        calendarDate.set(Calendar.SECOND, 59);
                        String endDateForPhp = String.valueOf((calendarDate.getTime()).getTime()  / NHDateUtils.TIMESTAMP_GAP);

                        selection = NHDataBaseUtils.concatenateWhereWithAnd(
                                NHDataBaseUtils.concatenateWhereWithAnd(
                                        NHDataBaseUtils.concatenateWhereWithAnd(
                                                Event.Columns.FULL_EVENT_TIME_STAMP_START + ">=?", Event.Columns.FULL_EVENT_TIME_STAMP_START + "<=?"
                                                //Event.Columns.FULL_EVENT_TIME_STAMP_START + ">=?", Event.Columns.FULL_EVENT_TIME_STAMP_END + "<=?"
                                        ),
                                        EventQuickly.Columns.FULL__ID + " IS NULL"
                                ),
                                Entity.Columns.FULL_CITY_ID + "=?"
                        );
                        selectionArgs = new String[]{startDateForPhp, endDateForPhp, String.valueOf(city.getId())};

                        Log.d(App.LOG_TAG, "NH::CALENDAR::startDateForPhp::" + startDateForPhp + "::endDateForPhp::" + endDateForPhp);
                    } else {
                        selection = NHDataBaseUtils.concatenateWhereWithAnd(
                                NHDataBaseUtils.concatenateWhereWithAnd(
                                        Event.Columns.FULL_EVENT_TIME_STAMP_END + ">=?",
                                        NHDataBaseUtils.concatenateWhereWithOr(
                                                EventQuickly.Columns.FULL__ID + " IS NULL ",
                                                NHDataBaseUtils.concatenateWhereWithAnd(
                                                        EventQuickly.Columns.FULL__ID + " IS NOT NULL",
                                                        EventQuickly.Columns.FULL_STATUS + "=?"
                                                )
                                        )
                                ),
                                Entity.Columns.FULL_CITY_ID + "=?"
                        );
                        selectionArgs = new String[]{String.valueOf(NHDateUtils.currentTimeStampForPhp()), EventQuickly.STATUS_NO_SCHEDULED, String.valueOf(city.getId())};
                    }

                    return new CursorLoader(
                            getActivity(),
                            Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH),
                            NHArrayUtils.mergeStringArrays(Event.Columns.getFullColumns(), Event.Columns.getFullColumnsEntity()),
                            selection,
                            selectionArgs,
                            Event.Columns.SORT_BY_FULL_EVENT_TIME_STAMP_START_ASC
                    );
            }
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.d(App.LOG_TAG, "NH::CALENDAR::onLoadFinished::cursor::" + cursor.getCount());
        if (cursor == null
                || (cursor != null && cursor.getCount() == 0)) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }

        Log.d(App.LOG_TAG, "NH::CALENDAR::onLoadFinished::time::"+time);
        if(time > 0) {
            Log.d(App.LOG_TAG, "NH::CALENDAR::onLoadFinished::time::VISIBLE");
            //information.setVisibility(View.VISIBLE);
        } else {
            Log.d(App.LOG_TAG, "NH::CALENDAR::onLoadFinished::time::GONE");
            //information.setVisibility(View.GONE);
        }

        eventCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        emptyView.setVisibility(View.VISIBLE);
        eventCursorAdapter.swapCursor(null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " should implement Callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        callbacks.onClickListItemSelected(id);
    }

    public void onSelectDate(Date date, View view) {
        this.date = date;
    }

    void onReload() {
        try {
            getActivity().getSupportLoaderManager().restartLoader(Event.ContentProvider.ALL_ROWS_CODE, null, this);
        } catch (Exception exception) {
        }
    }

    void onReloadDay(long time) {
        try {
            this.time = time;
            getActivity().getSupportLoaderManager().restartLoader(Event.ContentProvider.ALL_ROWS_CODE, null, this);
        } catch (Exception exception) {
        }
    }
}