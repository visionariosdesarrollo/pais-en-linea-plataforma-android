package com.nh.quickly.fragments.menu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.fragments.menu.options.ConfigurationFragment;
import com.nh.quickly.fragments.menu.options.EventsActivesFragment;
import com.nh.quickly.fragments.menu.options.PreferencesFragment;
import com.nh.quickly.nh.fragments.NHFragment;
import com.nh.quickly.services.MenuFragmentIntentService;

/**
 * Created by Diego on 26/10/2015.
 */
public class MenuFragment extends NHFragment {
    public final static String TAG = MenuFragment.class.toString();
    public static String ACTIVE_EVENTS = "activeEvents";
    public static String DATE = "date";
    public static String TIME = "time";
    public static String TIME_MERIDIAN = "timeMeridian";
    public static String CITY = "city";
    Intent menuFragmentIntentService;
    IntentFilter intentFilterMenuFragmentBroadcastReceiver;
    MenuFragmentBroadcastReceiver menuFragmentBroadcastReceiver;
    TextView date, time, timeMeridian, city;
    Button activeEvents;
    Boolean isActiveEvents = false;
    ImageButton configuration;
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    onReloadCity();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.menu_fragment, viewGroup, false);

        init();
        return rootView;
    }

    void init() {
        startViews();
        onReloadCity();
        startIntentService();
        startBroadcastReceiver();
        startListeners();
    }

    void startViews() {
        activeEvents = (Button) rootView.findViewById(R.id.activeEvents);
        activeEvents.setVisibility(View.GONE);
        date = (TextView) rootView.findViewById(R.id.date);
        time = (TextView) rootView.findViewById(R.id.time);
        timeMeridian = (TextView) rootView.findViewById(R.id.timeMeridian);
        city = (TextView) rootView.findViewById(R.id.city);
        configuration = (ImageButton) rootView.findViewById(R.id.configuration);
    }

    void startIntentService() {
        menuFragmentIntentService = new Intent(getActivity(), MenuFragmentIntentService.class);
        getActivity().startService(menuFragmentIntentService);
    }

    void startBroadcastReceiver() {
        intentFilterMenuFragmentBroadcastReceiver = new IntentFilter(MenuFragmentBroadcastReceiver.ACTION_UPDATE);
        intentFilterMenuFragmentBroadcastReceiver.addCategory(Intent.CATEGORY_DEFAULT);
        menuFragmentBroadcastReceiver = new MenuFragmentBroadcastReceiver();
        getActivity().registerReceiver(menuFragmentBroadcastReceiver, intentFilterMenuFragmentBroadcastReceiver);
    }

    void startListeners(){
        View.OnClickListener eventsActivesOnClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (isActiveEvents) {
                    isActiveEvents = false;
                    activeEvents.setSelected(isActiveEvents);
                    getActivity().getSupportFragmentManager().popBackStack();
                } else {
                    isActiveEvents = true;
                    activeEvents.setSelected(isActiveEvents);
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_top, R.anim.slide_in_top, R.anim.slide_out_top).add(R.id.page, EventsActivesFragment.getInstance(), EventsActivesFragment.TAG).addToBackStack(EventsActivesFragment.TAG).commit();
                }
            }
        };

        activeEvents.setOnClickListener(eventsActivesOnClickListener);
        city.setOnClickListener(eventsActivesOnClickListener);

        configuration.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.isSelected()) {
                    view.setSelected(false);
                    getActivity().getSupportFragmentManager().popBackStack();
                } else {
                    view.setSelected(true);
                    ConfigurationFragment fragment = new ConfigurationFragment();
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_top, R.anim.slide_in_top, R.anim.slide_out_top)
                            .add(R.id.page, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().stopService(menuFragmentIntentService);
        getActivity().unregisterReceiver(menuFragmentBroadcastReceiver);
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    void onReloadCity(){
        City city = PreferencesFragment.getCity(getContext());

        if (city != null)
            this.city.setText(city.getName());
    }

    public class MenuFragmentBroadcastReceiver extends BroadcastReceiver{
        public static final String ACTION_UPDATE =  "com.nh.quickly.intent.action.MENU_UPDATE";

        @Override
        public void onReceive(Context context, Intent intent) {
            int activeEventsCount = intent.getIntExtra(MenuFragment.ACTIVE_EVENTS, 0);
            if(activeEventsCount > 0) {
                activeEvents.setText(String.valueOf(activeEventsCount));
                activeEvents.setVisibility(View.VISIBLE);
            } else
                activeEvents.setVisibility(View.GONE);

            date.setText(intent.getStringExtra(MenuFragment.DATE));
            time.setText(intent.getStringExtra(MenuFragment.TIME));
            timeMeridian.setText(intent.getStringExtra(MenuFragment.TIME_MERIDIAN));
        }
    }
}