package com.nh.quickly.fragments.menu.options;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nh.quickly.App;
import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.EventQuickly;
import com.nh.quickly.fragments.menu.options.adapters.ShudleUserEventsCursorAdapter;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.fragments.NHListFragment;
import com.nh.quickly.nh.utils.NHArrayUtils;
import com.nh.quickly.nh.utils.NHDataBaseUtils;
import com.nh.quickly.nh.utils.NHDateUtils;

import java.util.Date;

/**
 * Created by Diego on 26/10/2015.
 */
public class ShudleUserEventsFragment extends NHListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static String TAG = ShudleUserEventsFragment.class.getName();
    public final static int POSITION = 0;
    private static ShudleUserEventsFragment shudleUserEventsFragmentFragment;
    public Callbacks callbacks;
    ShudleUserEventsCursorAdapter shudleUserEventsCursorAdapter;
    LinearLayout emptyView;
    ContentResolver contentResolver;
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    onReload();
                    break;
            }
        }
    };

    public static ShudleUserEventsFragment getInstance() {
        if(shudleUserEventsFragmentFragment == null)
            shudleUserEventsFragmentFragment = new ShudleUserEventsFragment();

        return shudleUserEventsFragmentFragment;
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    public interface Callbacks {
        public void onClickListItemSelected(long id);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contentResolver = getActivity().getContentResolver();

        getLoaderManager().initLoader(Event.ContentProvider.ALL_ROWS_CODE, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        rootView = layoutInflater.inflate(R.layout.shudle_user_events_list_fragment, viewGroup, false);
        emptyView = (LinearLayout) rootView.findViewById(R.id.empty);

        shudleUserEventsCursorAdapter = new ShudleUserEventsCursorAdapter(getActivity());
        setListAdapter(shudleUserEventsCursorAdapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        City city = PreferencesFragment.getCity(getContext());
        if (city != null) {
            switch (id) {
                case Event.ContentProvider.ALL_ROWS_CODE:
                    String time = String.valueOf((new Date().getTime() / NHDateUtils.TIMESTAMP_GAP));
                    String selection = NHDataBaseUtils.concatenateWhereWithAnd(
                            NHDataBaseUtils.concatenateWhereWithAnd(
                                    Event.Columns.FULL_EVENT_TIME_STAMP_END_LIMIT + ">=? ",
                                    NHDataBaseUtils.concatenateWhereWithAnd(
                                        EventQuickly.Columns.FULL__ID + " IS NOT NULL",
                                        EventQuickly.Columns.FULL_STATUS + "=?"
                                    )
                            ),
                            Entity.Columns.FULL_CITY_ID + "=?"
                    );

                    return new CursorLoader(
                            getActivity(),
                            Uri.parse(NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH),
                            NHArrayUtils.mergeStringArrays(Event.Columns.getFullColumns(), Event.Columns.getFullColumnsEntity()),
                            selection,
                            new String[]{time, EventQuickly.STATUS_SCHEDULED, String.valueOf(city.getId())},
                            Event.Columns.SORT_BY_FULL_EVENT_TIME_STAMP_START_ASC
                    );
            }
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null)
            emptyView.setVisibility(View.VISIBLE);
        else if (cursor != null && cursor.getCount() == 0)
            emptyView.setVisibility(View.VISIBLE);
        else
            emptyView.setVisibility(View.GONE);

        shudleUserEventsCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        emptyView.setVisibility(View.VISIBLE);
        shudleUserEventsCursorAdapter.changeCursor(null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " should implement Callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        callbacks.onClickListItemSelected(id);
    }

    void onReload() {
        try {
            getLoaderManager().restartLoader(Event.ContentProvider.ALL_ROWS_CODE, null, ShudleUserEventsFragment.this);
        } catch (Exception exception) {
        }
    }
}