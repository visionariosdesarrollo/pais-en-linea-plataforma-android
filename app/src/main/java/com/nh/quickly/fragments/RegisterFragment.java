package com.nh.quickly.fragments;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nh.quickly.R;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Department;
import com.nh.quickly.database.models.DocumentType;
import com.nh.quickly.database.models.User;
import com.nh.quickly.fragments.helpers.DocumentTypeListFragment;
import com.nh.quickly.nh.fragments.NHFragment;
import com.nh.quickly.nh.utils.validators.NHMail;
import com.nh.quickly.nh.utils.validators.NHNetworking;
import com.nh.quickly.nh.utils.validators.NHString;


/**
 * Created by Diego on 26/10/2015.
 */
public class RegisterFragment extends NHFragment implements View.OnClickListener {
    @Override
    public void onClick(View v) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.register_fragment, viewGroup, false);

        return rootView;
    }

    @Override
    public String getAutomaticTag() {
        return null;
    }
/*
    public final static String TAG = RegisterFragment.class.toString();
    private View rootView;
    private String zone;
    private String sex;
    private Department department;
    private City city;

    private EditText name;
    private Button documentTypeBtn;
    private EditText document;
    private Button documentDeliveryDateBtn;
    private RadioButton zoneRuralRadioButton;
    private RadioButton zoneTownRadioButton;
    private RadioButton sexMaleRadioButton;
    private RadioButton sexFemaleRadioButton;
    private EditText email;
    private Button departmentBtn;
    private Button cityBtn;
    private TextView about;
    private ImageButton registerBtn;

    @Override
    public String getAutomaticTag() {
        return TAG;
    }

    public interface Callbacks {
        public void onProgressDialogFragmentShow(Boolean progressBarShow, int rIdMessageText, int rIdhandlerTextTxt, View.OnClickListener handlerTxtOnClickListener);
        public void onProgressDialogFragmentShowString(Boolean progressBarShow, String messageText, int rIdhandlerTextTxt, View.OnClickListener handlerTxtOnClickListener);
        public void onProgressDialogGone();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.register_fragment, viewGroup, false);
        rootView.findViewById(R.id.register_text_frame_layout).setVisibility(View.VISIBLE);

        start();

        return rootView;
    }

    private Boolean validate() {
        Boolean errors = true;

        Drawable iconEditText = getResources().getDrawable(R.drawable.error);
        iconEditText.setBounds(0, 0, iconEditText.getIntrinsicWidth(), iconEditText.getIntrinsicHeight());

        Drawable iconSpinner = getResources().getDrawable(R.drawable.puntos_error);
        iconSpinner.setBounds(0, 0, iconSpinner.getIntrinsicWidth(), iconSpinner.getIntrinsicHeight());

        if( name.getText().toString().length() == 0 ) {
            name.requestFocus();
            name.setError(getResources().getString(R.string.register_name) + " " + getResources().getString(R.string.validate_empty), iconEditText);
            return false;
        } else if ( name.getText().toString().length() < 6 ) {
            name.requestFocus();
            name.setError(getResources().getString(R.string.register_name) + " " + getResources().getString(R.string.validate_min), iconEditText);
            return false;
        } else if ( NHString.isString(name.getText().toString())) {
            name.requestFocus();
            name.setError(getResources().getString(R.string.register_name) + " " + getResources().getString(R.string.validate_string), iconEditText);
            return false;
        } else
            name.setError(null, null);

        if( documentType == null ) {
            documentTypeBtn.requestFocus();
            documentTypeBtn.requestFocusFromTouch();
            documentTypeBtn.setError(getResources().getString(R.string.validate_empty_option) + " " + getResources().getString(R.string.register_document_type) + ".", iconSpinner);
            return false;
        } else
            documentTypeBtn.setError(null, null);

        if( document.getText().toString().length() == 0 ) {
            document.requestFocus();
            document.setError(getResources().getString(R.string.register_number_document) + " " + getResources().getString(R.string.validate_empty), iconEditText);
            return false;
        } else if ( name.getText().toString().length() < 6 ) {
            document.requestFocus();
            document.setError(getResources().getString(R.string.register_number_document) + " " + getResources().getString(R.string.validate_min), iconEditText);
            return false;
        } else
            document.setError(null, null);

        if( documentDeliveryDate == null ) {
            documentDeliveryDateBtn.requestFocus();
            documentDeliveryDateBtn.setError(getResources().getString(R.string.validate_empty_option) + " " + getResources().getString(R.string.register_document_delivery_date) + ".", iconSpinner);
            return false;
        } else
            documentDeliveryDateBtn.setError(null, null);

        if(zone == null) {
            zoneRuralRadioButton.requestFocus();
            zoneRuralRadioButton.setError(getResources().getString(R.string.validate_empty_option) + " " + getResources().getString(R.string.register_zone) + ".", null);
            return false;
        } else
            zoneRuralRadioButton.setError(null, null);

        if(sex == null) {
            sexMaleRadioButton.requestFocus();
            sexMaleRadioButton.setError(getResources().getString(R.string.validate_empty_option) + " " + getResources().getString(R.string.register_sex) + ".", null);
            return false;
        } else
            sexMaleRadioButton.setError(null, null);

        if( email.getText().toString().length() == 0 ) {
            email.requestFocus();
            email.setError(getResources().getString(R.string.register_mail) + " " + getResources().getString(R.string.validate_empty), iconEditText);
            return false;
        } else if ( NHMail.isMail(email.getText().toString()) ) {
            email.requestFocus();
            email.setError(getResources().getString(R.string.register_mail) + " " + getResources().getString(R.string.validate_mail), iconEditText);
            return false;
        } else
            email.setError(null, null);

        if( department == null ) {
            departmentBtn.requestFocus();
            departmentBtn.setError(getResources().getString(R.string.validate_empty_option) + " " + getResources().getString(R.string.department) + ".", iconSpinner);
            return false;
        } else
            departmentBtn.setError(null, null);

        if( city == null ) {
            cityBtn.requestFocus();
            cityBtn.setError(getResources().getString(R.string.validate_empty_option) + " " + getResources().getString(R.string.city) + ".", iconSpinner);
            return false;
        } else
            cityBtn.setError(null, null);

        return errors;
    }

    private void start() {
        startTextsView();
        startButtons();
        startRadioGroups();
        addListerners();
    }

    private void clearFields(){
        name.setError(null);
        documentTypeBtn.setError(null);
        document.setError(null);
        documentDeliveryDateBtn.setError(null);
        zoneRuralRadioButton.setError(null);
        sexMaleRadioButton.setError(null);
        email.setError(null);
        departmentBtn.setError(null);
        cityBtn.setError(null);
    }

    private void startTextsView(){
        name = (EditText) rootView.findViewById(R.id.name);
        document = (EditText) rootView.findViewById(R.id.document);
        email = (EditText) rootView.findViewById(R.id.email);
        about = (TextView) rootView.findViewById(R.id.about_txt);
    }

    private void setDocumentTypeTextButton() {
        if(documentType != null)
            documentTypeBtn.setText(documentType.getName());
        else
            documentTypeBtn.setText(getResources().getText(R.string.register_select_document_type));
    }

    private String getDocumentDeliveryDateString(){
        String day = String.valueOf(documentDeliveryDate.getDayOfMonth() > 9 ? documentDeliveryDate.getDayOfMonth() : "0" + documentDeliveryDate.getDayOfMonth());
        String month = String.valueOf(documentDeliveryDate.getMonth() > 9 ? documentDeliveryDate.getMonth() : "0" + documentDeliveryDate.getMonth());
        String year = String.valueOf(documentDeliveryDate.getYear());

        return day + "/" + month + "/" + year;
    }

    private String getDocumentDeliveryDateStringDataBase(){
        String day = String.valueOf(documentDeliveryDate.getDayOfMonth() > 9 ? documentDeliveryDate.getDayOfMonth() : "0" + documentDeliveryDate.getDayOfMonth());
        String month = String.valueOf(documentDeliveryDate.getMonth() > 9 ? documentDeliveryDate.getMonth() : "0" + documentDeliveryDate.getMonth());
        String year = String.valueOf(documentDeliveryDate.getYear());

        return year + "-" + month + "-" + day;
    }

    private void setDocumentDeliveryDateButton() {
        if(documentDeliveryDate != null) {
            documentDeliveryDateBtn.setText(getDocumentDeliveryDateString());
        } else
            documentDeliveryDateBtn.setText(getResources().getText(R.string.register_document_delivery_date));
    }

    private void setDepartmentTextButton() {
        if(department != null)
            departmentBtn.setText(department.getName());
        else
            departmentBtn.setText(getResources().getText(R.string.register_select_department));
    }

    private void setCityTextButton() {
        if(city != null)
            cityBtn.setText(city.getName());
        else
            cityBtn.setText(getResources().getText(R.string.register_select_city));
    }

    private void startRadioGroups(){
        zoneRuralRadioButton  = (RadioButton) rootView.findViewById(R.id.territoryRural);
        zoneTownRadioButton = (RadioButton) rootView.findViewById(R.id.territoryTown);
        sexMaleRadioButton = (RadioButton) rootView.findViewById(R.id.sexMan);
        sexFemaleRadioButton = (RadioButton) rootView.findViewById(R.id.sexFemale);
    }

    private void startButtons(){
        documentTypeBtn = (Button) rootView.findViewById(R.id.documentType);
        documentDeliveryDateBtn = (Button) rootView.findViewById(R.id.document_delivery_date_btn);
        departmentBtn = (Button) rootView.findViewById(R.id.department);
        cityBtn = (Button) rootView.findViewById(R.id.city);
        registerBtn = (ImageButton) rootView.findViewById(R.id.register_btn);

        setDocumentTypeTextButton();
        setDocumentDeliveryDateButton();
        setDepartmentTextButton();
        setCityTextButton();
    }

    private void addListerners(){
        documentTypeBtn.setOnClickListener(this);
        documentDeliveryDateBtn.setOnClickListener(this);
        departmentBtn.setOnClickListener(this);
        cityBtn.setOnClickListener(this);
        about.setOnClickListener(this);

        View.OnClickListener onClickListenerRadioButton = new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                RadioButton radioButton = (RadioButton) view;
                boolean checked = radioButton.isChecked();

                switch(radioButton.getId()) {
                    case R.id.territoryRural:
                        if (checked)
                            zone = User.TERRITORY_RURAL;
                            break;
                    case R.id.territoryTown:
                        if (checked)
                            zone = User.TERRITORY_URBAN;
                            break;
                    case R.id.sexMan:
                        if (checked)
                            sex = User.SEX_MAN;
                            break;
                    case R.id.sexFemale:
                        if (checked)
                            sex = User.SEX_FEMALE;
                            break;
                }
            }
        };
        zoneRuralRadioButton.setOnClickListener(onClickListenerRadioButton);
        zoneTownRadioButton.setOnClickListener(onClickListenerRadioButton);
        sexMaleRadioButton.setOnClickListener(onClickListenerRadioButton);
        sexFemaleRadioButton.setOnClickListener(onClickListenerRadioButton);

        registerBtn.setOnClickListener(this);
    }

    public void changeDocumentType(String documentTypeId){
        documentType = new DocumentType().one(getContext(), documentTypeId);
        documentTypeBtn.requestFocus();
    }

    public void changeDocumentDeliveryDate(DatePicker view){
        documentDeliveryDate = view;
        documentDeliveryDateBtn.requestFocus();
        setDocumentDeliveryDateButton();
    }

    public void changeDepartment(int departmentId){
        department = new Department().one(getContext(), departmentId);
        departmentBtn.requestFocus();
        city = null;
    }

    public void changeCity(int cityId){
        city = new City().one(getContext(), cityId);
        cityBtn.requestFocus();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callbacks = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " debe implementar Callbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();

        switch (view.getId()){
            case R.id.documentType:
                DocumentTypeListFragment documentTypeListFragment = new DocumentTypeListFragment();
                if(documentType != null) {
                    bundle.putString("documentTypeId", documentType.getId());
                    documentTypeListFragment.setArguments(bundle);
                }

                clearFields();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right).replace(R.id.column, documentTypeListFragment).addToBackStack(DocumentTypeListFragment.TAG).commit();
                break;
            case R.id.document_delivery_date_btn:
                clearFields();
                DialogFragment datePickerFragment = new DatePickerDialogFragment();
                datePickerFragment.show(getActivity().getSupportFragmentManager(), DatePickerDialogFragment.TAG);
                break;
            case R.id.department:
                DepartmentListFragment departmentListFragment = new DepartmentListFragment();
                if(department != null) {
                    bundle.putInt("departmentId", department.getId());
                    departmentListFragment.setArguments(bundle);
                }

                clearFields();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right).replace(R.id.column, departmentListFragment).addToBackStack(DepartmentListFragment.TAG).commit();
                break;
            case R.id.city:
                CityListFragment cityListFragment = new CityListFragment();
                if(department != null) {
                    bundle.putInt("departmentId", department.getId());

                    if(city != null)
                        bundle.putInt("cityId", city.getId());

                    cityListFragment.setArguments(bundle);
                }

                clearFields();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right).replace(R.id.column, cityListFragment).addToBackStack(CityListFragment.TAG).commit();
                break;
            case R.id.about_txt:
                clearFields();
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out).replace(R.id.column, new AboutFragment()).addToBackStack(AboutFragment.TAG).commit();
                break;
            default:
                if(validate()){
                    if(NHNetworking.isConnected(getActivity())) {
                        String[] arguments = new String[18];
                        arguments[0] = "name";
                        arguments[1] = String.valueOf(name.getText());
                        arguments[2] = "documentType";
                        arguments[3] = documentType.getId();
                        arguments[4] = "document";
                        arguments[5] = String.valueOf(document.getId());
                        arguments[6] = "sex";
                        arguments[7] = sex.toString();
                        arguments[8] = "email";
                        arguments[9] = String.valueOf(email.getText());
                        arguments[10] = "departmentId";
                        arguments[11] = String.valueOf(department.getId());
                        arguments[12] = "cityId";
                        arguments[13] = String.valueOf(city.getId());
                        arguments[14] = "territory";
                        arguments[15] = zone.toString();
                        arguments[16] = "documentDeliveryDate";
                        arguments[17] = getDocumentDeliveryDateStringDataBase();

                        //new RegisteredUserAsyncTask().execute(arguments);
                    } else
                        callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.not_is_network_connected_buttom, this);
                }
        }
    }
/*
    public class RegisteredUserAsyncTask extends AsyncTask<String, Long, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            callbacks.onProgressDialogFragmentShow(true, R.string.user_registration, 0, null);
        }

        @Override
        protected String doInBackground(String... arguments) {
            try {
                return HttpRequest.post(App.URL_API_REGISTER_USER, true, arguments)
                        .accept(HttpRequest.CONTENT_TYPE_JSON)
                        .body();
            } catch (HttpRequest.HttpRequestException httpRequestException) {
                publishProgress();
                cancel(true);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Long... arguments) {
            super.onProgressUpdate(arguments);
            callbacks.onProgressDialogFragmentShow(false, R.string.not_is_network_connected, R.string.close, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callbacks.onProgressDialogGone();
                }
            });
        }

        @Override
        protected void onPostExecute(String response) {
            try{
                ArrayList<ErrorQuickly> arrayListErrorsQuickly = SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(getActivity()).errorsQuickly(response);

                if(arrayListErrorsQuickly.size() > 0) {
                    ErrorQuickly errorQuickly = arrayListErrorsQuickly.get(0);
                    callbacks.onProgressDialogFragmentShowString(false, errorQuickly.getMessage(), R.string.close, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callbacks.onProgressDialogGone();
                        }
                    });
                } else {
                    SynchronizeDataJsonToDataBase.getInstanceSynchronizeDataJsonToDataBase(getActivity()).userRegister(response);

                    new Configuration().insertDefault(getContext(), department.getId(), city.getId());
                    User user = Login.getInstanceUser(getContext());
                    if( user != null) {

                        if(getResources().getBoolean(R.bool.two_column)) {
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.column_left, new MainFragment()).commit();
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.column_right, new DataSynchronizationFragment()).commit();
                        } else
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.column, new DataSynchronizationFragment()).commit();
                    } else
                        callbacks.onProgressDialogFragmentShow(false, R.string.user_registration_error, R.string.close, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                callbacks.onProgressDialogGone();
                            }
                        });
                }
            } catch (Exception exception) {
                callbacks.onProgressDialogFragmentShow(false, R.string.user_registration_error, R.string.close,  new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callbacks.onProgressDialogGone();
                    }
                });
            }
        }
    }
    */
}