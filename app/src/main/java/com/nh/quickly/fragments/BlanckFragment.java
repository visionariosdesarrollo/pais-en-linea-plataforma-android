package com.nh.quickly.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nh.quickly.R;
import com.nh.quickly.nh.fragments.NHFragment;

/**
 * Created by Diego on 26/10/2015.
 */
public class BlanckFragment extends NHFragment {
    public final static String TAG = BlanckFragment.class.toString();
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.blanck_fragment, viewGroup, false);

        return rootView;
    }

    @Override
    public String getAutomaticTag() {
        return TAG;
    }
}
