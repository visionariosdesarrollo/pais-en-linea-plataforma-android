package com.nh.quickly.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.R;

/**
 * Created by Diego on 25/05/2016.
 */
public class SyncAccount {
    static final long SECONDS_PER_MINUTE = 60L;
    static final long SYNC_INTERVAL_IN_MINUTES = 1L;
    public static final long SYNC_INTERVAL =
            SYNC_INTERVAL_IN_MINUTES *
                    SECONDS_PER_MINUTE;

    static Account account = null;

    public static Account getInstance(Context context) {
        if (account == null)
            account = createSyncAccount(context);

        return account;
    }

    private static Account createSyncAccount(Context context) {
        Account account = new Account(context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.authenticator_account_type));
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        if (accountManager.addAccountExplicitly(account, null, null)) {}
        else {}

        return account;
    }
}