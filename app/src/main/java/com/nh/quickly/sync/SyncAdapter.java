package com.nh.quickly.sync;

import android.accounts.Account;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nh.quickly.App;
import com.nh.quickly.database.ContentProviderApp;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Country;
import com.nh.quickly.database.models.Department;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.database.models.Event;
import com.nh.quickly.database.models.Release;
import com.nh.quickly.fragments.menu.options.PreferencesFragment;
import com.nh.quickly.nh.database.NHContentProvider;
import com.nh.quickly.nh.sync.NHAbstractThreadedSyncAdapter;
import com.nh.quickly.nh.sync.volley.NHVolley;
import com.nh.quickly.nh.utils.NHArrayUtils;
import com.nh.quickly.nh.utils.NHFileUtils;
import com.nh.quickly.nh.utils.NHJsonUtils;
import com.nh.quickly.nh.utils.validators.NHNetworking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Diego on 16/06/2016.
 */
public class SyncAdapter extends NHAbstractThreadedSyncAdapter {
    ContentResolver contentResolver;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        contentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        makeRequest();
    }

    void makeRequest() {
        JsonObjectRequest jsonObjectRequestCountries = new JsonObjectRequest(App.URL_API_COUNTRIES, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                parseJsonSyncCountryToContentProvider(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        });

        JsonObjectRequest jsonObjectRequestDepartments = new JsonObjectRequest(App.URL_API_DEPARTMENTS, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                parseJsonSyncDepartmentToContentProvider(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        });

        JsonObjectRequest jsonObjectRequestCities = new JsonObjectRequest(App.URL_API_CITIES, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                parseJsonSyncCityToContentProvider(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        });

        if (NHNetworking.isConnected(getContext()))
            NHVolley.getInstance(getContext())
                    .addToQueue(jsonObjectRequestCountries, null)
                    .addToQueue(jsonObjectRequestDepartments, null)
                    .addToQueue(jsonObjectRequestCities, null);

        City city = PreferencesFragment.getCity(getContext());
        if (city != null) {
            JsonObjectRequest jsonObjectRequestEntities = new JsonObjectRequest(
                    App.URL_API_ENTITIES + "?" + Entity.Columns.CITY_ID + "=" + city.getId(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            parseJsonSyncEntityToContentProvider(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                        }
                    }
            );

            JsonObjectRequest jsonObjectRequestEvents = new JsonObjectRequest(
                    App.URL_API_EVENTS + "?" + Entity.Columns.CITY_ID + "=" + city.getId(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            parseJsonSyncEventToContentProvider(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                        }
                    }
            );

            JsonObjectRequest jsonObjectRequestReleases = new JsonObjectRequest(
                    App.URL_API_RELEASES + "?" + Release.Columns.CITY_ID + "=" + city.getId(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            parseJsonSyncReleaseToContentProvider(response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                }
            }
            );

            if (NHNetworking.isConnected(getContext()))
                NHVolley.getInstance(getContext())
                        .addToQueue(jsonObjectRequestEntities, null)
                        .addToQueue(jsonObjectRequestEvents, null)
                        .addToQueue(jsonObjectRequestReleases, null);
        }
    }

    void parseJsonSyncCountryToContentProvider(JSONObject jsonObject) {
        try {
            if (jsonObject.equals(null))
                throw new NullPointerException();

            JSONArray jsonArray = null;
            Uri uri;
            Uri uriContentProvider;
            ArrayList<ContentProviderOperation> arrayListContentProviderOperation = new ArrayList<ContentProviderOperation>();
            String schemeCountry = NHContentProvider.SCHEME + Country.ContentProvider.QUERY_PATH;
            uriContentProvider = Uri.parse(schemeCountry);
            Set<Integer> schemeCountryIds = new HashSet<Integer>();
            Cursor cursorAll = contentResolver.query(uriContentProvider, new String[]{Country.Columns._ID}, null, null, null);
            if (cursorAll != null && cursorAll.getCount() > 0) {
                while (cursorAll.moveToNext())
                    schemeCountryIds.add(cursorAll.getInt(Country.Columns.ID_POSITION));
                cursorAll.close();
            }

            jsonArray = jsonObject.getJSONArray(Country.model().getTableName());
            ContentValues contentValues = new ContentValues();
            int id;

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObjectItem = jsonArray.getJSONObject(i);
                    id = jsonObjectItem.getInt(Country.Columns.ID);
                    contentValues.put(Country.Columns.NAME, jsonObjectItem.getString(Country.Columns.NAME));
                    contentValues.put(Country.Columns.CODE, jsonObjectItem.getString(Country.Columns.CODE));

                    if (!schemeCountryIds.isEmpty() && schemeCountryIds.contains(id)) {
                        uri = Uri.parse(schemeCountry + id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newUpdate(uri)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeCountryIds.remove(id);
                    } else if (schemeCountryIds.isEmpty() || !schemeCountryIds.contains(id)) {
                        contentValues.put(Entity.Columns._ID, id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newInsert(uriContentProvider)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeCountryIds.remove(id);
                    }

                    contentValues.clear();
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (JSONException jsonException) {
                }
            }

            ContentProviderResult[] contentProviderResult = contentResolver.applyBatch(ContentProviderApp.AUTHORITY, arrayListContentProviderOperation);
            arrayListContentProviderOperation.clear();

            if (contentProviderResult != null)
                if (!schemeCountryIds.isEmpty())
                    contentResolver.delete(uriContentProvider, Country.Columns._ID + " IN(" + NHArrayUtils.implodeInteger(", ", schemeCountryIds.toArray(new Integer[schemeCountryIds.size()])) + ")", new String[]{});
        } catch (RemoteException remoteException) {
        } catch (OperationApplicationException operationApplicationException) {
        } catch (IllegalArgumentException illegalArgumentException) {
        } catch (NullPointerException nullPointerException) {
        } catch (ClassCastException classCastException) {
        } catch (JSONException jsonException) {
        }
    }

    void parseJsonSyncDepartmentToContentProvider(JSONObject jsonObject) {
        try {
            if (jsonObject.equals(null))
                throw new NullPointerException();

            JSONArray jsonArray = null;
            Uri uri;
            Uri uriContentProvider;
            ArrayList<ContentProviderOperation> arrayListContentProviderOperation = new ArrayList<ContentProviderOperation>();
            String schemeDepartment = NHContentProvider.SCHEME + Department.ContentProvider.QUERY_PATH;
            uriContentProvider = Uri.parse(schemeDepartment);

            Set<Integer> schemeDepartmentIds = new HashSet<Integer>();
            Cursor cursorAll = contentResolver.query(uriContentProvider, new String[]{Department.Columns._ID}, null, null, null);
            if (cursorAll != null && cursorAll.getCount() > 0) {
                while (cursorAll.moveToNext())
                    schemeDepartmentIds.add(cursorAll.getInt(Department.Columns.ID_POSITION));
                cursorAll.close();
            }

            jsonArray = jsonObject.getJSONArray(Department.model().getTableName());
            ContentValues contentValues = new ContentValues();
            int id;

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObjectItem = jsonArray.getJSONObject(i);
                    id = jsonObjectItem.getInt(Department.Columns.ID);
                    contentValues.put(Department.Columns.COUNTRY_ID, jsonObjectItem.getInt(Department.Columns.COUNTRY_ID));
                    contentValues.put(Department.Columns.NAME, jsonObjectItem.getString(Department.Columns.NAME));

                    if (!schemeDepartmentIds.isEmpty() && schemeDepartmentIds.contains(id)) {
                        uri = Uri.parse(schemeDepartment + id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newUpdate(uri)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeDepartmentIds.remove(id);
                    } else if (schemeDepartmentIds.isEmpty() || !schemeDepartmentIds.contains(id)) {
                        contentValues.put(Entity.Columns._ID, id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newInsert(uriContentProvider)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeDepartmentIds.remove(id);
                    }

                    contentValues.clear();
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (JSONException jsonException) {
                }
            }

            ContentProviderResult[] contentProviderResult = contentResolver.applyBatch(ContentProviderApp.AUTHORITY, arrayListContentProviderOperation);
            arrayListContentProviderOperation.clear();

            if (contentProviderResult != null)
                if (!schemeDepartmentIds.isEmpty())
                    contentResolver.delete(uriContentProvider, Department.Columns._ID + " IN(" + NHArrayUtils.implodeInteger(", ", schemeDepartmentIds.toArray(new Integer[schemeDepartmentIds.size()])) + ")", new String[]{});
        } catch (RemoteException remoteException) {
        } catch (OperationApplicationException operationApplicationException) {
        } catch (IllegalArgumentException illegalArgumentException) {
        } catch (NullPointerException nullPointerException) {
        } catch (ClassCastException classCastException) {
        } catch (JSONException jsonException) {
        }
    }

    void parseJsonSyncCityToContentProvider(JSONObject jsonObject) {
        try {
            if (jsonObject.equals(null))
                throw new NullPointerException();

            JSONArray jsonArray = null;
            Uri uri;
            Uri uriContentProvider;
            ArrayList<ContentProviderOperation> arrayListContentProviderOperation = new ArrayList<ContentProviderOperation>();
            String schemeCity = NHContentProvider.SCHEME + City.ContentProvider.QUERY_PATH;
            uriContentProvider = Uri.parse(schemeCity);

            Set<Integer> schemeCityIds = new HashSet<Integer>();
            Cursor cursorAll = contentResolver.query(uriContentProvider, new String[]{City.Columns._ID}, null, null, null);
            if (cursorAll != null && cursorAll.getCount() > 0) {
                while (cursorAll.moveToNext())
                    schemeCityIds.add(cursorAll.getInt(City.Columns.ID_POSITION));
                cursorAll.close();
            }

            jsonArray = jsonObject.getJSONArray(City.model().getTableName());
            ContentValues contentValues = new ContentValues();
            int id;

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObjectItem = jsonArray.getJSONObject(i);
                    id = jsonObjectItem.getInt(City.Columns.ID);
                    contentValues.put(City.Columns.DEPARTMENT_ID, jsonObjectItem.getInt(City.Columns.DEPARTMENT_ID));
                    contentValues.put(City.Columns.NAME, jsonObjectItem.getString(City.Columns.NAME));
                    contentValues.put(City.Columns.DESCRIPTION, jsonObjectItem.getString(City.Columns.DESCRIPTION));
                    contentValues.put(City.Columns.IMAGE, jsonObjectItem.getString(City.Columns.IMAGE));

                    if (!schemeCityIds.isEmpty() && schemeCityIds.contains(id)) {
                        uri = Uri.parse(schemeCity + id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newUpdate(uri)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeCityIds.remove(id);
                    } else if (schemeCityIds.isEmpty() || !schemeCityIds.contains(id)) {
                        contentValues.put(Entity.Columns._ID, id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newInsert(uriContentProvider)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeCityIds.remove(id);
                    }

                    String image = contentValues.getAsString(City.Columns.IMAGE);
                    if (!TextUtils.isEmpty(image) && image != null)
                        NHFileUtils.saveInStorage(getContext(), App.URL_API_CITIES_IMAGES + image, App.directoryMediaImagesCities(getContext()), image, null);

                    contentValues.clear();
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (JSONException jsonException) {
                }
            }

            ContentProviderResult[] contentProviderResult = contentResolver.applyBatch(ContentProviderApp.AUTHORITY, arrayListContentProviderOperation);
            arrayListContentProviderOperation.clear();

            if (contentProviderResult != null)
                if (!schemeCityIds.isEmpty())
                    contentResolver.delete(uriContentProvider, City.Columns._ID + " IN(" + NHArrayUtils.implodeInteger(", ", schemeCityIds.toArray(new Integer[schemeCityIds.size()])) + ")", new String[]{});
        } catch (RemoteException remoteException) {
        } catch (OperationApplicationException operationApplicationException) {
        } catch (IllegalArgumentException illegalArgumentException) {
        } catch (NullPointerException nullPointerException) {
        } catch (ClassCastException classCastException) {
        } catch (JSONException jsonException) {
        }
    }

    void parseJsonSyncEntityToContentProvider(JSONObject jsonObject) {
        try {
            if (jsonObject.equals(null))
                throw new NullPointerException();

            JSONArray jsonArray = null;
            Uri uri;
            Uri uriContentProvider;
            ArrayList<ContentProviderOperation> arrayListContentProviderOperation = new ArrayList<ContentProviderOperation>();
            String schemeEntity = NHContentProvider.SCHEME + Entity.ContentProvider.QUERY_PATH;
            uriContentProvider = Uri.parse(schemeEntity);

            Set<Integer> schemeEntityIds = new HashSet<Integer>();
            Cursor cursorAll = contentResolver.query(uriContentProvider, new String[]{Entity.Columns._ID}, null, null, null);
            if (cursorAll != null && cursorAll.getCount() > 0) {
                while (cursorAll.moveToNext())
                    schemeEntityIds.add(cursorAll.getInt(Entity.Columns.ID_POSITION));
                cursorAll.close();
            }

            jsonArray = jsonObject.getJSONArray(Entity.model().getTableName());
            ContentValues contentValues = new ContentValues();
            int id;

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObjectItem = jsonArray.getJSONObject(i);
                    id = jsonObjectItem.getInt(Entity.Columns.ID);
                    contentValues.put(Entity.Columns.CITY_ID, jsonObjectItem.getInt(Entity.Columns.CITY_ID));
                    contentValues.put(Entity.Columns.NAME, jsonObjectItem.getString(Entity.Columns.NAME));
                    contentValues.put(Entity.Columns.DESCRIPTION, jsonObjectItem.getString(Entity.Columns.DESCRIPTION));
                    contentValues.put(Entity.Columns.EMAIL, jsonObjectItem.getString(Entity.Columns.EMAIL));
                    contentValues.put(Entity.Columns.PHONE_ONE, jsonObjectItem.getString(Entity.Columns.PHONE_ONE));
                    contentValues.put(Entity.Columns.PHONE_TWO, jsonObjectItem.getString(Entity.Columns.PHONE_TWO));
                    contentValues.put(Entity.Columns.IMAGE, jsonObjectItem.getString(Entity.Columns.IMAGE));
                    contentValues.put(Entity.Columns.STATUS, jsonObjectItem.getString(Entity.Columns.STATUS));

                    if (!schemeEntityIds.isEmpty() && schemeEntityIds.contains(id)) {
                        uri = Uri.parse(schemeEntity + id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newUpdate(uri)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeEntityIds.remove(id);
                    } else if (schemeEntityIds.isEmpty() || !schemeEntityIds.contains(id)) {
                        contentValues.put(Entity.Columns._ID, id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newInsert(uriContentProvider)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeEntityIds.remove(id);
                    }

                    contentValues.clear();
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (JSONException jsonException) {
                }
            }

            ContentProviderResult[] contentProviderResult = contentResolver.applyBatch(ContentProviderApp.AUTHORITY, arrayListContentProviderOperation);
            arrayListContentProviderOperation.clear();

            if (contentProviderResult != null) {
                if (!schemeEntityIds.isEmpty())
                    contentResolver.delete(uriContentProvider, Entity.Columns._ID + " IN(" + NHArrayUtils.implodeInteger(", ", schemeEntityIds.toArray(new Integer[schemeEntityIds.size()])) + ")", new String[]{});
            }
        } catch (RemoteException remoteException) {
        } catch (OperationApplicationException operationApplicationException) {
        } catch (IllegalArgumentException illegalArgumentException) {
        } catch (NullPointerException nullPointerException) {
        } catch (ClassCastException classCastException) {
        } catch (JSONException jsonException) {
        }
    }

    void parseJsonSyncEventToContentProvider(JSONObject jsonObject) {
        try {
            if (jsonObject.equals(null))
                throw new NullPointerException();

            JSONArray jsonArray = null;
            Uri uri;
            Uri uriContentProvider;
            ArrayList<ContentProviderOperation> arrayListContentProviderOperation = new ArrayList<ContentProviderOperation>();
            String schemeEvent = NHContentProvider.SCHEME + Event.ContentProvider.QUERY_PATH;
            uriContentProvider = Uri.parse(schemeEvent);

            Set<Integer> schemeEventIds = new HashSet<Integer>();
            Cursor cursorAll = contentResolver.query(uriContentProvider, new String[]{Event.Columns.FULL__ID}, null, null, null);
            if (cursorAll != null && cursorAll.getCount() > 0) {
                while (cursorAll.moveToNext())
                    schemeEventIds.add(cursorAll.getInt(Event.Columns.ID_POSITION));
                cursorAll.close();
            }

            jsonArray = jsonObject.getJSONArray(Event.model().getTableName());
            ContentValues contentValues = new ContentValues();
            int id;

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObjectItem = jsonArray.getJSONObject(i);
                    id = jsonObjectItem.getInt(Event.Columns.ID);
                    contentValues.put(Event.Columns.ENTITY_ID, jsonObjectItem.getInt(Event.Columns.ENTITY_ID));
                    contentValues.put(Event.Columns.NAME, jsonObjectItem.getString(Event.Columns.NAME));

                    JSONArray types = jsonObjectItem.getJSONArray(Event.Columns.TYPE);
                    if (types.length() > 0)
                        contentValues.put(Event.Columns.TYPE, NHArrayUtils.implodeString(",", NHJsonUtils.convertJSONArrayToStringArray(types)));

                    contentValues.put(Event.Columns.EVENT_DATE_START, jsonObjectItem.getString(Event.Columns.EVENT_DATE_START));
                    contentValues.put(Event.Columns.EVENT_TIME_START, jsonObjectItem.getString(Event.Columns.EVENT_TIME_START));
                    contentValues.put(Event.Columns.EVENT_DATE_END, jsonObjectItem.getString(Event.Columns.EVENT_DATE_END));
                    contentValues.put(Event.Columns.EVENT_TIME_END, jsonObjectItem.getString(Event.Columns.EVENT_TIME_END));
                    contentValues.put(Event.Columns.SITE, jsonObjectItem.getString(Event.Columns.SITE));
                    contentValues.put(Event.Columns.BODY, jsonObjectItem.getString(Event.Columns.BODY));
                    contentValues.put(Event.Columns.EVENT_TIME_STAMP_START, jsonObjectItem.getString(Event.Columns.EVENT_TIME_STAMP_START));
                    contentValues.put(Event.Columns.EVENT_TIME_STAMP_END, jsonObjectItem.getString(Event.Columns.EVENT_TIME_STAMP_END));
                    contentValues.put(Event.Columns.EVENT_TIME_STAMP_END_LIMIT, jsonObjectItem.getString(Event.Columns.EVENT_TIME_STAMP_END_LIMIT));
                    contentValues.put(Event.Columns.STATUS, jsonObjectItem.getString(Event.Columns.STATUS));

                    Log.d(App.LOG_TAG, "NH::Sync::parseJsonSyncEventToContentProvider::" + jsonObjectItem.getString(Event.Columns.NAME));

                    if (!schemeEventIds.isEmpty() && schemeEventIds.contains(id)) {
                        Log.d(App.LOG_TAG, "NH::Sync::parseJsonSyncEventToContentProvider::update::" + jsonObjectItem.getString(Event.Columns.NAME));
                        uri = Uri.parse(schemeEvent + id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newUpdate(uri)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeEventIds.remove(id);
                    } else if (schemeEventIds.isEmpty() || !schemeEventIds.contains(id)) {
                        Log.d(App.LOG_TAG, "NH::Sync::parseJsonSyncEventToContentProvider::insert::" + jsonObjectItem.getString(Event.Columns.NAME));
                        contentValues.put(Entity.Columns._ID, id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newInsert(uriContentProvider)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeEventIds.remove(id);
                    }

                    contentValues.clear();
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (JSONException jsonException) {
                }
            }

            ContentProviderResult[] contentProviderResult = contentResolver.applyBatch(ContentProviderApp.AUTHORITY, arrayListContentProviderOperation);
            Log.d(App.LOG_TAG, "NH::Sync::parseJsonSyncEventToContentProvider::applyBatch::" + contentProviderResult.length);
            arrayListContentProviderOperation.clear();

            if (contentProviderResult != null) {
                if (!schemeEventIds.isEmpty()) {
                    int deletes = contentResolver.delete(uriContentProvider, Event.Columns._ID + " IN(" + NHArrayUtils.implodeInteger(", ", schemeEventIds.toArray(new Integer[schemeEventIds.size()])) + ")", new String[]{});
                    Log.d(App.LOG_TAG, "NH::Sync::parseJsonSyncEventToContentProvider::deletes::" + deletes);
                }
                contentResolver.notifyChange(uriContentProvider, null);
                Log.d(App.LOG_TAG, "NH::Sync::parseJsonSyncEventToContentProvider::NO CONFIRMO::");
            }
        } catch (RemoteException remoteException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncEventToContentProvider::RemoteException");
        } catch (OperationApplicationException operationApplicationException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncEventToContentProvider::OperationApplicationException");
        } catch (IllegalArgumentException illegalArgumentException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncEventToContentProvider::IllegalArgumentException");
        } catch (NullPointerException nullPointerException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncEventToContentProvider::NullPointerException");
        } catch (ClassCastException classCastException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncEventToContentProvider::ClassCastException");
        } catch (JSONException jsonException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncEventToContentProvider::JSONException");
        }
    }

    void parseJsonSyncReleaseToContentProvider(JSONObject jsonObject) {
        Log.d(App.LOG_TAG, "NH::SyncAdapter::parseJsonSyncReleaseToContentProvider::" + jsonObject);
        try {
            if (jsonObject.equals(null))
                throw new NullPointerException();

            JSONArray jsonArray = null;
            Uri uri;
            Uri uriContentProvider;
            ArrayList<ContentProviderOperation> arrayListContentProviderOperation = new ArrayList<ContentProviderOperation>();
            String schemeRelease = NHContentProvider.SCHEME + Release.ContentProvider.QUERY_PATH;
            uriContentProvider = Uri.parse(schemeRelease);

            Set<Integer> schemeReleaseIds = new HashSet<Integer>();
            Cursor cursorAll = contentResolver.query(uriContentProvider, new String[]{Release.Columns._ID}, null, null, null);
            if (cursorAll != null && cursorAll.getCount() > 0) {
                while (cursorAll.moveToNext())
                    schemeReleaseIds.add(cursorAll.getInt(Release.Columns.ID_POSITION));
                cursorAll.close();
            }

            jsonArray = jsonObject.getJSONArray(Release.model().getTableName());
            ContentValues contentValues = new ContentValues();
            int id;
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObjectItem = jsonArray.getJSONObject(i);
                    id = jsonObjectItem.getInt(Release.Columns.ID);
                    contentValues.put(Release.Columns.USER_ID, jsonObjectItem.getInt(Release.Columns.USER_ID));
                    contentValues.put(Release.Columns.NAME, jsonObjectItem.getString(Release.Columns.NAME));
                    contentValues.put(Release.Columns.RELEASE_DATE_START, jsonObjectItem.getString(Release.Columns.RELEASE_DATE_START));
                    contentValues.put(Release.Columns.RELEASE_DATE_END, jsonObjectItem.getString(Release.Columns.RELEASE_DATE_END));
                    contentValues.put(Release.Columns.BODY, jsonObjectItem.getString(Release.Columns.BODY));
                    contentValues.put(Release.Columns.RELEASE_TIME_STAMP_START, jsonObjectItem.getString(Release.Columns.RELEASE_TIME_STAMP_START));
                    contentValues.put(Release.Columns.RELEASE_TIME_STAMP_END, jsonObjectItem.getString(Release.Columns.RELEASE_TIME_STAMP_END));
                    contentValues.put(Release.Columns.CITY_ID, jsonObjectItem.getString(Release.Columns.CITY_ID));
                    contentValues.put(Release.Columns.USER_NAME, jsonObjectItem.getString(Release.Columns.USER_NAME));
                    contentValues.put(Release.Columns.USER_IMAGE, jsonObjectItem.getString(Release.Columns.USER_IMAGE));
                    contentValues.put(Release.Columns.USER_ROLE, jsonObjectItem.getString(Release.Columns.USER_ROLE));
                    contentValues.put(Release.Columns.USER_DEPARTMENT_NAME, jsonObjectItem.getString(Release.USER_DEPARTMENT_NAME));
                    contentValues.put(Release.Columns.USER_CITY_NAME, jsonObjectItem.getString(Release.Columns.USER_CITY_NAME));
                    contentValues.put(Release.Columns.STATUS, jsonObjectItem.getString(Release.Columns.STATUS));

                    if (!schemeReleaseIds.isEmpty() && schemeReleaseIds.contains(id)) {
                        uri = Uri.parse(schemeRelease + id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newUpdate(uri)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeReleaseIds.remove(id);
                    } else if (schemeReleaseIds.isEmpty() || !schemeReleaseIds.contains(id)) {
                        contentValues.put(Entity.Columns._ID, id);
                        arrayListContentProviderOperation.add(ContentProviderOperation.newInsert(uriContentProvider)
                                .withValues(contentValues)
                                .withYieldAllowed(true)
                                .build());
                        schemeReleaseIds.remove(id);
                    }

                    contentValues.clear();
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (JSONException jsonException) {
                }
            }

            ContentProviderResult[] contentProviderResult = contentResolver.applyBatch(ContentProviderApp.AUTHORITY, arrayListContentProviderOperation);
            arrayListContentProviderOperation.clear();

            if (contentProviderResult != null) {
                if (!schemeReleaseIds.isEmpty())
                    contentResolver.delete(uriContentProvider, Release.Columns._ID + " IN(" + NHArrayUtils.implodeInteger(", ", schemeReleaseIds.toArray(new Integer[schemeReleaseIds.size()])) + ")", new String[]{});
                contentResolver.notifyChange(uriContentProvider, null);
            }
        } catch (RemoteException remoteException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncReleaseToContentProvider::RemoteException");
        } catch (OperationApplicationException operationApplicationException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncReleaseToContentProvider::OperationApplicationException");
        } catch (IllegalArgumentException illegalArgumentException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncReleaseToContentProvider::IllegalArgumentException");
        } catch (NullPointerException nullPointerException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncReleaseToContentProvider::NullPointerException");
        } catch (ClassCastException classCastException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncReleaseToContentProvider::ClassCastException");
        } catch (JSONException jsonException) {
            Log.d(App.LOG_TAG, "NH::parseJsonSyncReleaseToContentProvider::JSONException");
        }
    }
}