package com.nh.quickly.sync.authenticator;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.nh.quickly.App;

/**
 * Created by Diego on 16/06/2016.
 */
public class AuthenticatorService extends Service {
    private Authenticator nhAbstractAccountAuthenticator;

    @Override
    public void onCreate() {
        Log.i(App.LOG_TAG, "NH::AuthenticatorService::onCreate");
        nhAbstractAccountAuthenticator = new Authenticator(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(App.LOG_TAG, "NH::AuthenticatorService::onBind");
        return nhAbstractAccountAuthenticator.getIBinder();
    }
}
