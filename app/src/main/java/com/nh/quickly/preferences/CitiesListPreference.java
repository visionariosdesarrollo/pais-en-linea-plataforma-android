package com.nh.quickly.preferences;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.preference.ListPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.database.models.City;
import com.nh.quickly.database.models.Department;
import com.nh.quickly.nh.database.NHContentProvider;

/**
 * Created by Diego on 02/07/2016.
 */
public class CitiesListPreference extends ListPreference {
    public static String KEY = "configuration_preferences_location_city";
    CharSequence citiesArray[];
    CharSequence citiesIdsArray[];

    public CitiesListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CitiesListPreference(Context context) {
        super(context);
    }

    public boolean setCities(int departmentId) {
        boolean error = true;
        try {
            if (departmentId > 0) {
                Cursor citiesContentResolver = getContext().getContentResolver().query(Uri.parse(NHContentProvider.SCHEME + City.ContentProvider.QUERY_PATH), City.Columns.getColumns(), City.Columns.DEPARTMENT_ID + "=?", new String[]{String.valueOf(departmentId)}, City.Columns.SORT_BY_NAME_ASC);
                if (citiesContentResolver != null && citiesContentResolver.getCount() > 0) {
                    citiesContentResolver.moveToFirst();
                    citiesArray = new CharSequence[citiesContentResolver.getCount()];
                    citiesIdsArray = new CharSequence[citiesContentResolver.getCount()];
                    int i = 0;
                    do {
                        citiesArray[i] = citiesContentResolver.getString(City.Columns.NAME_POSITION);
                        citiesIdsArray[i] = String.valueOf(citiesContentResolver.getInt(City.Columns.ID_POSITION));
                        i++;
                    } while (citiesContentResolver.moveToNext());

                    error = false;
                }
            }

            setEntries(citiesArray);
            setEntryValues(citiesIdsArray);
        }
        catch (Exception exception){}

        return error;
    }
}