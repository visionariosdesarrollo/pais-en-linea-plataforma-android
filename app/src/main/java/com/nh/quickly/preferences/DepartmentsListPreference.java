package com.nh.quickly.preferences;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.preference.ListPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;

import com.nh.quickly.App;
import com.nh.quickly.database.models.Department;
import com.nh.quickly.database.models.Entity;
import com.nh.quickly.nh.database.NHContentProvider;

/**
 * Created by Diego on 02/07/2016.
 */
public class DepartmentsListPreference extends ListPreference {
    public static String KEY = "configuration_preferences_location_department";
    CharSequence departmentsArray[];
    CharSequence departmentsIdsArray[];

    public DepartmentsListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDepartments();
    }

    public DepartmentsListPreference(Context context) {
        super(context);
        setDepartments();
    }

    public void setDepartments() {
        try {
            Cursor departmentsContentResolver = getContext().getContentResolver().query(Uri.parse(NHContentProvider.SCHEME + Department.ContentProvider.QUERY_PATH), Department.Columns.getColumns(), null, null, Department.Columns.SORT_BY_NAME_ASC);
            if(departmentsContentResolver != null && departmentsContentResolver.getCount() > 0) {
                departmentsContentResolver.moveToFirst();
                departmentsArray = new CharSequence[departmentsContentResolver.getCount()];
                departmentsIdsArray = new CharSequence[departmentsContentResolver.getCount()];
                int i = 0;
                do {
                    departmentsArray[i] = departmentsContentResolver.getString(Department.Columns.NAME_POSITION);
                    departmentsIdsArray[i] = String.valueOf(departmentsContentResolver.getInt(Department.Columns.ID_POSITION));
                    i++;
                }while (departmentsContentResolver.moveToNext());
            }

            setEntries(departmentsArray);
            setEntryValues(departmentsIdsArray);
        }
        catch (Exception exception){}
    }
}